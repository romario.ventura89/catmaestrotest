<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MINISO M&Eacute;XICO | CATALOGO MAESTRO</title>

        <link rel="icon" type="image/png" href="static/img/logo.png" />

        <link href="static/css/bootstrap.min.css" rel="stylesheet" />
        <link href="static/css/bootstrap-datepicker.min.css" rel="stylesheet" />
        <link href="static/css/datatable.css" rel="stylesheet" />
        <link href="static/css/font-awesome.min.css" rel="stylesheet" />
        <link href="static/css/animate.css" rel="stylesheet" />
        <link href="static/css/noty.css" rel="stylesheet">
        <link href="static/css/themes/metroui.css" rel="stylesheet">
        <link href="static/css/fuentes.css" rel="stylesheet">
        <link href="static/css/custom.css" rel="stylesheet" />
        <link href="static/css/loader.css" rel="stylesheet" />
        <link href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" rel="stylesheet"/>
    </head>

    <body style="display: none">


        <div id="loader" style="display: none">  
            <div id="inner_loader">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                    <div class="loader-text">Cargando...</div>
                </div>
                <div id="backLoader"></div>
            </div>
        </div>

        <div id="menu" class="container">

        </div>

        <div class="container" id="cuerpo" >
            
        </div>
    </div>
</div>


<div class="footer">
    <div class="container text-center col-md-12">
        <p id="footer-Miniso">
            MINISO MEXICO

        </p>
        <p id="derechosReservados">
            Todos los derechos reservados 2018
        </p>

        <div id="linea"></div>
        <p>

        </p>

    </div>
</div>
</div>

<script src="static/js/lib/jquery-3.2.1.min.js"></script>
<script src="static/js/lib/bootstrap.min.js"></script>
<script src="static/js/lib/bootstrap-datepicker.min.js"></script>
<script src="static/js/lib/bootstrap-datepicker.es.min.js"></script>
<script src="static/js/lib/noty.min.js"></script>
<script src="static/js/lib/moment.min.js"></script>
<script src="static/js/lib/xdomain.js"></script>
<script src="static/js/dataTable.js"></script>
<script src="static/js/lib/dataTables.fixedColumns.min.js"></script>
<script src="static/js/conecta.js"></script>
<script src="static/js/utilidades.js"></script>

<script src="static/js/index.js"></script>

</body>
</html>
