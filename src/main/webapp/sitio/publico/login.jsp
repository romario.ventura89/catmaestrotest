

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Iniciar sesión.</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>
<div class="row">

    <div class="col-md-12 text-center" style="margin-bottom: 20px">
        <h5>Favor de ingresar su usuario y contraseña</h5>
    </div>
    <div class="col-md-12 form-horizontal">
        <div class="form-group">
            <label class="col-md-3 col-xs-4 control-label">Usuario:</label>
            <div class="col-md-6 col-xs-8">
                <input type="text" style="text-transform: none" id="username" class="form-control"/>
            </div>
        </div>
        <div class="form-group">

            <label class="col-md-3 col-xs-4  control-label">Contraseña:</label>
            <div class="col-md-6 col-xs-8">
                <input type="password" style="text-transform: none" id="contra" class="form-control"/>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <hr />
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-danger" id="enviar"><i class="fa fa-sign-in" style="margin-right: 5px"></i> Ingresar </button>
    </div>
</div>
<script src="static/js/login.js"></script>
