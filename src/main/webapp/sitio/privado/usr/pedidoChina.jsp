<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "../privado/error.jsp";
</script>
<% } else {
    String nombre = usuario.getTitulo() + " " + usuario.getNombre() + " " + usuario.getApellidos();
%>

<form id="enviaArchivoChina"  name="enviaArchivoChina"
      enctype="multipart/form-data" method="post" role="">
    <div class="row">
        <div class="col-md-12 text-right">
            <h5>Usuario: <span id="nombreUsuario"><%=nombre%></span></h5>
            <li class="dropdown"><a href="#"  id="salir"><i class="fa  fa-power-off"></i> Salir</a>
            </li>
        </div>
    </div>
    <div class="row">
        <div id="divErrorPagina" class="col-md-12" style="display: none">
            <div id="divError" class="alert alert-danger">
                Error de carga de archivo: <br> <span id="LblError">
                        El archivo no pudo cargarse de manera correcta favor de
                        intentarlo mas tarde </span>
            </div>
        </div>
        <div id="divExitoPagina" class="col-md-12" style="display: none">
            <div id="divExito" class="alert alert-success">
                <span id="numReg"></span> correctamente.
            </div>
        </div>
    </div>
    <div class="row" id="cuerpo">

        <div class="form-group col-md-12">
            <label class="col-md-3 control-label">Archivo:</label>
            <div class="col-md-6">
                <input class="errorMessageCall col-md-12"
                    name="inputFileExcelCarga" id="inputFileExcelCarga" type="file"
                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    style="padding: 10px;" />
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="col-md-3 control-label">Fecha de pedido:</label>
            <div class="col-md-6">
                <input type="text" style="text-transform: none" id="fechaRegistro" class="form-control"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <label class="col-md-6 control-label"></label>
            <div class="col-md-6 control-label">
                <button type="button" class="btn btn-danger" id="btnCargar">Enviar Respuesta</button>
            </div>
            
            <div class="col-md-6 control-label">
                <button type="button" class="btn btn-danger" id="prueba">Prueba</button>
            </div>
        </div> 

        <ul class="nav nav-tabs" role="tablist">

        </ul>
    </div>
</form>
<script src="static/js/PedidoChina.js"></script>
<script src="static/js/cargarArchivo.js"></script>
<% }%>
