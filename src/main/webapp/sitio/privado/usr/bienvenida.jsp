<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>
<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "../dmz/error.jsp";
</script>
<% } else {%>

<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Bienvenido</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>Catalogo Maestro</h2>
    </div>
</div>
<div class="row">
    <hr />
</div>
<div class="row">
    <div class="col-md-12">
        <div id="sociedades"></div>
    </div>
</div>
<script src="static/js/bienvenida.js"></script>
<% }%>
