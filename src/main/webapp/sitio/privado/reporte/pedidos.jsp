<%-- 
    Document   : carga_masiva
    Created on : 20/03/2018, 07:59:54 PM
    Author     : e5-523
--%>
<script>
    $("body").show();
</script>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Reporte confirmación detalle</p>
    </div>
</div>
<div class="row">
    <hr class="rojohr"/>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Filters</label>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group confirmaciones" >
                        <label class="col-md-2 text-right">Start Date:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="fechaInicio" class="form-control" readonly="readonly"/>
                        </div>
                        <label class="col-md-2 text-right">End Date:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="fechaFin" class="form-control" readonly="readonly"/>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <button type="button" class="btn btn-default" id="limpiar">Clear</button>
                    <button type="button" class="btn btn-danger" id="buscar">Search</button>
                </div>
            </div>
        </div>
    </div>

</div>



<div class="row">
    <div class="col-md-12" id="resultado">


    </div>
    <div class="col-md-12" id="botonDescarga" style="display: none">
        <div class="panel-footer text-right" id="botonExcel" style="margin-top: 20px">  
            <button id="descargarReporte" class="btn btn-danger" >Download</button>
        </div>
    </div>
</div>



<script src="static/js/reportes/pedidos.js"></script>