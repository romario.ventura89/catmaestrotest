<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <body>
        <main id="cuerpo"></main>
        <div id="menu"></div>
        <div id="portada" >
            <div class="titan-caption">
                <div class="caption-content" style="color: #fff">
           
                   
                        <div class="col-md-12">
                            <h3>Favor de proporcionarnos la siguiente información</h3>
                        </div>
                    <div class="form-horizontal col-md-offset-3 col-md-6">
                        <div class="form-group">
                            <form class="form">
                                <div class="form-group">
                                    <input type="text" style="text-transform: none" id="email" class="form-control" placeholder="Ticket"/><br />
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Código de verificación:</label>
                                    <div class="col-sm-3">
                                        <img id="captcha" />			
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" maxlength="6" class="form-control"
                                               id="txtCaptcha" style="text-transform: none"/>
                                        <small id="error_txtCaptcha"
                                               style="display: none; color: #D0021B"></small>
                                    </div>
                                    <div class="col-md-1">
                                        <a class="section-scroll btn btn-border-w btn-round" href="#" id="refrescar" ><i class="fa fa-refresh"></i></a>
                                    </div>
                                </div>
                                <a class="section-scroll btn btn-border-w btn-round" href="#" id="buscaEmail" >Enviar información</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

   

        <hr class="divider-d">

    </body>

