

<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>
<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Carga de archivo Json(GdInput)</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>


<div class="row">
    <form id="cargaMasiva" name="cargamasiva" id="cargamasiva"
          enctype="multipart/form-data" method="post" role="" class="form-horizontal">

        <div class="form-group">
            <label class="col-md-2 text-right">Fecha de carga</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="fechaCarga" name="fechaCarga" class="form-control" readonly="readonly"/>
            </div>

            <label class="col-md-2 text-right">Identificador Json(Input)</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="numeroInput" name="numeroOrden" class="form-control"/>
            </div>

        </div>
        <div class="form-group">
            <label class="col-md-2 text-right"  for="archivo">Archivo de carga masiva:</label> 
            <div class="col-md-8">
                <input class=" col-md-12"
                       name="archivo" id="archivo" type="file"
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                       style="padding: 10px;" />
            </div>
        </div>

        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-danger" id="cargar">Cargar</button>
        </div>

    </form>
</div>

<div class="row" id="panelResultados" style="display: none;margin-top: 30px">
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Resultados</label>
            </div>
            <div class="panel-body">
                <h1 id="resultadoCarga"></h1>
                <h3>Order: <span id="orderRes"></span></h3>
                <h3>Fecha de confirmación: <span id="fecRes"></span></h3>
                <h5 id="datosRes"></h5>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Code</th>
                            <th>Munit</th>
                            <th>Qpc</th>
                            <th>QpcStr</th>
                            <th>RtlPrc</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyRes">
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>

<div class="modal body" tabindex="-1" role="dialog" id="modalConfirmacion" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Remplazar archivo</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div  class="col-md-12">
                        <h5>¿Seguro que desea remplazar la informacion de la confirmación con el número de orden <span id="numeroContenedorRemplazar"></span>?</h5>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="">Close</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="modalSubir">Upload</button>
            </div>
        </div>
    </div>
</div>

<script src="static/js/catalogo/carga_json_input.js"></script>

<% }%>