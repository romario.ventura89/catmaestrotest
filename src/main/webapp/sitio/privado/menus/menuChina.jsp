<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
    String nombre = usuario.getTitulo() + " " + usuario.getNombre() + " " + usuario.getApellidos();
%>
<div class="row">
    <div class="col-md-2 col-xs-3">
        <img src="static/img/logo_pagina.png" alt="Miniso" class="animated bounceIn" />
    </div>
    <div class="col-md-10 col-xs-9">
        <h1 id="tituloDentro">CATALOGO MAESTRO MINISO Mx</h1>
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-8" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>  
            </div>
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-8"> 
                <ul class="nav navbar-nav " style="width: 100%"> 
                    <li class="navbar-tex">
                        <a href="javascript:;" id="inicio"><i class="glyphicon glyphicon-home"></i> Inicio</a>
                    </li>
                    <li class="navbar-tex">
                        <a href="javascript:;">MÉXICO - CHINA</a>
                    </li> 
                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Confirmacion <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;" id="misConfirmaciones">Mis confirmaciones</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:;" id="cargaMasivaConfirmaciones">Carga Masiva</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Facturas <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;" id="misFacturas">Mis Facturas</a></li>
                            <li><a href="javascript:;" id="menuReactivacion">Reactivar Facturas</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:;" id="cargaMasivaFacturas">Carga Masiva</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;" id="rConfirmacion">Reporte confirmacion </a></li>
                            <li><a href="javascript:;" id="rComodato">Reporte comodato</a></li>
                            <li><a href="javascript:;" id="rPedidos">Reporte confirmación detalle</a></li>
                        </ul>
                    </li>

                    <li class="navbar-tex navbar-right">
                        <a href="javascript:;" id="salir"><i class="fa  fa-power-off"></i> Salir</a>
                    </li> 
                    <li class="navbar-tex navbar-right">
                        <a href="javascript:;" id="descargaReporte"><i class="fa  fa-download"></i> Descargar reporte</a>
                    </li> 
                    <li class="navbar-tex navbar-right">
                        <a href="javascript:;">Usuario: <span id="nombreUsuario" style="font-weight: bold"><%=nombre%></span></a>
                    </li> 
                </ul> 
            </div>
        </nav>
    </div>

</div>


<script src="static/js/menuChina.js"></script>
<% }%>