<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
    String nombre = usuario.getTitulo() + " " + usuario.getNombre() + " " + usuario.getApellidos();
%>
<div class="row">
    <div class="col-md-2 col-xs-3">
        <img src="static/img/logo_pagina.png" alt="Miniso" class="animated bounceIn" />
    </div>
    <div class="col-md-10 col-xs-9">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-8" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>  
            </div>
            <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-8">
                <ul class="nav navbar-nav " style="width: 100%">
                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cat�logo Maestro <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;" id="miCatalogo">Cat�logo Goods</a></li>
                            <li><a href="javascript:;" id="cargaJsonGds">Carga JSON Goods</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:;" id="miCatalogoInput">Cat�logo Input</a></li>
                            <li><a href="javascript:;" id="cargaJsonInpt">Carga JSON Input</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cat�logo Excel<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;" id="carExcel">Carga Excel</a></li>
                            <li><a href="javascript:;" id="catExcel">Consulta Cat�logo Excel</a></li>
                        </ul>
                    </li>
<!--                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Catalogo Maestro <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;" id="carNom">Cargar de archivo Nom</a></li>
                            <li><a href="javascript:;" id="catNom">Consulta de registros Nom</a></li>
                        </ul>
                    </li>
-->                    <li>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:;" id="rActivo">Cat�logo Maestro Activo </a></li>
                            <li><a href="javascript:;" id="rCompras">Cat�logo Maestro/Compras</a></li>
                            <li><a href="javascript:;" id="rErrores"> Reporte de Errores</a></li>
                        </ul>
                    </li>
                    <li class="navbar-tex navbar-right">
                        <a href="javascript:;" id="salir"><i class="fa  fa-power-off"></i> Salir</a>
                    </li>
                    <li class="navbar-tex navbar-right">
                        <a href="javascript:;">Usuario: <span id="nombreUsuario" style="font-weight: bold"><%=nombre%></span></a>
                    </li> 
                </ul> 
            </div>
        </nav>
    </div>

</div>


<script src="static/js/menuGeneral.js"></script>
<% }%>