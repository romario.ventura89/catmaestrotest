

<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>
<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Carga Masiva</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>


<div class="row">
    <form id="cargaMasiva" name="cargamasiva"
          enctype="multipart/form-data" method="post" role="" class="form-horizontal">

        <div class="form-group">
            <label class="col-md-2 text-right">Fecha de pedido:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="fechaPedido" class="form-control" readonly="readonly"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 text-right"  for="archivo">Archivo de carga masiva:</label> 
            <div class="col-md-8">
                <input class=" col-md-12"
                       name="archivo" id="archivo" type="file"
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                       style="padding: 10px;" />
            </div>
        </div>

        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-danger" id="cargar">Cargar</button>
        </div>

    </form>
</div>

<script src="static/js/carga_masiva.js"></script>

<% }%>