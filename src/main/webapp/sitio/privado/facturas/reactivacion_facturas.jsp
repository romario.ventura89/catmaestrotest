<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Reactivación Facturas</p>
    </div>
</div>
<div class="row">
    <hr class="rojohr"/>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Filters</label>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group confirmaciones" >
                        <label class="col-md-2 text-right">Invoice:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="factura" class="form-control"/>
                        </div>
                        <label class="col-md-2 text-right">Date:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="fechaConfirmacion" class="form-control" readonly="reaonly"/>
                        </div>
                        <label class="col-md-2 text-right">Product code:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="sku" class="form-control"/>
                        </div>

                    </div>

                </div>
                <div class="form-horizontal">
                    <div class="form-group confirmaciones" >

                        <label class="col-md-2 text-right">Container:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="Container" class="form-control"/>
                        </div>
                        <label class="col-md-2 text-right">ETD:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="etd" class="form-control" readonly="reaonly"/>
                        </div>
                        <label class="col-md-2 text-right">ETA:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="eta" class="form-control" readonly="reaonly"/>
                        </div>
                    </div>
                </div>
      
                <div class="panel-footer text-center">
                    <button type="button" class="btn btn-default" id="limpiar">Clear</button>
                    <button type="button" class="btn btn-danger" id="buscar">Search</button>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12" >
        <table id="tablaReporteConfirmacion" class="table-hover display" >
            <thead id="encabezadoFacturas">

            </thead>
            <tbody id="cuerpoTabla">
            </tbody>
        </table>
    </div>
</div>
<div class="modal body" tabindex="-1" role="dialog" id="modalDetalle" >
    <div class="modal-dialog" role="document" style="width: 75%">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Container: <span id="detalleContenedor"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div  class="col-md-12">
                        <table id="tablaFactura" class="table table-hover display" >
                            <thead>
                                <tr>
                                    <th>Sku</th>
                                    <th>Description</th>
                                    <th>Pieces</th>
                                    <th>Unit price</th>
                                    <th>Total price</th>
                                </tr>
                            </thead>
                            <tbody id="cuerpoTablaFactura"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>   
<div class="modal body" tabindex="-1" role="dialog" id="modalReactivar" >
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reactivar Factura</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12">
                        <h3>¿Está seguro que desea reactivar la factura 
                            "<span id="reactivarFacturaTexto"></span>" que contiene el contenedor 
                            "<span id="raactivarContenedorTexto"></span>"?</h3>
                        <div class="form-group">
                            <label>No Order</label>
                            <input type="text" class="form-control" id="noOrden" />
                        </div>


                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" id="aceptarReactivar">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script src="static/js/factura/reactivacion_facturas.js"></script>
<% }%>