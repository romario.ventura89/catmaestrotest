<%-- 
    Document   : carga_masiva
    Created on : 20/03/2018, 07:59:54 PM
    Author     : e5-523
--%>
<script>
    $("body").show();
</script>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Carga Masiva de Factura</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>


<div class="row">
    <form id="cargaMasiva" name="cargamasiva" id="cargamasiva"
          enctype="multipart/form-data" method="post" role="" class="form-horizontal">

        <div class="form-group">
            <label class="col-md-2 text-right">Date invoice:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="fecha" name="fecha" class="form-control" readonly="readonly"/>
            </div>

            <label class="col-md-2 text-right">Order:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="numeroOrden" name="numeroOrden" class="form-control"/>
            </div>

            <label class="col-md-2 text-right">Invoice:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="numeroFactura" name="numeroFactura" class="form-control"/>
            </div>



        </div>
        <div class="form-group">
            <label class="col-md-2 text-right">Container:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="contenedor" name="contenedor" class="form-control"/>
            </div>     
            <label class="col-md-2 text-right">ETD:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="etd" name="etd" class="form-control" readonly="readonly"/>
            </div>
            <label class="col-md-2 text-right">ETA:</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="eta" name="eta" class="form-control" readonly="readonly"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 text-right"  for="archivo">File excel invoice:</label> 
            <div class="col-md-8">
                <input class=" col-md-12"
                       name="archivo" id="archivo" type="file"
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                       style="padding: 10px;" />
            </div>
        </div>

        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-danger" id="cargar">Upload</button>
        </div>

    </form>
</div>


<div class="row" id="panelResultados" style="display: none;margin-top: 30px" >
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Resultados</label>
            </div>
            <div class="panel-body">
                <h1 id="resultadoCarga"></h1>
                <h3>Date invoice: <span id="dateRes"></span></h3>
                <h3>Order: <span id="orderRes"></span></h3>
                <h3>Invoice: <span id="invoiceRes"></span></h3>
                <h3>Container: <span id="containerRes"></span></h3>
                <h3>ETD: <span id="etdRes"></span></h3>
                <h3>ETA: <span id="etaRes"></span></h3>
                <h5 id="datosRes"></h5>

                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Product code</th>
                            <th>description</th>
                            <th>piece</th>
                            <th>price</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyRes">

                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>

<script src="static/js/factura/carga_masiva.js"></script>