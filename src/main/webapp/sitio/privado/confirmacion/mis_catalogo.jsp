
<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Mis Confirmaciones</p>
    </div>
</div>
<div class="row">
    <hr class="rojohr"/>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Filters</label>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group confirmaciones" >

                        <label class="col-md-2 text-right">Codigo de Barras:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="cBarras" class="form-control">
                        </div>
                        <label class="col-md-2 text-right">Date:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="fechaIngreso" class="form-control" readonly="readonly"/>
                        </div>
                        <label class="col-md-2 text-right">SKU:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="sku" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-center">
                    <button type="button" class="btn btn-default" id="limpiar">Clear</button>
                    <button type="button" class="btn btn-danger" id="buscar">Search</button>

                </div>



            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12" >
        <table id="tablaReporteConfirmacion" class=" table-hover display" >
            <thead id="titulosConfirmaciones">

            </thead>
            <tbody id="cuerpoTabla">
            </tbody>
        </table>
    </div>
</div>
<div class="modal body" tabindex="-1" role="dialog" id="modalDetalle" >
    <div class="modal-dialog" role="document" style="width: 75%">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detalle Order: <span id="detalleNumOrden"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div id="contenidoDetalle" class="col-md-12"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>   


<div class="modal body" tabindex="-1" role="dialog" id="modalEliminar" >
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Eliminación de Confirmación</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12">
                        <h3>¿Está seguro que desea eliminar la confirmación con la Order 
                            "<span id="eliminarOrdenTexto"></span>", Type Order "<span id="eliminarTipoTexto"></span>"?</h3>
                        <h4>Se le informa que está acción eliminará las facturas que tienen relación de forma permanente.</h4>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" id="aceptarEliminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>   
<script src="static/js/catalogo/mi_catalogo.js"></script>

<% }%>