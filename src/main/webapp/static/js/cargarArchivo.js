
$(document).ready(function() {
    $("#h2Subtitulo").html("Carga de prelaci&oacute;n");
    $("#ligaopc").html("Carga de prelaci&oacute;n");
    $("#ligaopc").show(); 
	agregaEventoCargarArchivo();
//	cargaCatalogoCicloEscolar();	
});

/*######### Ejecución de eventos #########*/
function agregaEventoCargarArchivo() {
	$("#btnCargar").on("click", function(e) {
//	    limpiaErrores();
	    if(validaCargaArchivo())
	    {
                alert("true");
                ejecutaCargaArchivo();
//		$("#procesando").hide();
//		$(".cargaModal").show();
//		$("#modalCargaArchivo").modal();
	    }
	});
	
//	$("#btnCargarArchivo").on("click", function(e) {
//		ejecutaCargaArchivo();
//	});
	
//	$("#cerrarModal").on("click", function(e) {
//		$("#procesando").hide();
//		$(".cargaModal").show();
//	});
}

/*######### Carga del catálogo de ciclo escolar #########*/
function cargaCatalogoCicloEscolar() {
	var vURL = 'service/archivo/cargar';
	conectaAjax(vURL, "", resCargaCatalogoCicloEscolar);
}

function resCargaCatalogoCicloEscolar(data) {
	llenaComboCicloEscolar(data);
}

function llenaComboCicloEscolar(datos) {
	$('#slcCicloEscolar').empty();
	var opcionCicloEscolar = $('#slcCicloEscolar');
	for (var i = 0; i < datos.length; i++) {
		var option = $('<option/>');
		option.attr("value", datos[i].idCicloEscolar);
		option.text(datos[i].cicloEscolar);
		opcionCicloEscolar.append(option);
	}
}

/*######### Funcionalidad del archivo para envíar al servidor #########*/
function ejecutaCargaArchivo() {
//	limp
//	iaErrores();
    alert("ejecutaCargaArchivo");
            var formEnvio = document.getElementById("enviaArchivoChina");
            var formData = new FormData(formEnvio);
            var vURL = "service/cargarArchivo";
            
//            $("#procesando").show();
//            $(".cargaModal").hide();
            enviarArchivo(vURL,formData);
	
}

function validaCargaArchivo() {
	var componenteArchivoDeCarga = $("#inputFileExcelCarga");
	var idMsmError = "#divErrorPagina";
	var msmErrorNulo = "<strong>¡Error de registro!</strong> no ha llenado varios campos requeridos. Por favor verifique.";
	var msmErrorFormato = "El formato de archivo aceptado es: <strong>.xlsx</strong>";
	
	if (componenteArchivoDeCarga.val() == "") {
		cambiaBorderComponenteIncorrecto();
		creaMensajeError(idMsmError,msmErrorNulo);
		$(".obligatorio").show();
		return false;
	}
	else if (componenteArchivoDeCarga.val().lastIndexOf(".xlsx") <= 0) {
		cambiaBorderComponenteIncorrecto();
		creaMensajeError(idMsmError,msmErrorFormato);
		return false;
	}
	return true;
}

/* ######### Utilidades ############ */
function limpiaErrores()
{
//    alert("limpiarErrores ");
//	$("#divErrorPagina").hide();
//	$("#divErrorPagina").children("div").html("");
//	$(".obligatorio").hide();
//	cambiaBorderComponenteCorrecto();
//	$("#divExitoPagina").hide();
//	$("#procesando").hide();
//	$(".cargaModal").show();
}

function creaMensajeError(id,msm)
{
	$(id).children("div").html(msm);
	$(id).show();
}

function cambiaBorderComponenteIncorrecto(){

	$("#inputFileExcelCarga").css("border", "1px solid #D0021B");
}

function cambiaBorderComponenteCorrecto(){
	$("#inputFileExcelCarga").css("border", "");
}


/* ####### Implementación de Ajax ######### */

function conectaAjax(url, datos, metodo) {
	$.ajax({
		url : url,
		type : 'POST',
		contentType : 'application/json',
		data : JSON.stringify(datos),
		dataType : "json",
		cache : false,
		statusCode : {
			408 : function(data) {
			}
		},
		success : function(data) {
			metodo(data);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			showModalMessageAccept(jqXHR.responseText);
		}
	});
}


function cargaPagina(id,path)
{
	$(id).load(path);
}

var jsonLog = "";

function enviarArchivo(ruta, formData) {
    alert("enviar archivo ");
	var idMsmError = "#divErrorPagina";
	var xhr = new XMLHttpRequest();
	xhr.open('POST', ruta, true);
	xhr.responseType = 'blob';


	xhr.onload = function(e) {
            alert("onload ");
//		$("#cerrarModal").click();
		if(this.status == 200)
		{
                    alert("status..." +this.status);
			var text =  blobToString(this.response);
			
			$("#divExito").html('<span id="numReg">Se cargo el archivo</span> correctamente.</div>')
//			agregaNumeroRegistroXSustentante(text);
			$("#divExitoPagina").show();
		}
		else if (this.status == 203) {
	    	var msmErrorFormato = "<strong>¡Error de archivo!</strong> el archivo de prelación no contiene registros, por favor verifique el archivo de prelación.";
	    	descargaArchivoExcel(this.response);
	    	creaMensajeError(idMsmError,msmErrorFormato);
	    }else  if (this.status == 207) {
                alert("207");
	    	var msmErrorFormato = "<strong>¡Error de archivo!</strong> el archivo de prelación contiene información no válida, por favor verifique el archivo de prelación.";
	    	descargaArchivoExcel(this.response);
	    	creaMensajeError(idMsmError,msmErrorFormato);
	    } else  if (this.status == 206) {
                alert("206");
		var text =  blobToString(this.response);
		creaMensajeError(idMsmError,"<strong>¡Error de archivo!</strong> " + text);
	    }
	    else{
                alert("else");
	    	var msmErrorFormato = "No se pudo realizar la carga del archivo de prelación, por favor intente más tarde.";
	    	creaMensajeError(idMsmError,msmErrorFormato);
	    }
	};

	xhr.send(formData);
	
}

function blobToString(b) {
    var u, x;
    u = URL.createObjectURL(b);
    x = new XMLHttpRequest();
    x.open('GET', u, false); // although sync, you're not fetching over internet
    x.send();
    URL.revokeObjectURL(u);
    return x.responseText;
}

function resCargarArchivoPrelacionNumReg(data)
{
	agregaNumeroRegistroXSustentante(data);
}

function agregaNumeroRegistroXSustentante(numReg){
	var res = "";
	if(numReg == 1)
	{
		res = "Se insertó un sustentante"
	}else
	{
		res = "Se insertaron " + numReg + " sustentantes"
	}
	$("#numReg").html(res);
}

function descargaArchivoExcel(response)
{
	 var blob = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
     var downloadUrl = URL.createObjectURL(blob);
     var a = document.createElement("a");
     a.href = downloadUrl;
     a.download = $("#inputFileExcelCarga").val().replace(/.*(\/|\\)/, '');;
     document.body.appendChild(a);
     a.click();
}