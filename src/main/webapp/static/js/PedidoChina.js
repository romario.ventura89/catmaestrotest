var pedidos = "";
$(document).ready(function () {

    eventos();

    function eventos() {
        $("#enviarExcel").click(function () {
             cargarArchivo();
        });
        $("#prueba").click(function (){
            pruebaExcel();
        });
        
        $("#salir").click(function () {
            salir();
        });

        $("#facturar").click(function () {
            consultaTicket();
        });

        $("#ticket").keypress(function (event) {
            var keycode = event.keyCode || event.which;
            if (keycode == '13') {
                consultaTicket();
            }
        });
    }
    
    function pruebaExcel(){
        var path = "service/cargarArchivo/archivo";
        conectaPost(path, "", "");
    }
    
    function llenarPedidos() {
    var resHTML = "";
    for (var i = 0; i < pedidos.length; i++)
    {
        var pedido = pedidos[i];
        resHTML += "<tr>";
        resHTML += "<td style='text-align: right;'>";
        resHTML += (i + 1);
        resHTML += "</td>";

        resHTML += "<td>";
        resHTML += pedido.sku;
        resHTML += "</td>";

        resHTML += "<td style='text-align: justify;'>";
        resHTML += pedido.piezas;
        resHTML += "</td>";

        resHTML += "<td class='text-center'>";
        resHTML += '<button type="button" id="delDes-' + i + '"  class="delete btn btn-danger" data-toggle="tooltip" title="Eliminar" data-placement="left"><i class="fa fa-times"></i></button>';
        resHTML += "</td>";


    }
    $("#cuerpoControlVentas").html(resHTML);

    $(".delete").tooltip();

//    $(".delete").click(function (e) {
//        e.preventDefault();
//        var miID = this.id;
//        miID = miID.replace("delDes-", "");
//        datosCatalogo = pedidos[miID * 1];
//
//        $("#causaModal").html(datosCatalogo.causa);
//        $("#modalDesperfectos").modal();
//    });

    tabla = $("#tablaControlVentas").DataTable({
      
    });
    
}
    
    function salir(){
        var path = "service/login/salir";
        window.location.href = path;
    }
    
    function cargarArchivo() {
        if(validaFormulario()){
            var path = "service/producto/guardar";
            var datos = {
            sku: $("#sku").val(),
            fechaRegistro: $("#fechaRegistro").val(),
            piezas: $("#piezas").val()
        };
        conectaPost(path, datos, returnRegistro);
        }
    }
    
    function  returnRegistro(data) {
      mensajeExito("El SKU se agrego correctamente.");
      mensajeExito("La fecha se agrego correctamente.");
      mensajeExito("Las piezas se agregaron correctamente.");
      pedidos = data;
      llenarPedidos();
      
      
    }
    
 

    function validaFormulario() {
        if($("#archivo").val().trim() == "" ){
            mensajeError("Recuerda cargar el achivo de respuesta");
            return false;
        }
        if($("#fechaRegistro").val().trim() == "" ){
            mensajeError("Recuerda capturar la fecha de registro");
            return false;
        }
        return true;
    }

    function validaInformacion() {
        if ($("#ticket").val().length < 1) {
            mensajeError("Favor de capturar su ticket.");
            return false;
        }
        if ($("#txtCaptcha").val().length < 1) {
            mensajeError("Favor de capturar el código de la imagen.");
            return false;
        }
        return true;
    }
    
    
    $("#ticket").focus();


    function consultaTicket() {
        if (validaInformacion())
        {
            var path = "service/registro/solicitarFactura";
            var datos = {
                ticket: $("#ticket").val(),
                captcha: $("#txtCaptcha").val()
            };
            conectaPost(path, datos, function(data){
                loadPage("#cuerpo","sitio/privado/datosFactura.jsp");
            });
        }

    }

});

function obtenCaptcha() {
    var url = 'service/captcha/generar?servicio=index';
    obtenImagenBase64(url, obtenCaptchaRes);
}

function obtenCaptchaRes(data)
{
    if (!cargadoCaptcha)
    {
        cargadoCaptcha = true;
        $("#refresh").click();
    } else
    {
        $("#captcha").attr("src", "data:image/png;base64," + data);
    }
}

$(document).ready(function () {
    $("#fechaRegistro").datepicker({
        firstDay: 1,
        dateFormat: 'dd/mm/yy'
    });
});

var cargadoCaptcha = false;



function comunRes(data)
{
    jsonLog = data;
}



