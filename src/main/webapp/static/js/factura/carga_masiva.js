$(document).ready(function () {
    $("#fecha").datepicker({language: 'es'});
    $("#etd").datepicker({language: 'es'});
    $("#eta").datepicker({language: 'es'});

    eventos();

    function eventos() {
        $("#cargar").click(function () {
            if (validaCargaArchivo())
            {
                ejecutaCargaArchivo();
            }
        });
    }

    function validaCargaArchivo() {
        var componenteArchivoDeCarga = $("#archivo");
        var msmErrorNulo = "Favor de seleccionar el archivo de carga masiva.";
        var msmErrorFormato = "El formato de archivo aceptado es: <strong>.xlsx</strong>";
        if ($("#fecha").val() == "")
        {
            mensajeError("Favor de seleccionar date.");
            return false;
        }

        if ($("#numeroOrden").val() == "")
        {
            mensajeError("Favor de ingresar el order");
            return false;
        }

        if ($("#numeroFactura").val() == "")
        {
            mensajeError("Favor de ingresar el invoice");
            return false;
        }

        if ($("#contenedor").val() == "")
        {
            mensajeError("Favor de ingresar el container");
            return false;
        }

        if ($("#etd").val() == "")
        {
            mensajeError("Favor de ingresar el etd");
            return false;
        }

        if ($("#eta").val() == "")
        {
            mensajeError("Favor de ingresar la eta");
            return false;
        }

        if (componenteArchivoDeCarga.val() == "") {
            mensajeError(msmErrorNulo);

            return false;
        } else if (componenteArchivoDeCarga.val().lastIndexOf(".xlsx") <= 0) {
            mensajeError(msmErrorFormato);
            return false;
        }

        return true;
    }

    function ejecutaCargaArchivo() {

        var formData = new FormData();
        formData.append('fechaFactura', $("#fecha").val());
        formData.append('numeroOrden', $("#numeroOrden").val())
        formData.append('numeroFactura', $("#numeroFactura").val());
        ;
        formData.append('contenedor', $("#contenedor").val());
        formData.append('etd', $("#etd").val());
        formData.append('eta', $("#eta").val());

        formData.append('archivo', $("#archivo")[0].files[0]);
        var vURL = "service/cargaArchivo/factura";
        enviarArchivo(vURL, formData);
    }

    function enviarArchivo(ruta, formData) {
        $("#loader").show();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', ruta, true);
        xhr.responseType = 'blob';


        xhr.onload = function (e) {
            $("#loader").hide();
            if (this.status == 200)
            {
                var text = blobToString(this.response);
                cargaResultadosExitosos(JSON.parse(text));
                mensajeExito("Se cargo el archivo correctamente");
            } else if (this.status == 203) {
                var text = blobToString(this.response);
                cargaResultadosFallidos(JSON.parse(text));
            } else if (this.status == 207) {
                mensajeError("El archivo contiene errores, favor de verificar");
            } else if (this.status == 206) {
                var text = blobToString(this.response);
                mensajeError("Error de archivo " + text);
            } else {
                var msmErrorFormato = "No se pudo realizar la carga del archivo, por favor intente más tarde.";
                mensajeError(msmErrorFormato);
            }
        };

        xhr.send(formData);

    }

    function descargaArchivoExcel(response)
    {
        var blob = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
        var downloadUrl = URL.createObjectURL(blob);
        var a = document.createElement("a");
        a.href = downloadUrl;
        a.download = $("#archivo").val().replace(/.*(\/|\\)/, '');
        document.body.appendChild(a);
        a.click();
    }

    function blobToString(b) {
        var u, x;
        u = URL.createObjectURL(b);
        x = new XMLHttpRequest();
        x.open('GET', u, false); // although sync, you're not fetching over internet
        x.send();
        URL.revokeObjectURL(u);
        return x.responseText;
    }

    function cargaResultadosExitosos(resultado) {
        $("#resultadoCarga").html("Factura cargada correctamente").addClass("bg-success").removeClass("bg-danger");
        $("#dateRes").html(resultado.fechaFactura);
        $("#orderRes").html(resultado.numeroOrden);
        $("#invoiceRes").html(resultado.numeroFactura);
        $("#containerRes").html(resultado.contenedor);
        $("#etdRes").html(resultado.etd);
        $("#etaRes").html(resultado.eta);

        var resHTML = "";
        resHTML += llenaRegistro(resultado.datos);
        $("#datosRes").html("");

        $("#tbodyRes").html(resHTML);

        $("#panelResultados").show();

    }

    function cargaResultadosFallidos(resultado) {
        $("#resultadoCarga").html("Factura con errores ").addClass("bg-danger").removeClass("bg-success");

        var fechaFacturaErrorTXT = "";
        if (resultado.fechaFacturaErrorTXT != null)
        {
            fechaFacturaErrorTXT = resultado.fechaFacturaErrorTXT;
        }
        $("#dateRes").html(resultado.fechaFactura + " " + fechaFacturaErrorTXT);

        var numeroOrdenErrorTXT = "";
        if (resultado.numeroOrdenErrorTXT != null)
        {
            numeroOrdenErrorTXT = resultado.numeroOrdenErrorTXT;
        }
        $("#orderRes").html(resultado.numeroOrden + " " + numeroOrdenErrorTXT);

        var numeroFacturaErrorTXT = "";
        if (resultado.numeroFacturaErrorTXT != null)
        {
            numeroFacturaErrorTXT = resultado.numeroFacturaErrorTXT;
        }
        $("#invoiceRes").html(resultado.numeroFactura + " " + numeroFacturaErrorTXT);

        var contenedorErrorTXT = "";
        if (resultado.contenedorErrorTXT != null)
        {
            contenedorErrorTXT = resultado.contenedorErrorTXT;
        }
        $("#containerRes").html(resultado.contenedor + " " + contenedorErrorTXT);

        var etdErrorTXT = "";
        if (resultado.etdErrorTXT != null)
        {
            etdErrorTXT = resultado.etdErrorTXT;
        }
        $("#etdRes").html(resultado.etd + " " + etdErrorTXT);

        var etaErrorTXT = "";
        if (resultado.etaErrorTXT != null)
        {
            etaErrorTXT = resultado.etaErrorTXT;
        }
        $("#etaRes").html(resultado.eta + " " + etaErrorTXT);

        if (resultado.errorListado)
        {
            $("#datosRes").html("El archivo no es válido o no contiene información");
        } else {
            var resHTML = "";
            resHTML += llenaRegistroError(resultado.datos);

            $("#tbodyRes").html(resHTML);
        }




        $("#panelResultados").show();
    }


    function llenaRegistro(lista)
    {
        var resHTML = "";
        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";


            resHTML += "<td>";
            resHTML += registro.sku;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.nombre;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.piezas;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.precio;
            resHTML += "</td>";

            resHTML += "</tr>";
        }

        return resHTML;
    }

    function llenaRegistroError(lista, tipo)
    {
        var resHTML = "";

        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";


            resHTML += "<td>";
            resHTML += registro.sku;
            resHTML += "</td>";


            resHTML += "<td>";
            resHTML += registro.nombre;
            resHTML += "</td>";

            if (registro.piezasError)
            {
                resHTML += "<td class='bg-danger'>";
                resHTML += registro.piezas + " " + registro.piezasErrorTxt;
                resHTML += "</td>";
            } else {
                resHTML += "<td>";
                resHTML += registro.piezas;
                resHTML += "</td>";
            }

            if (registro.precioError)
            {
                resHTML += "<td class='bg-danger'>";
                resHTML += registro.precio + " " + registro.precioErrorTxt;
                resHTML += "</td>";
            } else {
                resHTML += "<td>";
                resHTML += registro.precio;
                resHTML += "</td>";
            }

            resHTML += "</tr>";
        }

        return resHTML;
    }
});


