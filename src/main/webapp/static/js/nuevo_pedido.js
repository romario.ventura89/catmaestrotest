var nuevoPedido = "";
var tabla = "";
var productoSel = "";
var numPedidos = "";

$(document).ready(function () {

    $("#fechaPedido").datepicker();
    eventos();

    function eventos() {

        $("#guardaSKU").click(function () {
            guardaProducto();
        });

        $("#descargarPedido").click(function () {
            exportaExcel();
        });

        $("#fechaPedido").change(function () {
            obtenProductos();
        });

        $("#eliminarProductoPedido").click(function () {
            var path = "service/producto/eliminarProducto";
            var datos = {
                idPedidoProducto: productoSel.idProducto
            };
            conectaPost(path, datos, function (data) {
                nuevoPedido = data;
                mensajeExito("El producto " + productoSel.sku + " se elimin&oacute; del pedido correctamente.");
                $("#modalEliminar").modal('toggle');
                obtenProductos();
            });

        });
    }

    function guardaProducto() {
        if ($("#fechaPedido").val() == "")
        {
            mensajeError("Favor de seleccionar una fecha de pedido.");
            return "";
        }
        var path = "service/producto/guardar";
        var datos = {
            fechaPedido: $("#fechaPedido").val(),
            productoNuevo: {
                sku: $("#sku").val(),
                cantidad: $("#piezas").val()
            }

        };

        conectaPost(path, datos, function (data) {
            limpiar();
            nuevoPedido = data;
            $("#botonExcel").show();
            $("#panelProductos").show();
            llenarPedidos();
        });

    }

    function obtenProductos() {
        var path = "service/pedido/obtenProductos";
        var datos = {
            fechaPedido: $("#fechaPedido").val()
        };

        conectaPost(path, datos, function (data) {
            nuevoPedido = data;

            if (!nuevoPedido.nuevo)
            {

                $(".productos").show();
                limpiar();
                $("#panelProductos").show();
                llenarPedidos();

                if (nuevoPedido.enviado)
                {
                    $(".productos").hide();

                }


            } else
            {
                $(".productos").show();
                limpiar();
                $("#panelProductos").hide();
                $("#cuerpoControlVentas").html("");
            }
        });
    }

    function nuevoRegistro() {
        var path = "service/pedido/contar";
        conectaPost(path, "", redirectoToNew);
    }

    function redirectoToNew(data) {
        numPedidos = data;
        $("#numPedido").val(data);
    }

    function buscaPedidos() {
        loadPage("#cuerpo", "service/producto/pedidos");
    }

    function exportaExcel() {
        $("#descargarPedido").attr("href", "service/descagasExcel/pedido");
        loadPage("#cuerpo", "service/producto/nuevo");
    }

    function validaFormulario() {
        if ($("#sku").val().trim() == "") {
            mensajeError("Recuerda capturar el valor de SKU");
            return false;
        }
        if ($("#sku").val().trim() == "0") {
            mensajeError("El valor de SKU debe ser mayor a 0");
            return false;
        }
        if ($("#piezas").val().trim() == "") {
            mensajeError("Recuerda capturar la cantidad de piezas");
            return false;
        }
        if ($("#piezas").val().trim() == "0") {
            mensajeError("El valor de la cantidad debe ser mayor a 0");
            return false;
        }
        if ($("#fechaRegistro").val().trim() == "") {
            mensajeError("Recuerda capturar la fecha de registro");
            return false;
        }
        return true;
    }

    function llenarPedidos() {
        var resHTML = "";
        for (var i = 0; i < nuevoPedido.productos.length; i++)
        {
            var producto = nuevoPedido.productos[i];
            resHTML += "<tr>";
            resHTML += "<td style='text-align: right;'>";
            resHTML += (i + 1);
            resHTML += "</td>";

            resHTML += "<td style='text-align: justify;'>";
            resHTML += producto.sku;
            resHTML += "</td>";

            resHTML += "<td style='text-align: justify;'>";
            resHTML += producto.cantidad;
            resHTML += "</td>";

            resHTML += "<td style='text-align: justify;'>";
            resHTML += '<button type="button" id="del-' + i + '"  class="borrar btn btn-danger btn-xs productos" data-toggle="tooltip" data-placement="left"><i class="fa fa-times"></i></button>';
            resHTML += "</td>";
        }
        $("#cuerpoControlVentas").html(resHTML);
        $(".borrar").tooltip();

        $(".borrar").click(function (e) {
            e.preventDefault();
            var miID = this.id;
            miID = miID.replace("del-", "");
            productoSel = nuevoPedido.productos[miID * 1];

            $("#skuEliminar").html(productoSel.sku);
            $("#modalEliminar").modal();
        });



    }



    function limpiar() {
        $("#sku").val();
        $("#piezas").val();
    }
});



