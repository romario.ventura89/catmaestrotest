$(document).ready(function ()
{
    eventos();

    function eventos() {
        $("#inicio").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=1");
            loadPage("#menu", "service/navegacion/seleccionaMenu?idMenu=3");
        });

        $("#salir").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=2");
            loadPage("#menu", "service/navegacion/seleccionaMenu?idMenu=0");
        });
        
         $("#misConfirmaciones").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=3");
        });

        $("#cargaMasivaConfirmaciones").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=4");
        });
        $("#misFacturas").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=5");
        });
        $("#cargaMasivaFacturas").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=6");
        });
        $("#rConfirmacion").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=7");
        });

        $("#rComodato").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=8");
        });

        $("#rPedidos").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=9");
        });
        
        $("#menuReactivacion").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=10");
        });

        $("#descargaReporte").click(function () {
            leerArchivo("service/reportesGeneral/excel", "REPORTE_GENERAL.xlsx");
        });
    }
});
