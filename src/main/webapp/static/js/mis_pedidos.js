$(document).ready(function () {
    buscaPedidos();
});


function buscaPedidos() {
    var path = "service/pedido/misPedidos";
    conectaPost(path, "", function (data) {
        pedidos = data;
        llenarPedidos();
    });
}




function llenarPedidos() {
    var resHTML = "";
    for (var i = 0; i < pedidos.length; i++)
    {
        var pedido = pedidos[i];
        resHTML += "<tr>";
        resHTML += "<td style='text-align: right;'>";
        resHTML += (i + 1);
        resHTML += "</td>";

        resHTML += "<td style='text-align: justify;'>";
        resHTML += pedido.fechaPedido;
        resHTML += "</td>";

        resHTML += "<td style='text-align: justify;'>";
        if (pedido.estatus == "N")
        {
            resHTML += "En proceso";
        } else if (pedido.estatus == "E")
        {
            resHTML += "Terminado";
        }

        resHTML += "</td>";

        resHTML += "<td >";
        resHTML += '<button type="button" id="des-' + i + '"  class="descargar btn btn-danger btn-xs" data-toggle="tooltip" data-placement="left"><i class="fa fa-cloud-download"></i></button>';
        resHTML += "</td>";

    }
    $("#cuerpoControlVentas").html(resHTML);

    $(".descargar").click(function (event) {
        event.preventDefault();
        var miID = this.id;
        miID = miID.replace("des-", "");
        var pedidoSel = pedidos[miID * 1];

        window.open('service/descagasExcel/miPedido?fechaPedido=' + pedidoSel.fechaPedido, '_blank');


    });


    tabla = $("#tablaMisPedidos").DataTable();

}


