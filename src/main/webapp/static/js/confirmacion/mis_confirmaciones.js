var datosConfirmacion = "";
var detalleProductos = "";
var confirmacionSel = "";
var tabla = "";

$(document).ready(function () {

    $("#fechaConfirmacion").datepicker({language: 'es'});
    eventos();

    function eventos() {

        $("#limpiar").click(function () {
            limpiar();
        });

        $("#buscar").click(function () {
            buscar();
        });
        
          $("#aceptarEliminar").click(function () {
            eliminaConfirmacion(confirmacionSel);
        });


    }

    function buscar() {

        var path = "service/confirmacion/buscar";
        var datos = {
            numeroOrden: $("#noOrden").val(),
            sku: $("#sku").val(),
            fecha: $("#fechaConfirmacion").val()
        };

        conectaPost(path, datos, function (data) {
            $("#cuerpoTabla").html("");
            datosConfirmacion = data;
            $("#botonExcel").show();
            $("#panelConfirmacion").show();
            llenarConfirmaciones();
        });

    }


    function llenarConfirmaciones() {
        var resHTML = "";
        for (var i = 0; i < datosConfirmacion.length; i++)
        {
            var conf = datosConfirmacion[i];
            resHTML += "<tr>";
            resHTML += "<td style='text-align: justify;'>";
            resHTML += conf.numeroOrden;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += conf.tipoOrden;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += conf.fecha;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += conf.productos;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += conf.solicitadas;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += conf.entregadas;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += conf.total;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="detalle-' + i + '"  class="detalle btn btn-danger btn-xs confirmaciones" data-toggle="tooltip" data-placement="left">detail</button>';
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="exc-' + i + '"  class="btn btn-success btn-xs excel"  style="margin-right:5px;" >Excel</button>';
            resHTML += '<button type="button" id="xml-' + i + '"  class="btn btn-info btn-xs xml" style="margin-left:5px;">XML</button>';
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="eli-' + i + '"  class="delete btn btn-danger btn-xs">Eliminar</button>';
            resHTML += "</td>";
        }
        if (tabla != "")
        {
            tabla.destroy();
        }
        $("#cuerpoTabla").html(resHTML);

        $(".detalle").click(function (e) {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("detalle-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            obtenDetalle(confirmacionSel);
        });

        $(".delete").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("eli-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            $("#modalEliminar").modal();
            $("#eliminarOrdenTexto").html(confirmacionSel.numeroOrden);
            $("#eliminarTipoTexto").html(confirmacionSel.tipoOrden);

        });

        $(".xml").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("xml-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            obtenXML(confirmacionSel);
        });

        $(".excel").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("exc-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            obtenExcel(confirmacionSel);
        });

      

        var filtros = [];

        $("#titulosConfirmaciones").html(creaEncabezados());
        tabla = crearDatatableConfirmaciones("#tablaReporteConfirmacion", filtros);

    }

    function creaEncabezados() {
        var resHTML = "<tr>";
        resHTML += "<th style='background:white'>Order</th>";
        resHTML += "<th style='background:white'>Type order</th>";
        resHTML += "<th>Date</th>";
        resHTML += "<th>Total products</th>";
        resHTML += "<th>Total ordered quantity</th>";
        resHTML += "<th>Total final shipped quantity</th>";
        resHTML += "<th>Total cost</th>";
        resHTML += "<th class='text-center'>Details</th>";
        resHTML += "<th class='text-center'>Downloads</th>";
        resHTML += "<th class='text-center'>Acción</th>";
        resHTML += "</tr>";
        return resHTML;
    }

    function obtenXML(confirmacion)
    {
        var path = "service/confirmacion/xml?idConfirmacion=" + confirmacion.idConfirmacion + "&idTipoOrden=" + confirmacion.idTipoOrden;
        window.open(path, '_blank');
    }

    function obtenExcel(confirmacion) {
        var path = "service/confirmacion/excel?idConfirmacion=" + confirmacion.idConfirmacion + "&idTipoOrden=" + confirmacion.idTipoOrden;
        window.open(path, '_blank');
    }

    function eliminaConfirmacion(confirmacion) {

        var path = "service/confirmacion/eliminar";
        conectaPost(path, confirmacion, function (data)
        {
            mensajeExito("Se realizó la eliminación correctamente.");
            $("#modalEliminar").modal('toggle');
            buscar();
        });
    }

    function obtenDetalle(confirmacion)
    {
        var path = "service/confirmacion/detalle";
        conectaPost(path, confirmacion, function (data)
        {
            detalleProductos = data;

            creaDetalle();
            $("#modalDetalle").modal();
        });
    }

    function creaDetalle() {

        $("#detalleNumOrden").html(confirmacionSel.numeroOrden);
        var resHTML = "";
        var resHTMLGeneral = titulosTablaTabs("tablaGeneral");
        var resHTMLPingshan = titulosTablaTabs("tablaPingshan");
        var resHTMLQianhai = titulosTablaTabs("tablaQianhai");
        var general = false;
        var pingshan = false;
        var qianhai = false;


        for (var i = 0; i < detalleProductos.length; i++) {
            var detalle = detalleProductos[i];
            if (detalle.general) {
                resHTMLGeneral += llenaCuarpoTab(detalle);
                general = true;
            }
            if (detalle.pingshan) {
                resHTMLPingshan += llenaCuarpoTab(detalle);
                pingshan = true;
            }
            if (detalle.qianhai) {
                resHTMLQianhai += llenaCuarpoTab(detalle);
                qianhai = true;
            }
        }
        resHTMLGeneral += "</tbody></table>";
        resHTMLPingshan += "</tbody></table>";
        resHTMLQianhai += "</tbody></table>";

        resHTML += '<ul class="nav nav-tabs" role="tablist">';
        if (general)
        {
            resHTML += '<li role="presentation" class="active" ><a href="#tabGeneral" aria-controls="tabGeneral" role="tab" data-toggle="tab">General</a></li>';
        }
        if (pingshan)
        {
            resHTML += '<li role="presentation"><a href="#tabPingshan" aria-controls="tabPingshan" role="tab" data-toggle="tab">Pingshan</a></li>';
        }
        if (qianhai)
        {
            resHTML += '<li role="presentation"><a href="#tabQianhai" aria-controls="tabQianhai" role="tab" data-toggle="tab">Qianhai</a></li>';
        }
        resHTML += '</ul>';

        resHTML += '<div class="tab-content" style="margin-top:10px">';
        if (general)
        {
            resHTML += '<div role="tabpanel" class="tab-pane active" id="tabGeneral">1</div>';
        }
        if (pingshan)
        {
            resHTML += '<div role="tabpanel" class="tab-pane active" id="tabPingshan">2</div>';
        }
        if (qianhai)
        {
            resHTML += '<div role="tabpanel" class="tab-pane active" id="tabQianhai">3</div>';
        }
        resHTML += '</div>';

        $("#contenidoDetalle").html(resHTML);
        if (general)
        {

            $("#tabGeneral").html(resHTMLGeneral);
            $("#tablaGeneral").DataTable();
        }
        if (pingshan)
        {
            $("#tabPingshan").html(resHTMLPingshan);
            $("#tablaPingshan").DataTable();
        }
        if (qianhai)
        {
            $("#tabQianhai").html(resHTMLQianhai);
            $("#tablaQianhai").DataTable();
        }


    }

    function titulosTablaTabs(id) {
        var resHTML = "<table class='table table-striped' id='" + id + "'>";
        resHTML += "<thead><tr>";
        resHTML += "<th>product code</th>";
        resHTML += "<th>product name</th>";
        resHTML += "<th>price</th>";
        resHTML += "<th>ordered quantity</th>";
        resHTML += "<th>final shipped quantity</th>";
        resHTML += "<th>total price</th>";

        resHTML += "</tr></thead>";
        resHTML += "<tbody>";
        return resHTML;
    }

    function llenaCuarpoTab(detalle)
    {
        var resHTML = "";
        if (detalle.notOrder)
        {
            resHTML += '<tr style="background-color: #fcf8e3">';
        } else
        {
            resHTML += '<tr>';
        }

        resHTML += '<td>';
        resHTML += detalle.sku;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.descripcion;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.precioUnitario;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.piezasSolicitadas;
        resHTML += '</td>';

        resHTML += '<td>';
        if (detalle.notOrder)
        {
            resHTML += "Not order";
        } else {
            resHTML += detalle.piezasEntregadas;
        }
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.precioTotal;
        resHTML += '</td>';
        resHTML += '</tr>';
        return resHTML;
    }

    function limpiar() {
        $("#noOrden").val("");
        $("#sku").val("");
        $("#fechaConfirmacion").val("");
    }
});



