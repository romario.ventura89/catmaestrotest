$(document).ready(function () {


    eventos();

    function eventos() {

        $("#descargarReporteConfirmacion").click(function () {
            var path = "service/reportes/excelConfirmacionSesion";
            conectaPost(path, confirmacionR, function (data) {
                leerArchivo("service/reportes/excelConfirmacion","REPORTE_CONFIRMACION.xlsx");
              
            });
        });
        
      
    }

    buscaPedidosConfirmacion();
    function buscaPedidosConfirmacion() {
        var path = "service/reportes/confirmacion";
        conectaPost(path, "", function (data) {
            confirmacionR = data;
            llenarPedidosConfirmacion();
        });
    }

    function llenarPedidosConfirmacion() {
        var resHTML = "";
        for (var i = 0; i < confirmacionR.length; i++)
        {
            var conf = confirmacionR[i];
            resHTML += "<tr>";
            resHTML += "<td style='text-align: right;'>";
            resHTML += (i + 1);
            resHTML += "</td>";
            resHTML += "<td style='text-align: justify;'>";
            resHTML += conf.sku;
            resHTML += "</td>";
            resHTML += "<td style='text-align: justify;'>";
            resHTML += conf.productName;
            resHTML += "</td>";
            resHTML += "<td style='text-align: justify;'>";
            resHTML += conf.confirmacion;
            resHTML += "</td>";
            resHTML += "<td style='text-align: justify;'>";
            resHTML += conf.facturas;
            resHTML += "</td>";
        }

        if (confirmacionR.length == 0) {
            var mensaje = "No se encontraron pedidos confirmados, favor de intentar más tarde";
            $("#sinPedidos").html(mensaje);
        }
        $("#cuerpoReporteConfirmacion").html(resHTML);
        $(".descargar").click(function (event) {
            event.preventDefault();
            var miID = this.id;
            miID = miID.replace("des-", "");
            var pedidoConf = confirmacionR[miID * 1];
            window.open('service/descagasExcel/miPedido?fechaPedido=' + pedidoConf.fechaPedido, '_blank');
        });
        var filtros = new Array(1, 2, 3, 4);
        tabla = crearDatatable100("#tablaReporteConfirmacion", filtros);


    }
});


function crearDatatableRespaldo(id, listaFiltros)
{
    var i = 0;
    var tabla = $(id).DataTable({
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var mostrar = false;
                for (var j = 0; j < listaFiltros.length; j++)
                {
                    if (i == listaFiltros[j])
                    {
                        mostrar = true;
                        break;
                    }
                }
                i++;
                if (mostrar)
                {
                    var select = $('<br /><select><option value=""></option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }

            });
        }
    });
    return tabla;
}
