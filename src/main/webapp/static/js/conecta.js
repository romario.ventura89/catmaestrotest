function loadPage(id, path) {
    $("#loader").show();
    $(id).load(path, function () {
        $("#loader").hide();
    });
}

function loadScript(path)
{
    $.getScript(path, function (data, textStatus, jqxhr) {

    });
}


function conectaPost(path, datos, metodo) {
    $("#loader").show();
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: path,
        dataType: "json",
        data: JSON.stringify(datos),
        success: function (data) {
            respuestaExisto(metodo, data);
        },
        error: function (data) {

            if (HttpStatus.NOT_FOUND === data.status)
            {
                mensajeError("<i class='fa fa-window-close-o'></i> El recurso solicitado no se encuentra en el sitio, favor de verificar el recurso.");
            } else if (HttpStatus.NON_AUTHORITATIVE_INFORMATION === data.status ||
                    HttpStatus.NO_CONTENT === data.status ||
                    HttpStatus.CONFLIC === data.status ||
                    HttpStatus.INTERNAL_SERVER_ERROR === data.status ||
                    HttpStatus.NETWORK_AUTHENTICATION_REQUIRED === data.status)
            {
                mensajeError(data.responseText);
            } else {
                mensajeError("<i class='fa fa-plug'></i> Por el momento no podemos atender su solicitud, favor de intentar más tarde, gracias.");
            }
            $("#loader").hide();

        }
    });
}
function conectaPost(path, datos, metodo) {
    $("#loader").show();
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: path,
        dataType: "json",
        data: JSON.stringify(datos),
        success: function (data) {
            respuestaExisto(metodo, data);
        },
        error: function (data) {

            if (HttpStatus.NOT_FOUND === data.status)
            {
                mensajeError("<i class='fa fa-window-close-o'></i> El recurso solicitado no se encuentra en el sitio, favor de verificar el recurso.");
            } else if (HttpStatus.NON_AUTHORITATIVE_INFORMATION === data.status ||
                    HttpStatus.NO_CONTENT === data.status ||
                    HttpStatus.CONFLIC === data.status ||
                    HttpStatus.INTERNAL_SERVER_ERROR === data.status ||
                    HttpStatus.NETWORK_AUTHENTICATION_REQUIRED === data.status)
            {
                mensajeError(data.responseText);
            } else {
                mensajeError("<i class='fa fa-plug'></i> Por el momento no podemos atender su solicitud, favor de intentar más tarde, gracias.");
            }
            $("#loader").hide();

        }
    });
}


function obtenImagenBase64(url, metodo) {
    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        statusCode: {

            408: function (data) {
                mensajeError(jqXHR.responseText);
            },
            422: function (data) {

                metodo(data, true);
            }
        },
        success: function (data) {
            metodo(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}

function respuestaExisto(metodo, data)
{
    $("#loader").hide();
    metodo(data);
}


var HttpStatus = {
    NON_AUTHORITATIVE_INFORMATION: 203,
    NO_CONTENT: 204,
    NOT_FOUND: 404,
    CONFLIC: 409,
    INTERNAL_SERVER_ERROR: 500,
    NETWORK_AUTHENTICATION_REQUIRED: 511
};

function leerArchivo(ruta,nombre) {
   
    $("#loader").show();
    var xhr = new XMLHttpRequest();
    xhr.open('get', ruta, true);
    xhr.responseType = 'blob';
    
    xhr.onload = function (e) {
        var blob = xhr.response;
        jsonLog = e;
        descargaArchivo(blob,nombre);
        $("#loader").hide();
    };
    
     xhr.send();
}

function descargaArchivo(response,nombre)
{
    var blob = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
    var downloadUrl = URL.createObjectURL(blob);
    var a = document.createElement("a");
    a.href = downloadUrl;
    a.download = nombre;
    document.body.appendChild(a);
    a.click();
}

