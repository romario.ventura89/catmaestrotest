$(document).ready(function () {
     $("#fechaCarga").datepicker({
        language: 'es',
	dateFormat: 'dd/mm/yy'
    }).datepicker("setDate", new Date());

    eventos();

    function eventos() {
        $("#cargar").click(function () {
            if (validaCargaArchivo())
            {
                ejecutaCargaArchivo(false);
            }
        });

        $("#modalSubir").click(function () {
            if (validaCargaArchivo())
            {
                ejecutaCargaArchivo(true);
            }
        })
    }

    function validaCargaArchivo() {
        var componenteArchivoDeCarga = $("#archivo");
        var msmErrorNulo = "Favor de seleccionar el archivo de carga masiva.";
        var msmErrorFormato = "El formato de archivo aceptado es: <strong>.xlsx</strong>";
        if ($("#fechaCarga").val() == ""){
            mensajeError("Favor de seleccionar la fecha de carga.");
            return false;
        }

        if ($("#numeroExcel").val() == ""){
            mensajeError("Favor de ingresar el nombre del archivo de actualizacion");
            return false;
        }
        if (componenteArchivoDeCarga.val() == "") {
            mensajeError(msmErrorNulo);
            return false;
        } else if (componenteArchivoDeCarga.val().lastIndexOf(".xlsx") <= 0) {
            mensajeError(msmErrorFormato);
            return false;
        }
        return true;
    }

    function ejecutaCargaArchivo(remplazar) {

        var formData = new FormData();
        formData.append('fechaCarga', $("#fechaCarga").val());
        formData.append('numeroExcel', $("#numeroExcel").val());
        formData.append('remplazar', remplazar);

        formData.append('archivo', $("#archivo")[0].files[0]);
        var vURL = "service/catalogoExcel/excel";
        enviarArchivo(vURL, formData);
    }

    function enviarArchivo(ruta, formData) {
        $("#loader").show();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', ruta, true);
        xhr.responseType = 'blob';

        xhr.onload = function (e) {
            $("#loader").hide();

            if (this.status == 200)
            {
                var text = blobToString(this.response);
                cargaResultadosExitosos(JSON.parse(text));
                mensajeExito("Se cargo el archivo correctamente");
                defaultValores();
            } else if (this.status == 203) {
                var text = blobToString(this.response);
                cargaResultadosFallidos(JSON.parse(text));
            } else if (this.status == 205) {
                var text = blobToString(this.response);

            } else if (this.status == 207) {
                mensajeError("El archivo contiene errores, favor de verificar");
            } else if (this.status == 206) {
                var text = blobToString(this.response);
                mensajeError("Error de archivo " + text);
            } else {
                var msmErrorFormato = "No se pudo realizar la carga del archivo, por favor intente más tarde.";
                mensajeError(msmErrorFormato);
            }
        };

        xhr.send(formData);
    }

    function descargaArchivoExcel(response){
        var blob = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
        var downloadUrl = URL.createObjectURL(blob);
        var a = document.createElement("a");
        a.href = downloadUrl;
        a.download = $("#archivo").val().replace(/.*(\/|\\)/, '');
        document.body.appendChild(a);
        a.click();
    }

    function blobToString(b) {
        var u, x;
        u = URL.createObjectURL(b);
        x = new XMLHttpRequest();
        x.open('GET', u, false); // although sync, you're not fetching over internet
        x.send();
        URL.revokeObjectURL(u);
        return x.responseText;
    }

    function cargaResultadosExitosos(resultado) {
        $("#resultadoCarga").html("Archivo Excel cargado correctamente").addClass("bg-success").removeClass("bg-danger");
        $("#orderRes").html(resultado.numeroExcel);
        $("#fecRes").html(resultado.fechaCarga);

        var resHTML = "";
        resHTML += llenaRegistro(resultado.correctos, "Nuevo");
        resHTML += llenaRegistro(resultado.actualizados, "Actualizado");
        resHTML += llenaRegistro(resultado.erroneos, "Erroneo");
        
        $("#tbodyRes").html(resHTML);
        
         $("#datosRes").html("");

        $("#panelResultados").show();

    }

    function cargaResultadosFallidos(resultado) {
        $("#resultadoCarga").html("Confirmación con errores ").addClass("bg-danger").removeClass("bg-success");

        var textoErrorOrden = "";
        if (resultado.numeroOrdenErrorTxt != null)
        {
            textoErrorOrden = resultado.numeroOrdenErrorTxt;
        }

        $("#orderRes").html(resultado.numeroOrden + " " + textoErrorOrden);

        var textoErrorFecha = "";
        if (resultado.fechaConfirmacionErrorTxt != null)
        {
            textoErrorFecha = resultado.fechaConfirmacionErrorTxt;
        }
        $("#fecRes").html(resultado.fechaConfirmacion + " " + textoErrorFecha);

        if (resultado.errorListado)
        {
            $("#datosRes").html("El archivo no es válido o no contiene información");
        } else {
            var resHTML = "";
            resHTML += llenaRegistroError(resultado.general, "General");
            resHTML += llenaRegistroError(resultado.qianhai, "Qianhai");
            resHTML += llenaRegistroError(resultado.pingshan, "Pingshan");

            $("#tbodyRes").html(resHTML);
        }



        $("#panelResultados").show();
    }


    function llenaRegistro(lista, tipo){
        var resHTML = "";
        for (var i = 0; i < lista.length; i++){
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += tipo;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.cod_proveedor;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.cod_barra;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.clave_de_linea_de_producto;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.familia;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.product;
            resHTML += "</td>";

            resHTML += "</tr>";
        }
        return resHTML;
    }

    function llenaRegistroError(lista, tipo)
    {
        var resHTML = "";

        for (var i = 0; i < lista.length; i++)
        {
            var registro = lista[i];
            resHTML += "<tr>";

            resHTML += "<td>";
            resHTML += (i + 1);
            resHTML += "</td>";


            resHTML += "<td>";
            resHTML += tipo;
            resHTML += "</td>";

            if (registro.skuError)
            {
                resHTML += "<td class='bg-danger'>";
                resHTML += registro.sku + " " + registro.skuErrorTXT;
                resHTML += "</td>";
            } else {
                resHTML += "<td>";
                resHTML += registro.sku;
                resHTML += "</td>";
            }


            resHTML += "<td>";
            resHTML += registro.nombre;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.precio;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += registro.piezasSolicitadas;
            resHTML += "</td>";

            resHTML += "<td>";
            if (registro.notOrder)
            {
                resHTML += "Not Order";
            } else {
                resHTML += registro.piezasEntregadas;
            }
            resHTML += "</td>";

            resHTML += "</tr>";
        }

        return resHTML;
    }
    
     function defaultValores(){           
        $("#archivo").val("");
        $("#numeroExcel").val("");
        $("#fechaCarga").datepicker({
        language: 'es',
	dateFormat: 'dd/mm/yy'
        }).datepicker("setDate", new Date());
    }
});


