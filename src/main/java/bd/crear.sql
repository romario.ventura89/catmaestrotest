USE [CatalogoMaestro]
GO
/****** Object:  Table [dbo].[catMaestro]    Script Date: 30/10/2018 10:15:49 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catMaestro](
	[idCatMaestro] [int] IDENTITY(1,1) NOT NULL,
	[codProveedor] [varchar](50) NOT NULL,
	[codBarra] [varchar](50) NOT NULL,
	[predeterminado] [varchar](50) NULL,
	[clave_de_linea_de_producto] [varchar](50) NULL,
	[familia] [varchar](50) NULL,
	[subFamilia] [varchar](50) NULL,
	[subSubFamilia] [varchar](50) NULL,
	[descripcion] [varchar](150) NULL,
	[category] [varchar](50) NULL,
	[middleCategory] [varchar](50) NULL,
	[subcategory] [varchar](50) NULL,
	[smallCategory] [varchar](50) NULL,
	[product] [varchar](150) NULL,
	[fraccAr] [varchar](50) NULL,
	[inner_] [varchar](50) NULL,
	[validacion] [varchar](50) NULL,
	[fvMxp] [varchar](50) NULL,
	[fvRmb] [varchar](50) NULL,
	[ctRmb] [varchar](50) NULL,
	[textil] [varchar](50) NULL,
	[calzado] [varchar](50) NULL,
	[fechaRegistro] [datetime] NULL,
	[fechaActualizacion] [datetime] NULL,
	[archivoExcel] [varchar](50) NULL,
	[estatus] [int] NULL,
	[descripcionError] [varchar](250) NULL,
 CONSTRAINT [PK__catMaestro__B47B53073469505D] PRIMARY KEY CLUSTERED 
(
	[idCatMaestro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[estadoRegistro]    Script Date: 30/10/2018 10:15:50 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estadoRegistro](
	[idEstado] [int] NULL,
	[estado] [varchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gdinput]    Script Date: 30/10/2018 10:15:50 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gdinput](
	[idgdinput] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](15) NOT NULL,
	[munit] [varchar](15) NULL,
	[qpc] [int] NOT NULL,
	[qpcStr] [varchar](15) NULL,
	[rtlPrc] [varchar](15) NULL,
	[uuid] [varchar](15) NULL,
	[fechaRegistro] [datetime] NOT NULL,
	[fechaActualizacion] [datetime] NULL,
	[estatus] [int] NULL,
	[numeroJson] [varchar](20) NOT NULL,
	[descripcion] [varchar](250) NULL,
 CONSTRAINT [PK__GdInput__B47B53073469505D] PRIMARY KEY CLUSTERED 
(
	[idgdinput] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[goods]    Script Date: 30/10/2018 10:15:50 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[goods](
	[idGoods] [int] IDENTITY(1,1) NOT NULL,
	[brand] [varchar](5) NOT NULL,
	[code] [varchar](15) NULL,
	[manufactor] [varchar](50) NULL,
	[munit] [varchar](50) NOT NULL,
	[name] [varchar](150) NULL,
	[origin] [varchar](50) NULL,
	[qpc] [int] NOT NULL,
	[rtlPrc] [varchar](15) NULL,
	[sort] [varchar](15) NOT NULL,
	[spec] [varchar](80) NULL,
	[tm] [varchar](50) NULL,
	[uuid] [varchar](15) NOT NULL,
	[fechaRegistro] [datetime] NOT NULL,
	[fechaActualizacion] [datetime] NULL,
	[estatus] [int] NULL,
	[numeroJson] [varchar](20) NULL,
	[descripcion] [varchar](250) NULL,
 CONSTRAINT [PK__Goods__B47B53073469505D] PRIMARY KEY CLUSTERED 
(
	[idGoods] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[navegacion]    Script Date: 30/10/2018 10:15:50 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[navegacion](
	[idNavegacion] [int] IDENTITY(1,1) NOT NULL,
	[path] [varchar](100) NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[objeto] [varbinary](1) NULL,
	[estatus] [bit] NOT NULL,
 CONSTRAINT [PK__navegaci__15F6D04FE18C19FE] PRIMARY KEY CLUSTERED 
(
	[idNavegacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rol]    Script Date: 30/10/2018 10:15:50 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rol](
	[idRol] [int] NOT NULL,
	[descripcion] [varchar](60) NOT NULL,
 CONSTRAINT [PK__rol__3C872F764974B344] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sesion]    Script Date: 30/10/2018 10:15:50 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sesion](
	[idSesion] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idNavegacion] [int] NOT NULL,
 CONSTRAINT [PK__sesion__DB6C2DE64288DF8E] PRIMARY KEY CLUSTERED 
(
	[idSesion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 30/10/2018 10:15:50 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](60) NOT NULL,
	[password] [varchar](32) NOT NULL,
	[estatus] [bit] NOT NULL,
	[fechaRegistro] [datetime] NOT NULL,
	[email] [varchar](100) NULL,
	[nombre] [varchar](30) NULL,
	[apellidos] [varchar](100) NULL,
	[titulo] [varchar](10) NULL,
	[idRol] [int] NOT NULL,
 CONSTRAINT [PK__usuario__645723A6776BD6AE] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sesion]  WITH CHECK ADD  CONSTRAINT [FKsesion106662] FOREIGN KEY([idNavegacion])
REFERENCES [dbo].[navegacion] ([idNavegacion])
GO
ALTER TABLE [dbo].[sesion] CHECK CONSTRAINT [FKsesion106662]
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD  CONSTRAINT [FKusuario330728] FOREIGN KEY([idRol])
REFERENCES [dbo].[rol] ([idRol])
GO
ALTER TABLE [dbo].[usuario] CHECK CONSTRAINT [FKusuario330728]
GO
