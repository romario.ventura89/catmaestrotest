package mx.com.miniso.maestro.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import mx.com.miniso.maestro.constantes.DescripcionEnum;
import mx.com.miniso.maestro.constantes.SkuEstatusEnum;
import mx.com.miniso.maestro.dao.CatalogoDao;
import mx.com.miniso.maestro.dao.CatalogoExcelDao;
import mx.com.miniso.maestro.dto.CatMaestro;
import mx.com.miniso.maestro.dto.Goods;
import mx.com.miniso.maestro.dto.Input;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.util.ValidadorJson;
import mx.com.miniso.maestro.vo.CargaExcelDatosVO;
import mx.com.miniso.maestro.vo.CargaExcelVO;
import mx.com.miniso.maestro.vo.CatalogoExcelVO;
import mx.com.miniso.maestro.vo.CatalogoMaestroVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoInputVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CargaExcelService {

    final static Logger logger = LoggerFactory.getLogger(CargaExcelService.class);

    @Autowired 
    private CatalogoDao catalogoDao;

    @Autowired 
    private CatalogoExcelDao catalogoExcelDao;
    
    private String descripcion;

    public CargaExcelVO obtenerDatos(CargaExcelVO cargaVO, MultipartFile archivo) throws IOException {
        Workbook wb = null;
        try {
            wb = new XSSFWorkbook(archivo.getInputStream());
        } catch (IOException e) {
            logger.error("Error al convertir el archivo al formato de Excel");
            throw e;
        }
        XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(0);
        if (sheet != null) {
            cargaVO.setDatos(extraeHoja(sheet));
        }
        for(CargaExcelDatosVO carga : cargaVO.getDatos()){
            if(realizaValidaciones(carga)){
                cargaVO.getCorrectos().add(carga);
            }else{
                carga.setDescripcionError(descripcion);
                cargaVO.getErroneos().add(carga);
            }
        }
        return cargaVO;
    }
    
     public boolean realizaValidaciones(CargaExcelDatosVO excel){
        ValidadorJson validadorJson = new ValidadorJson();
        if(excel.getCod_proveedor()!=null && !excel.getCod_proveedor().isEmpty()){  
            if(!validadorJson.validaEsNumero(excel.getCod_proveedor())){
                setDescripcion(DescripcionEnum.SKULETRAS.getDesc());
                return false;
            }
        }else{
            setDescripcion(DescripcionEnum.SKU.getDesc());
            return false;
        }
        if(excel.getCod_barra()!=null && !excel.getCod_barra().isEmpty()){
            if(!validadorJson.validaEsNumero(excel.getCod_barra())){
                setDescripcion(DescripcionEnum.CBARRASLETRAS.getDesc());
                return false;
            }
            if(!validadorJson.validaTamanio(13, excel.getCod_barra())){
                return false;
            }
        }else{
            setDescripcion(DescripcionEnum.CBARRAS.getDesc());
            return false;
        }
        return true;
    }
    
    
    public List<CatalogoExcelVO> buscarCatalogo(FiltrosCatalogoInputVO filtrosCatalogoInputVO) throws Exception {
        List<CatalogoExcelVO> catalogoExcelVO = new ArrayList<>(0);
        catalogoExcelVO = catalogoExcelDao.obtenerXParametros(filtrosCatalogoInputVO);
        return catalogoExcelVO;
    }
    
    public List<CatalogoExcelVO> buscarExcel(FiltrosCatalogoInputVO filtrosCatalogoInputVO) throws Exception {
        List<CatalogoExcelVO> catalogoExcelVO = new ArrayList<>(0);
        catalogoExcelVO = catalogoExcelDao.obtenerExcel(filtrosCatalogoInputVO);
        return catalogoExcelVO;
    }
    
    public List<CatalogoExcelVO> obtenerDetalle(CatalogoExcelVO catalogoExcelVO) {
        List<CatalogoExcelVO> detalles = catalogoExcelDao.obtenerDetalle(catalogoExcelVO);
        return detalles;
    }
    
    private List<CargaExcelDatosVO> extraeHoja(XSSFSheet sheet) {
        int columnaCProveedor = 1;
        int columnaCBarras = 2;
        int columnaClvLn = 3;
        int columnaFamilia = 4;
        int columnaSbFamilia =5;
        int columnaSbSbFamilia =6;
        int columnaDsc = 7;
        int columnaCat = 8;
        int columnaMdlCat = 9;
        int columnaSbCat =10;
        int columnaSmlCat = 11;
        int columnaProd = 12;
        int columnaFracc = 13;
        int columnainner = 14;
        int columnaVal = 15;
        int columnaMxp = 16;
        int columnaFvRmb = 17;
        int columnaCtRmb = 18;
        int columnaTextil = 19;
        int columnaCalzado = 20;
        List<CargaExcelDatosVO> datos = new ArrayList<>(0);
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            CargaExcelDatosVO dato = new CargaExcelDatosVO();
            String valor = formateaValor(row.getCell(columnaCProveedor));
            if (StringUtils.isEmpty(valor)) {
                break;
            }
            dato.setCod_proveedor(valor);

            valor = formateaValor(row.getCell(columnaCBarras));
            dato.setCod_barra(valor);
            
            if (StringUtils.isEmpty(valor)) {
                break;
            }
            
            valor = formateaValor(row.getCell(columnaClvLn));
            dato.setClave_de_linea_de_producto(valor);

            valor = formateaValor(row.getCell(columnaFamilia));
            dato.setFamilia(valor);

            valor = formateaValor(row.getCell(columnaSbFamilia));
            dato.setSubfamilia(valor);

            valor = formateaValor(row.getCell(columnaSbSbFamilia));
            dato.setSub_subfamilia(valor);

            valor = formateaValor(row.getCell(columnaDsc));
            dato.setDescripcion(valor);

            valor = formateaValor(row.getCell(columnaCat));
            dato.setCategory(valor);

            valor = formateaValor(row.getCell(columnaMdlCat));
            dato.setMiddle_category(valor);

            valor = formateaValor(row.getCell(columnaSbCat));
            dato.setSubcategory(valor);

            valor = formateaValor(row.getCell(columnaSmlCat));
            dato.setSmall_category(valor);

            valor = formateaValor(row.getCell(columnaProd));
            dato.setProduct(valor);

            valor = formateaValor(row.getCell(columnaFracc));
            dato.setFracc_ar(valor);
            
            valor = formateaValor(row.getCell(columnainner));
            dato.setInner(valor); 

            valor = formateaValor(row.getCell(columnaVal));
            dato.setValidacion(valor);

            valor = formateaValor(row.getCell(columnaMxp));            
            dato.setFv_mxp(valor.replace(" ", ""));            

            valor = formateaValor(row.getCell(columnaFvRmb));            
            dato.setFv_rmb(valor.replace(" ", ""));            

            valor = formateaValor(row.getCell(columnaCtRmb));            
            dato.setCt_rmb(valor.replace(" ", ""));            

            valor = formateaValor(row.getCell(columnaTextil));
            dato.setTextil(valor);

            valor = formateaValor(row.getCell(columnaCalzado));
            dato.setCalzado(valor);  
            datos.add(dato);
        }
        return datos;
    }

    private String formateaValor(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        String valor = formatter.formatCellValue(cell);
        return valor;
    }

    public CargaExcelVO guarda(CargaExcelVO cargaVO) throws Exception {
        CatMaestro catMaestro = new CatMaestro();
        CargaExcelVO carga = new CargaExcelVO();
        boolean actualizar= false;
        String sku="";
        String codBarras="";
        CatMaestro catalogoList = new CatMaestro();
        for(CargaExcelDatosVO cargaDatos: cargaVO.getCorrectos()){
            Input input = new Input();
            Goods goods = new Goods();
            sku=cargaDatos.getCod_proveedor();
            codBarras=cargaDatos.getCod_barra();
            catalogoList = catalogoExcelDao.obtenerCombinacion(cargaDatos.getCod_proveedor(),cargaDatos.getCod_barra());
            if(catalogoList != null){
                actualizar= true;
            }
            
            if(actualizar && catalogoList != null){
                catMaestro = guarda(cargaDatos);
                catMaestro.setIdCatMaestro(catalogoList.getIdCatMaestro());
                catMaestro.setArchivoExcel(cargaVO.getNumeroExcel());
                catMaestro.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
                catMaestro.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
                carga.getActualizados().add(cargaDatos);
                catalogoExcelDao.actualizar(catMaestro);
            }else{
                catMaestro = guarda(cargaDatos);
                catMaestro.setDescripcionError(getDescripcion());
                catMaestro.setArchivoExcel(cargaVO.getNumeroExcel());
                catMaestro.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
                catMaestro.setEstatus(SkuEstatusEnum.NUEVO.getId());
                catalogoExcelDao.guardar(catMaestro);
                input.setCode(codBarras);
                goods.setCode(sku);
                input = catalogoDao.obtenerXSkuInput(input);
                goods = catalogoDao.obtenerXSku(goods);
                if(goods == null || input == null){
                    catMaestro.setArchivoExcel(cargaVO.getNumeroExcel());
                    catMaestro.setEstatus(SkuEstatusEnum.ERRONEO.getId());
                    catMaestro.setDescripcionError(DescripcionEnum.NOENCONTRADO.getDesc());
                    carga.getErroneos().add(cargaDatos);
                }else{
                    catMaestro.setArchivoExcel(cargaVO.getNumeroExcel());
                    catMaestro.setEstatus(SkuEstatusEnum.NUEVO.getId());
                    carga.getCorrectos().add(cargaDatos);
                    catalogoExcelDao.guardar(catMaestro);
                }
            }
        }
        
        for(CargaExcelDatosVO cargaError: cargaVO.getErroneos()){
            catMaestro = guarda(cargaError);
            catMaestro.setArchivoExcel(cargaVO.getNumeroExcel());
            catMaestro.setEstatus(SkuEstatusEnum.ERRONEO.getId());
            carga.getErroneos().add(cargaError);
            catalogoExcelDao.guardar(catMaestro);
        }
        return carga;
    }

    public CatMaestro guarda(CargaExcelDatosVO cargaDatos) throws Exception {
        CatMaestro catMaestro = new CatMaestro();
        catMaestro.setCodProveedor(cargaDatos.getCod_proveedor());
        catMaestro.setCodBarra(cargaDatos.getCod_barra());
        catMaestro.setClave_de_linea_de_producto(cargaDatos.getClave_de_linea_de_producto());
        catMaestro.setFamilia(cargaDatos.getFamilia());
        catMaestro.setSubFamilia(cargaDatos.getSubfamilia());
        catMaestro.setSubSubFamilia(cargaDatos.getSub_subfamilia());
        catMaestro.setDescripcion(cargaDatos.getDescripcion());
        catMaestro.setCategory(cargaDatos.getCategory());
        catMaestro.setMiddleCategory(cargaDatos.getMiddle_category());
        catMaestro.setSubcategory(cargaDatos.getSubcategory());
        catMaestro.setSmallCategory(cargaDatos.getSmall_category());
        catMaestro.setProduct(cargaDatos.getProduct());
        catMaestro.setFraccAr(cargaDatos.getFracc_ar());
        catMaestro.setInner_(cargaDatos.getInner());
        catMaestro.setValidacion(cargaDatos.getValidacion());
        catMaestro.setFvMxp(cargaDatos.getFv_mxp());
        catMaestro.setFvRmb(cargaDatos.getFv_rmb());
        catMaestro.setCtRmb(cargaDatos.getCt_rmb());
        catMaestro.setTextil(cargaDatos.getTextil());
        catMaestro.setCalzado(cargaDatos.getCalzado());
        catMaestro.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
//        catMaestro.setDescripcionError(cargaDatos.getDescripcionError());
        return catMaestro;
    }
    
    public CatalogoExcelVO actualizar(CatalogoExcelVO catalogoExcelVO,String idCatMaestro) {
        System.out.println("idGdInput "+idCatMaestro);
        CatMaestro catMaestro = new CatMaestro();
        catMaestro.setIdCatMaestro(Long.parseLong(idCatMaestro));
        catMaestro.setCodProveedor(catalogoExcelVO.getCodProveedor());
        catMaestro.setCodBarra(catalogoExcelVO.getCodBarra());
        catMaestro.setClave_de_linea_de_producto(catalogoExcelVO.getClave_de_linea_de_producto());
        catMaestro.setFamilia(catalogoExcelVO.getFamilia());
        catMaestro.setSubFamilia(catalogoExcelVO.getSubFamilia());
        catMaestro.setSubSubFamilia(catalogoExcelVO.getSubSubFamilia());
        catMaestro.setDescripcion(catalogoExcelVO.getDescripcion());
        catMaestro.setCategory(catalogoExcelVO.getCategory());
        catMaestro.setMiddleCategory(catalogoExcelVO.getMiddleCategory());
        catMaestro.setSubcategory(catalogoExcelVO.getSubcategory());
        catMaestro.setSmallCategory(catalogoExcelVO.getSmallCategory());
        catMaestro.setProduct(catalogoExcelVO.getProduct());
        catMaestro.setFraccAr(catalogoExcelVO.getFraccAr());
        catMaestro.setInner_(catalogoExcelVO.getInner_());
        catMaestro.setValidacion(catalogoExcelVO.getValidacion());
        catMaestro.setFvMxp(catalogoExcelVO.getFvMxp());
        catMaestro.setFvRmb(catalogoExcelVO.getFvRmb());
        catMaestro.setCtRmb(catalogoExcelVO.getCtRmb());
        catMaestro.setTextil(catalogoExcelVO.getTextil());
        catMaestro.setCalzado(catalogoExcelVO.getCalzado());
        catMaestro.setFechaRegistro(FechaUtil.convierteFechaStringTOTimestamp(catalogoExcelVO.getFechaRegistro()));
        catMaestro.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
        catMaestro.setArchivoExcel(catalogoExcelVO.getArchivoExcel());
        catMaestro.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
        catalogoExcelDao.actualizar(catMaestro);
        return catalogoExcelVO;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public List<CatalogoMaestroVO> buscarErrores() throws Exception {
        List<CatalogoMaestroVO> catalogoMaestroVO = new ArrayList<>(0);
        catalogoMaestroVO = catalogoExcelDao.obtenerErrores();
        return catalogoMaestroVO;
    }
    
    
}
