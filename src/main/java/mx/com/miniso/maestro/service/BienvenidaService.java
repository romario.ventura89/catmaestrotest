package mx.com.miniso.maestro.service;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.dto.Sociedad;
import mx.com.miniso.maestro.vo.SociedadVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BienvenidaService {
    
    @Autowired
    private CatalogoService catalogoService;

    public List<SociedadVO> getCatalogoSociedades() throws Exception {
        List<Sociedad> sociedad = catalogoService.getCatalogoSociedades();
        List<SociedadVO> sociedadList = new ArrayList<>(0);
        for (Sociedad sociedades: sociedad){
            SociedadVO sociedadVO = new SociedadVO();
            sociedadVO.setDescripcion(sociedades.getDescripcion());
            sociedadVO.setIdSociedad(String.valueOf(sociedades.getIdSociedad()));
            sociedadList.add(sociedadVO);
        }
        return sociedadList;
    }
}
