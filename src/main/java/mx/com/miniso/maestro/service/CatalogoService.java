package mx.com.miniso.maestro.service;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.constantes.CatalogoEnum;
import mx.com.miniso.maestro.constantes.SkuEstatusEnum;
import mx.com.miniso.maestro.dao.CatalogoDao;
import mx.com.miniso.maestro.dao.SociedadDao;
import mx.com.miniso.maestro.dao.TipoOrdenDao;
import mx.com.miniso.maestro.dto.Goods;
import mx.com.miniso.maestro.dto.Sociedad;
import mx.com.miniso.maestro.dto.TipoOrden;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CatalogoVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CatalogoService {
    
    final static Log logger = LogFactory.getLog(CatalogoService.class);
    
    @Autowired    
    private SociedadDao sociedadDao;
    
    @Autowired 
    private CatalogoDao catalogoDao;
    
    @Autowired
    private TipoOrdenDao tipoOrdenDao;
    
    private List<Sociedad> sociedades;
    private List<TipoOrden> tipoOrdenes;
    
    public List<CatalogoVO> buscarCatalogo(FiltrosCatalogoVO filtrosCatalogoVO) throws Exception {
        List<CatalogoVO> catalogoMaestro = new ArrayList<>(0);
        if (filtrosCatalogoVO == null) {
            return catalogoMaestro;
        }
        catalogoMaestro = catalogoDao.obtenerXParametros(filtrosCatalogoVO);
        return catalogoMaestro;
    }
    
    public List<CatalogoVO> buscarExcel(FiltrosCatalogoVO filtrosCatalogoVO) throws Exception {
        List<CatalogoVO> catalogoMaestro = new ArrayList<>(0);
        if (filtrosCatalogoVO == null) {
            return catalogoMaestro;
        }
        catalogoMaestro = catalogoDao.obtenerExcel(filtrosCatalogoVO);
        return catalogoMaestro;
    }
    
    public List<CatalogoVO> obtenerDetalle(CatalogoVO catalogoVO) {
        List<CatalogoVO> detalles = catalogoDao.obtenDetalleErreneo(catalogoVO);
        return detalles;
    }
    
    public List<CatalogoVO> obtenerDetalleXId(CatalogoVO catalogoVO) {
        List<CatalogoVO> detalles = catalogoDao.obtenDetalleXId(catalogoVO);
        return detalles;
    }
    
    public void eliminar(CatalogoVO catalogoVO) {
        Goods goods = new Goods();
        goods.setCode(catalogoVO.getCode());
        goods = catalogoDao.obtenerXSku(goods);
        if(goods == null){
            return;
        }else{
            catalogoDao.borrar(goods);
        }
    }
    
    public CatalogoVO actualizar(CatalogoVO catalogoVO,String idGoods) {
        Goods goods = new Goods();
        goods.setIdGoods(Long.parseLong(idGoods));
        goods.setBrand(catalogoVO.getBrand());
        goods.setCode(catalogoVO.getCode());
        goods.setManufactor(catalogoVO.getManufactor());
        goods.setMunit(catalogoVO.getMunit());
        goods.setName(catalogoVO.getName());
        goods.setOrigin(catalogoVO.getOrigin());
        catalogoVO.setQpc("13");
        goods.setQpc(Long.parseLong(catalogoVO.getQpc()));
        goods.setRtlPrc(catalogoVO.getRtlPrc());
        goods.setSort(catalogoVO.getSort());
        goods.setSpec(catalogoVO.getSpec());
        goods.setTm(catalogoVO.getTm());
        goods.setUuid(catalogoVO.getUuid());
        goods.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
        goods.setDescripcion(catalogoVO.getDescripcion());
        goods.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
        goods.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
        catalogoDao.actualizar(goods);

        return catalogoVO;
    }
    
     public void verificaCatalogo(CatalogoEnum catalogoEnum) {
        if (catalogoEnum.equals(CatalogoEnum.CAT_SOCIEDAD)) {
            if (sociedades == null) {
                sociedades = sociedadDao.buscarTodos();
            }
        } else if (catalogoEnum.equals(CatalogoEnum.CAT_TIPO_ORDEN)) {
            if (tipoOrdenes == null) {
                tipoOrdenes = tipoOrdenDao.buscarTodos();
            }
        }
    }
     
     public List<Sociedad> getCatalogoSociedades() {
        verificaCatalogo(CatalogoEnum.CAT_SOCIEDAD);
        return sociedades;
    }
    
     public Sociedad buscarSociedad(Sociedad sociedad) {
        verificaCatalogo(CatalogoEnum.CAT_SOCIEDAD);
        for (Sociedad catSociedadIter : sociedades) {
            if (catSociedadIter.equals(sociedad)) {
                return catSociedadIter;
            }
            
        }
        return null;
    }   
     public Sociedad buscarCatSociedad(long idSociedad) {
        Sociedad sociedad = new Sociedad();
        sociedad.setIdSociedad(idSociedad);
        return buscarSociedad(sociedad);
    }
     
     public List<TipoOrden> getCatalogoTipoOrdenes(){
        verificaCatalogo(CatalogoEnum.CAT_TIPO_ORDEN);
        return tipoOrdenes;
    }
    
     public TipoOrden buscarTipoOrden(TipoOrden tipoOrden) {
        verificaCatalogo(CatalogoEnum.CAT_TIPO_ORDEN);
        for (TipoOrden catTipoOrdenIter : tipoOrdenes) {
            if (catTipoOrdenIter.equals(tipoOrden)) {
                return catTipoOrdenIter;
            }
            
        }
        return null;
    }   
     public TipoOrden buscarCatTipoOrden(long idTipoOrden) {
        TipoOrden tipoOrden = new TipoOrden();
        tipoOrden.setIdTipoOrden(idTipoOrden);
        return buscarTipoOrden(tipoOrden);
    }
     
    public List<CatalogoVO> buscarErrores() throws Exception {
        List<CatalogoVO> catalogoErrores = new ArrayList<>(0);       
        catalogoErrores = catalogoDao.obtenerErrores();
        return catalogoErrores;
    } 
     
}
