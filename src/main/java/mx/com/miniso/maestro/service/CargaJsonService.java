package mx.com.miniso.maestro.service;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.constantes.DescripcionEnum;
import mx.com.miniso.maestro.constantes.SkuEstatusEnum;
import mx.com.miniso.maestro.dao.CatalogoDao;
import mx.com.miniso.maestro.dto.Goods;
import mx.com.miniso.maestro.ex.ConfirmacionNumeroOrdenExcepcion;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.util.ValidadorJson;
import mx.com.miniso.maestro.vo.CargaGoodsVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CargaJsonService {

    final static Logger logger = LoggerFactory.getLogger(CargaJsonService.class);

    @Autowired
    private CatalogoDao catalogoDao;
    
    private String descripcion;
    
    public CargaGoodsVO obtenerDatos(CargaGoodsVO cargaGoodsVO, MultipartFile archivo) throws IOException {
        InputStreamReader crunchifyReader = new InputStreamReader(archivo.getInputStream());
        BufferedReader br = new BufferedReader(crunchifyReader);
        List<Goods> results;
        Gson gson = new Gson();
        results = new ArrayList<>(0);
        String registro = br.readLine();
            while (registro != null) {
                Goods goods = new Goods();
                goods = gson.fromJson(registro, Goods.class);
                results.add(goods);
                registro = br.readLine();
            }
            
            for(Goods goods : results){
                setDescripcion("");
                if(realizaValidaciones(goods)){
                    cargaGoodsVO.getCorrectos().add(goods);
                }else{
                    goods.setDescripcion(getDescripcion());
                    cargaGoodsVO.getErroneos().add(goods);
                }
            }
        return cargaGoodsVO;
    }
    
    public boolean realizaValidaciones(Goods goods){
        ValidadorJson validadorJson = new ValidadorJson();
        if(goods.getCode()!=null && !goods.getCode().isEmpty()){
            if(!validadorJson.validaEsNumero(goods.getCode())){
                setDescripcion(DescripcionEnum.SKULETRAS.getDesc());
                return false;
            }
        }else{
            setDescripcion(DescripcionEnum.SKU.getDesc());
            return false;
        }
        if(goods.getUuid()!=null && !goods.getUuid().isEmpty()){
            if(!validadorJson.validaEsNumero(goods.getUuid())){
                setDescripcion(DescripcionEnum.UUIDLETRAS.getDesc());
                return false;
            }
        }else{
            setDescripcion(DescripcionEnum.UUID.getDesc());
            return false;
        }
        return true;
    }

    public void guarda(CargaGoodsVO cargaGoodsVO) throws ConfirmacionNumeroOrdenExcepcion, Exception {
        boolean actualizar = false;
        Goods goods = new Goods();
        for(Goods goodsCorrectos : cargaGoodsVO.getCorrectos()){
            goods = catalogoDao.obtenerXSku(goodsCorrectos);
            if (goods != null) {
                actualizar = true;
            }
            if (actualizar && goods != null) {
                goodsCorrectos.setIdGoods(goods.getIdGoods());
                goodsCorrectos.setFechaRegistro(goods.getFechaRegistro());
                goodsCorrectos.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
                goodsCorrectos.setNumeroJson(cargaGoodsVO.getNumeroJson());
                goodsCorrectos.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
                cargaGoodsVO.getActualizados().add(goodsCorrectos);
                catalogoDao.actualizar(goodsCorrectos);
            }else{
                goodsCorrectos.setFechaRegistro(FechaUtil.convierteFechaStringTOTimestamp(cargaGoodsVO.getFechaCarga()));
                goodsCorrectos.setEstatus(SkuEstatusEnum.NUEVO.getId());
                goodsCorrectos.setNumeroJson(cargaGoodsVO.getNumeroJson());
                cargaGoodsVO.getNuevos().add(goodsCorrectos);
                catalogoDao.guardarGoods(goodsCorrectos);
            }
        }
        guardaErroneos(cargaGoodsVO);
    }

    public void guardaErroneos(CargaGoodsVO cargaGoodsVO) throws ConfirmacionNumeroOrdenExcepcion, Exception {
        Goods goods = new Goods();
        for(Goods goodsErroneos : cargaGoodsVO.getErroneos()){
            goods = catalogoDao.obtenerXSku(goodsErroneos);
            if (goods == null) {
                goodsErroneos.setFechaRegistro(FechaUtil.convierteFechaStringTOTimestamp(cargaGoodsVO.getFechaCarga()));
                goodsErroneos.setEstatus(SkuEstatusEnum.ERRONEO.getId());
                goodsErroneos.setNumeroJson(cargaGoodsVO.getNumeroJson());
                cargaGoodsVO.getErrores().add(goodsErroneos);
                catalogoDao.guardarGoods(goodsErroneos);
            }
        }
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
