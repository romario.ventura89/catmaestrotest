package mx.com.miniso.maestro.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import mx.com.miniso.maestro.constantes.DescripcionEnum;
import mx.com.miniso.maestro.constantes.SkuEstatusEnum;
import mx.com.miniso.maestro.dao.CatalogoDao;
import mx.com.miniso.maestro.dao.CatalogoNomDao;
import mx.com.miniso.maestro.dto.CatMaestro;
import mx.com.miniso.maestro.dto.Goods;
import mx.com.miniso.maestro.dto.Input;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.util.ValidadorJson;
import mx.com.miniso.maestro.vo.CargaNomDatosVO;
import mx.com.miniso.maestro.vo.CargaNomVO;
import mx.com.miniso.maestro.vo.CatalogoMaestroVO;
import mx.com.miniso.maestro.vo.CatalogoNomVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoNomVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CargaNomService {

    final static Logger logger = LoggerFactory.getLogger(CargaNomService.class);

    @Autowired 
    private CatalogoDao catalogoDao;

    @Autowired 
    private CatalogoNomDao catalogoNomDao;
    
    private String descripcion;

    public CargaNomVO obtenerDatos(CargaNomVO cargaNomVO, MultipartFile archivo) throws IOException {
        Workbook wb = null;
        try {
            wb = new XSSFWorkbook(archivo.getInputStream());
        } catch (IOException e) {
            logger.error("Error al convertir el archivo al formato de Excel");
            throw e;
        }
        XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(0);
        if (sheet != null) {
            cargaNomVO.setDatos(extraeHoja(sheet));
        }
        for(CargaNomDatosVO carga : cargaNomVO.getDatos()){
            if(realizaValidaciones(carga)){
                cargaNomVO.getCorrectos().add(carga);
            }else{
                carga.setDescripcionError(descripcion);
                cargaNomVO.getErroneos().add(carga);
            }
        }
        return cargaNomVO;
    }
    
     public boolean realizaValidaciones(CargaNomDatosVO excel){
        ValidadorJson validadorJson = new ValidadorJson();
        if(excel.getCod_proveedor()!=null && !excel.getCod_proveedor().isEmpty()){  
            if(!validadorJson.validaEsNumero(excel.getCod_proveedor())){
                setDescripcion(DescripcionEnum.SKULETRAS.getDesc());
                return false;
            }
        }else{
            setDescripcion(DescripcionEnum.SKU.getDesc());
            return false;
        }
        if(excel.getCod_barra()!=null && !excel.getCod_barra().isEmpty()){
            if(!validadorJson.validaEsNumero(excel.getCod_barra())){
                setDescripcion(DescripcionEnum.CBARRASLETRAS.getDesc());
                return false;
            }
            if(!validadorJson.validaTamanio(13, excel.getCod_barra())){
                return false;
            }
        }else{
            setDescripcion(DescripcionEnum.CBARRAS.getDesc());
            return false;
        }
        return true;
    }
    
    
    public List<CatalogoNomVO> buscarCatalogo(FiltrosCatalogoNomVO filtrosCatalogoNomVO) throws Exception {
        List<CatalogoNomVO> catalogoNomVO = new ArrayList<>(0);
        catalogoNomVO = catalogoNomDao.obtenerXParametros(filtrosCatalogoNomVO);
        return catalogoNomVO;
    }
    
    public List<CatalogoNomVO> buscarExcel(FiltrosCatalogoNomVO filtrosCatalogoNomVO) throws Exception {
        List<CatalogoNomVO> catalogoNomVO = new ArrayList<>(0);
        catalogoNomVO = catalogoNomDao.obtenerExcel(filtrosCatalogoNomVO);
        return catalogoNomVO;
    }
    
    public List<CatalogoNomVO> obtenerDetalle(CatalogoNomVO catalogoNomVO) {
        List<CatalogoNomVO> detalles = catalogoNomDao.obtenerDetalle(catalogoNomVO);
        return detalles;
    }
    
    private List<CargaNomDatosVO> extraeHoja(XSSFSheet sheet) {
        int columnaCProveedor = 1;
        int columnaCBarras = 2;
        int columnaClvLn = 3;
        int columnaFamilia = 4;
        int columnaSbFamilia =5;
        int columnaSbSbFamilia =6;
        int columnaDsc = 7;
        int columnaCat = 8;
        int columnaMdlCat = 9;
        int columnaSbCat =10;
        int columnaSmlCat = 11;
        int columnaProd = 12;
        int columnaFracc = 13;
        int columnainner = 14;
        int columnaVal = 15;
        int columnaMxp = 16;
        int columnaFvRmb = 17;
        int columnaCtRmb = 18;
        int columnaTextil = 19;
        int columnaCalzado = 20;
        List<CargaNomDatosVO> datos = new ArrayList<>(0);
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            CargaNomDatosVO dato = new CargaNomDatosVO();
            String valor = formateaValor(row.getCell(columnaCProveedor));
            if (StringUtils.isEmpty(valor)) {
                break;
            }
            dato.setCod_proveedor(valor);

            valor = formateaValor(row.getCell(columnaCBarras));
            dato.setCod_barra(valor);
            
            if (StringUtils.isEmpty(valor)) {
                break;
            }
            
            valor = formateaValor(row.getCell(columnaClvLn));
            dato.setClave_de_linea_de_producto(valor);

            valor = formateaValor(row.getCell(columnaFamilia));
            dato.setFamilia(valor);

            valor = formateaValor(row.getCell(columnaSbFamilia));
            dato.setSubfamilia(valor);

            valor = formateaValor(row.getCell(columnaSbSbFamilia));
            dato.setSub_subfamilia(valor);

            valor = formateaValor(row.getCell(columnaDsc));
            dato.setDescripcion(valor);

            valor = formateaValor(row.getCell(columnaCat));
            dato.setCategory(valor);

            valor = formateaValor(row.getCell(columnaMdlCat));
            dato.setMiddle_category(valor);

            valor = formateaValor(row.getCell(columnaSbCat));
            dato.setSubcategory(valor);

            valor = formateaValor(row.getCell(columnaSmlCat));
            dato.setSmall_category(valor);

            valor = formateaValor(row.getCell(columnaProd));
            dato.setProduct(valor);

            valor = formateaValor(row.getCell(columnaFracc));
            dato.setFracc_ar(valor);
            
            valor = formateaValor(row.getCell(columnainner));
            dato.setInner(valor); 

            valor = formateaValor(row.getCell(columnaVal));
            dato.setValidacion(valor);

            valor = formateaValor(row.getCell(columnaMxp));            
            dato.setFv_mxp(valor.replace(" ", ""));            

            valor = formateaValor(row.getCell(columnaFvRmb));            
            dato.setFv_rmb(valor.replace(" ", ""));            

            valor = formateaValor(row.getCell(columnaCtRmb));            
            dato.setCt_rmb(valor.replace(" ", ""));            

            valor = formateaValor(row.getCell(columnaTextil));
            dato.setTextil(valor);

            valor = formateaValor(row.getCell(columnaCalzado));
            dato.setCalzado(valor);  
            datos.add(dato);
        }
        return datos;
    }

    private String formateaValor(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        String valor = formatter.formatCellValue(cell);
        return valor;
    }

    public CargaNomVO guarda(CargaNomVO cargaNomVO) throws Exception {
        CatMaestro catMaestro = new CatMaestro();
        CargaNomVO carga = new CargaNomVO();
        boolean actualizar= false;
        String sku="";
        String codBarras="";
        CatMaestro catalogoList = new CatMaestro();
        for(CargaNomDatosVO cargaDatos: cargaNomVO.getCorrectos()){
            Input input = new Input();
            Goods goods = new Goods();
            sku=cargaDatos.getCod_proveedor();
            codBarras=cargaDatos.getCod_barra();
            catalogoList = catalogoNomDao.obtenerCombinacion(cargaDatos.getCod_proveedor(),cargaDatos.getCod_barra());
            if(catalogoList != null){
                actualizar= true;
            }
            
            if(actualizar && catalogoList != null){
                catMaestro = guarda(cargaDatos);
                catMaestro.setIdCatMaestro(catalogoList.getIdCatMaestro());
                catMaestro.setArchivoExcel(cargaNomVO.getNumeroExcel());
                catMaestro.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
                catMaestro.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
                carga.getActualizados().add(cargaDatos);
                catalogoNomDao.actualizar(catMaestro);
            }else{
                catMaestro = guarda(cargaDatos);
                catMaestro.setDescripcionError(getDescripcion());
                catMaestro.setArchivoExcel(cargaNomVO.getNumeroExcel());
                catMaestro.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
                catMaestro.setEstatus(SkuEstatusEnum.NUEVO.getId());
                catalogoNomDao.guardar(catMaestro);
                input.setCode(codBarras);
                goods.setCode(sku);
                input = catalogoDao.obtenerXSkuInput(input);
                goods = catalogoDao.obtenerXSku(goods);
                if(goods == null || input == null){
                    catMaestro.setArchivoExcel(cargaNomVO.getNumeroExcel());
                    catMaestro.setEstatus(SkuEstatusEnum.ERRONEO.getId());
                    catMaestro.setDescripcionError(DescripcionEnum.NOENCONTRADO.getDesc());
                    carga.getErroneos().add(cargaDatos);
                }else{
                    catMaestro.setArchivoExcel(cargaNomVO.getNumeroExcel());
                    catMaestro.setEstatus(SkuEstatusEnum.NUEVO.getId());
                    carga.getCorrectos().add(cargaDatos);
                    catalogoNomDao.guardar(catMaestro);
                }
            }
        }
        
        for(CargaNomDatosVO cargaError: cargaNomVO.getErroneos()){
            catMaestro = guarda(cargaError);
            catMaestro.setArchivoExcel(cargaNomVO.getNumeroExcel());
            catMaestro.setEstatus(SkuEstatusEnum.ERRONEO.getId());
            carga.getErroneos().add(cargaError);
            catalogoNomDao.guardar(catMaestro);
        }
        return carga;
    }

    public CatMaestro guarda(CargaNomDatosVO cargaNomDatosVO) throws Exception {
        CatMaestro catMaestro = new CatMaestro();
        catMaestro.setCodProveedor(cargaNomDatosVO.getCod_proveedor());
        catMaestro.setCodBarra(cargaNomDatosVO.getCod_barra());
        catMaestro.setClave_de_linea_de_producto(cargaNomDatosVO.getClave_de_linea_de_producto());
        catMaestro.setFamilia(cargaNomDatosVO.getFamilia());
        catMaestro.setSubFamilia(cargaNomDatosVO.getSubfamilia());
        catMaestro.setSubSubFamilia(cargaNomDatosVO.getSub_subfamilia());
        catMaestro.setDescripcion(cargaNomDatosVO.getDescripcion());
        catMaestro.setCategory(cargaNomDatosVO.getCategory());
        catMaestro.setMiddleCategory(cargaNomDatosVO.getMiddle_category());
        catMaestro.setSubcategory(cargaNomDatosVO.getSubcategory());
        catMaestro.setSmallCategory(cargaNomDatosVO.getSmall_category());
        catMaestro.setProduct(cargaNomDatosVO.getProduct());
        catMaestro.setFraccAr(cargaNomDatosVO.getFracc_ar());
        catMaestro.setInner_(cargaNomDatosVO.getInner());
        catMaestro.setValidacion(cargaNomDatosVO.getValidacion());
        catMaestro.setFvMxp(cargaNomDatosVO.getFv_mxp());
        catMaestro.setFvRmb(cargaNomDatosVO.getFv_rmb());
        catMaestro.setCtRmb(cargaNomDatosVO.getCt_rmb());
        catMaestro.setTextil(cargaNomDatosVO.getTextil());
        catMaestro.setCalzado(cargaNomDatosVO.getCalzado());
        catMaestro.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
//        catMaestro.setDescripcionError(cargaDatos.getDescripcionError());
        return catMaestro;
    }
    
    public CatalogoNomVO actualizar(CatalogoNomVO catalogoNomVO,String idCatMaestro) {
        System.out.println("idGdInput "+idCatMaestro);
        CatMaestro catMaestro = new CatMaestro();
        catMaestro.setIdCatMaestro(Long.parseLong(idCatMaestro));
        catMaestro.setCodProveedor(catalogoNomVO.getCodProveedor());
        catMaestro.setCodBarra(catalogoNomVO.getCodBarra());
        catMaestro.setClave_de_linea_de_producto(catalogoNomVO.getClave_de_linea_de_producto());
        catMaestro.setFamilia(catalogoNomVO.getFamilia());
        catMaestro.setSubFamilia(catalogoNomVO.getSubFamilia());
        catMaestro.setSubSubFamilia(catalogoNomVO.getSubSubFamilia());
        catMaestro.setDescripcion(catalogoNomVO.getDescripcion());
        catMaestro.setCategory(catalogoNomVO.getCategory());
        catMaestro.setMiddleCategory(catalogoNomVO.getMiddleCategory());
        catMaestro.setSubcategory(catalogoNomVO.getSubcategory());
        catMaestro.setSmallCategory(catalogoNomVO.getSmallCategory());
        catMaestro.setProduct(catalogoNomVO.getProduct());
        catMaestro.setFraccAr(catalogoNomVO.getFraccAr());
        catMaestro.setInner_(catalogoNomVO.getInner_());
        catMaestro.setValidacion(catalogoNomVO.getValidacion());
        catMaestro.setFvMxp(catalogoNomVO.getFvMxp());
        catMaestro.setFvRmb(catalogoNomVO.getFvRmb());
        catMaestro.setCtRmb(catalogoNomVO.getCtRmb());
        catMaestro.setTextil(catalogoNomVO.getTextil());
        catMaestro.setCalzado(catalogoNomVO.getCalzado());
        catMaestro.setFechaRegistro(FechaUtil.convierteFechaStringTOTimestamp(catalogoNomVO.getFechaRegistro()));
        catMaestro.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
        catMaestro.setArchivoExcel(catalogoNomVO.getArchivoExcel());
        catMaestro.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
        catalogoNomDao.actualizar(catMaestro);
        return catalogoNomVO;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public List<CatalogoMaestroVO> buscarErrores() throws Exception {
        List<CatalogoMaestroVO> catalogoMaestroVO = new ArrayList<>(0);
        catalogoMaestroVO = catalogoNomDao.obtenerErrores();
        return catalogoMaestroVO;
    }
    
    
}
