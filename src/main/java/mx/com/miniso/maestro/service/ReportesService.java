
package mx.com.miniso.maestro.service;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.dao.ReportesDao;
import mx.com.miniso.maestro.vo.ReporteConfirmacionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class ReportesService {
    
    @Autowired
    private ReportesDao reporteDao;
    
    public List<ReporteConfirmacionVO> Confirmacion(){
        List<ReporteConfirmacionVO> reporteConfirmacion = reporteDao.buscarTodos();
        return reporteConfirmacion;
    }
    
    
}
