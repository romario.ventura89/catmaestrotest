package mx.com.miniso.maestro.service.compras;

import java.util.List;
import mx.com.miniso.maestro.dao.compras.ReporteComprasDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReporteComprasService {
    
    @Autowired
    private ReporteComprasDao reporteComprasDao;
    
    public List<String> ComprasCatalogo() throws Exception{
        List<String> reporteConfirmacion = reporteComprasDao.obtenerSkuCompras();
        return reporteConfirmacion;
    }
}
