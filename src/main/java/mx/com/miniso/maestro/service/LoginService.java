package mx.com.miniso.maestro.service;

import mx.com.miniso.maestro.dao.UsuarioDao;
import mx.com.miniso.maestro.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
        
    @Autowired
    private UsuarioDao usuarioDao;

    public Usuario obtenerUsuarioXId(Usuario usuario) throws Exception {

        return usuarioDao.obtenerUsuarioXId(usuario);
    }
    
    public Usuario obtenerUsuarioXuserName(Usuario usuario) throws Exception {

        return usuarioDao.obtenerUsuarioXuserName(usuario);
    }
    
    
}
