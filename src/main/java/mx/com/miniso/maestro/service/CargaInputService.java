package mx.com.miniso.maestro.service;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.constantes.DescripcionEnum;
import mx.com.miniso.maestro.constantes.SkuEstatusEnum;
import mx.com.miniso.maestro.dao.CatalogoDao;
import mx.com.miniso.maestro.dao.CatalogoInputDao;
import mx.com.miniso.maestro.dto.Input;
import mx.com.miniso.maestro.ex.ConfirmacionNumeroOrdenExcepcion;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.util.ValidadorJson;
import mx.com.miniso.maestro.vo.CargaInputVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CargaInputService {

    final static Logger logger = LoggerFactory.getLogger(CargaInputService.class);
    
    @Autowired
    private CatalogoInputDao catalogoInputDao;
    
    @Autowired
    private CatalogoDao catalogoDao;
    
    private String descripcion;

    public CargaInputVO obtenerDatos(CargaInputVO cargaInputVO, MultipartFile archivo) throws IOException, Exception {
        InputStreamReader crunchifyReader = new InputStreamReader(archivo.getInputStream());
        BufferedReader br = new BufferedReader(crunchifyReader);
        List<Input> results;
        Gson gson = new Gson();
        results = new ArrayList<>(0);
        String registro = br.readLine();
        List<String> skus = new ArrayList<String>(0);
        skus = catalogoDao.obtenerListaSKU();
            while (registro != null) {
                Input input = new Input();
                input = gson.fromJson(registro, Input.class);
                results.add(input);
                registro = br.readLine();
            }
            for(Input input : results){
                setDescripcion("");
                if(!skus.contains(input.getCode())){
                    if(realizaValidaciones(input)){
                        cargaInputVO.getCorrectos().add(input);
                    }else{
                        input.setDescripcion(getDescripcion());
                        cargaInputVO.getErroneos().add(input);
                    }
                }
            }
        return cargaInputVO;
    }
    
    public boolean realizaValidaciones(Input input){
        ValidadorJson validadorJson = new ValidadorJson();
        if(!input.getCode().isEmpty() && input.getCode()!= null){
            if(!validadorJson.validaEsNumero(input.getCode())){
                setDescripcion(DescripcionEnum.CBARRASLETRAS.getDesc());
                return false;
            }
            if(!validadorJson.validaEsNumero(input.getUuid())){
                return false;
            }
        }else{
            return false;
        }
        if(input.getUuid()!=null && !input.getUuid().isEmpty()){
            if(!validadorJson.validaEsNumero(input.getUuid())){
                setDescripcion(DescripcionEnum.UUIDLETRAS.getDesc());
                return false;
            }
        }else{
            setDescripcion(DescripcionEnum.UUID.getDesc());
            return false;
        }
        return true;
    }

    public void guarda(CargaInputVO cargaInputVO) throws ConfirmacionNumeroOrdenExcepcion, Exception {
        boolean actualizar = false;
        Input input = new Input();
        for(Input inputCorrectos : cargaInputVO.getCorrectos()){
            input = catalogoInputDao.obtenerXSkuInput(inputCorrectos);
            if (input != null) {
                actualizar = true;
            }
            if (actualizar && input != null) {
                inputCorrectos.setIdgdinput(input.getIdgdinput());
                inputCorrectos.setFechaRegistro(input.getFechaRegistro());
                inputCorrectos.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
                inputCorrectos.setNumeroJson(cargaInputVO.getNumeroJson());
                inputCorrectos.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
                cargaInputVO.getActualizados().add(inputCorrectos);
                catalogoInputDao.actualizar(inputCorrectos);
            }else{
                inputCorrectos.setFechaRegistro(FechaUtil.convierteFechaStringTOTimestamp(cargaInputVO.getFechaCarga()));
                inputCorrectos.setEstatus(SkuEstatusEnum.NUEVO.getId());
                inputCorrectos.setNumeroJson(cargaInputVO.getNumeroJson());
                cargaInputVO.getNuevos().add(inputCorrectos);
                catalogoInputDao.guardar(inputCorrectos);
            }
        }
        guardaErroneos(cargaInputVO);
    }

    public void guardaErroneos(CargaInputVO cargaInputVO) throws ConfirmacionNumeroOrdenExcepcion, Exception {
        Input input = new Input();
        ValidadorJson validadorJson = new ValidadorJson();
        for(Input inputErroneos : cargaInputVO.getErroneos()){
            if(validadorJson.validaTamanio(13, inputErroneos.getCode())){
                input = catalogoInputDao.obtenerXSkuInput(inputErroneos);
                if (input == null) {
                    inputErroneos.setFechaRegistro(FechaUtil.convierteFechaStringTOTimestamp(cargaInputVO.getFechaCarga()));
                    inputErroneos.setEstatus(SkuEstatusEnum.ERRONEO.getId());
                    inputErroneos.setNumeroJson(cargaInputVO.getNumeroJson());
                    cargaInputVO.getErrores().add(inputErroneos);
                    catalogoInputDao.guardar(inputErroneos);
                }
            }
        }
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
