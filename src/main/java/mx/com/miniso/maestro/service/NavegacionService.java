package mx.com.miniso.maestro.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import mx.com.miniso.maestro.dao.NavegacionDao;
import mx.com.miniso.maestro.dao.SesionDao;
import mx.com.miniso.maestro.dto.Navegacion;
import mx.com.miniso.maestro.dto.Sesion;
import mx.com.miniso.maestro.dto.Usuario;
import mx.com.miniso.maestro.util.FechaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NavegacionService {

    @Autowired
    private SesionDao sesionDao;
    @Autowired
    private NavegacionDao navegacionDao;

    public void guarda(Usuario usuario, String path, Object objecto) throws Exception {

        Sesion sesion = new Sesion();
        sesion.setIdUsuario(usuario.getIdUsuario());

        sesion = sesionDao.obtenerXUsuario(sesion);
        if (sesion == null) {
            creaNuevaSesion(usuario, path, objecto);
        } else {
            Navegacion navegacion = new Navegacion();
            navegacion.setIdNavegacion(sesion.getIdNavegacion());
            navegacion = navegacionDao.obtenerXId(navegacion);
            if (navegacion == null) {
                creaNavegacion(path, objecto);
            } else {
                navegacion.setPath(path);
                navegacion.setObjeto(guardaObjeto(objecto));
                navegacion.setEstatus(true);
                navegacion.setFechaCreacion(FechaUtil.creaFechaActualTimestamp());
                navegacionDao.actualizar(navegacion);
            }

        }

    }

    private void creaNuevaSesion(Usuario usuario, String path, Object objecto) throws IOException, Exception {
        Navegacion navegacion = creaNavegacion(path, objecto);
        Sesion sesion = new Sesion();
        sesion.setIdUsuario(usuario.getIdUsuario());
        sesion.setIdNavegacion(navegacion.getIdNavegacion());
        sesionDao.guardar(sesion);
    }

    private Navegacion creaNavegacion(String path, Object objecto) throws IOException, Exception {
        Navegacion navegacion = new Navegacion();
        navegacion.setPath(path);
        navegacion.setFechaCreacion(FechaUtil.creaFechaActualTimestamp());
        navegacion.setEstatus(true);
        navegacion.setObjeto(guardaObjeto(objecto));
        navegacion = navegacionDao.guardar(navegacion);
        return navegacion;
    }

    private byte[] guardaObjeto(Object object) throws IOException {
        if (object == null) {
            return null;
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] bytes = null;
        try {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(object);
            out.flush();
            bytes = bos.toByteArray();
        } finally {
            bos.close();

        }
        return bytes;
    }

    public Navegacion buscar(Usuario usuario) throws Exception {
        Sesion sesion = new Sesion();
        sesion.setIdUsuario(usuario.getIdUsuario());
        sesion = sesionDao.obtenerXUsuario(sesion);
        if (sesion == null) {
            return null;
        }
        Navegacion navegacion = new Navegacion();
        navegacion.setIdNavegacion(sesion.getIdNavegacion());
        navegacion = navegacionDao.obtenerXId(navegacion);
        return navegacion;
    }
}
