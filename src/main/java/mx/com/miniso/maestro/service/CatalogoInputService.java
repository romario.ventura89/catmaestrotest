package mx.com.miniso.maestro.service;

import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.constantes.CatalogoEnum;
import mx.com.miniso.maestro.constantes.SkuEstatusEnum;
import mx.com.miniso.maestro.dao.CatalogoInputDao;
import mx.com.miniso.maestro.dao.SociedadDao;
import mx.com.miniso.maestro.dao.TipoOrdenDao;
import mx.com.miniso.maestro.dto.Input;
import mx.com.miniso.maestro.dto.Sociedad;
import mx.com.miniso.maestro.dto.TipoOrden;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CatalogoInputVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoInputVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CatalogoInputService {
    
    final static Log logger = LogFactory.getLog(CatalogoInputService.class);
    
    @Autowired    
    private SociedadDao sociedadDao;
    
    @Autowired 
    private CatalogoInputDao catalogoInputDao;
    
    @Autowired
    private TipoOrdenDao tipoOrdenDao;
    
    private List<Sociedad> sociedades;
    private List<TipoOrden> tipoOrdenes;
    
    public List<CatalogoInputVO> buscarCatalogo(FiltrosCatalogoInputVO filtrosCatalogoInputVO) throws Exception {
        List<CatalogoInputVO> catalogoInput = new ArrayList<>(0);
        if (filtrosCatalogoInputVO == null) {
            return catalogoInput;
        }
        catalogoInput = catalogoInputDao.obtenerXParametros(filtrosCatalogoInputVO);
        return catalogoInput;
    }
    
    public List<CatalogoInputVO> buscarExcel(FiltrosCatalogoInputVO filtrosCatalogoInputVO) throws Exception {
        List<CatalogoInputVO> catalogoInput = new ArrayList<>(0);
        if (filtrosCatalogoInputVO == null) {
            return catalogoInput;
        }
        catalogoInput = catalogoInputDao.obtenerExcel(filtrosCatalogoInputVO);
        return catalogoInput;
    }
    
    public List<CatalogoInputVO> obtenerDetalle(CatalogoInputVO catalogoInputVO) {
        List<CatalogoInputVO> detalles = catalogoInputDao.obtenDetalleErreneo(catalogoInputVO);
        return detalles;
    }
    
    public CatalogoInputVO actualizar(CatalogoInputVO catalogoInputVO,String idGdInput) {
        Input input = new Input();
        input.setIdgdinput(Long.parseLong(idGdInput));
        input.setCode(catalogoInputVO.getCodeBarras());
        input.setMunit(catalogoInputVO.getMunit());
        catalogoInputVO.setQpc("13");
        input.setQpc(Long.parseLong(catalogoInputVO.getQpc()));
        input.setQpcStr(catalogoInputVO.getQpcStr());
        input.setRtlPrc(catalogoInputVO.getRtlPrc());
        input.setUuid(catalogoInputVO.getUuid());
        input.setNumeroJson(catalogoInputVO.getNumeroJson());
        input.setEstatus(SkuEstatusEnum.ACTUALIZADO.getId());
        input.setDescripcion(catalogoInputVO.getDescripcion());
        input.setFechaRegistro(FechaUtil.creaFechaActualTimestamp());
        input.setFechaActualizacion(FechaUtil.creaFechaActualTimestamp());
        
        catalogoInputDao.actualizar(input);
        return catalogoInputVO;
    }
    
    public void eliminar(CatalogoInputVO catalogoInputVO) {
        Input input = new Input();
        input.setCode(catalogoInputVO.getCodeBarras());
        input = catalogoInputDao.obtenerXSku(input);
        if(input == null){
            return;
        }else{
            catalogoInputDao.borrar(input);
        }
    }
    
     public void verificaCatalogo(CatalogoEnum catalogoEnum) {
        if (catalogoEnum.equals(CatalogoEnum.CAT_SOCIEDAD)) {
            if (sociedades == null) {
                sociedades = sociedadDao.buscarTodos();
            }
        } else if (catalogoEnum.equals(CatalogoEnum.CAT_TIPO_ORDEN)) {
            if (tipoOrdenes == null) {
                tipoOrdenes = tipoOrdenDao.buscarTodos();
            }
        }
    }
     
     public List<Sociedad> getCatalogoSociedades() {
        verificaCatalogo(CatalogoEnum.CAT_SOCIEDAD);
        return sociedades;
    }
    
     public Sociedad buscarSociedad(Sociedad sociedad) {
        verificaCatalogo(CatalogoEnum.CAT_SOCIEDAD);
        for (Sociedad catSociedadIter : sociedades) {
            if (catSociedadIter.equals(sociedad)) {
                return catSociedadIter;
            }
            
        }
        return null;
    }   
     public Sociedad buscarCatSociedad(long idSociedad) {
        Sociedad sociedad = new Sociedad();
        sociedad.setIdSociedad(idSociedad);
        return buscarSociedad(sociedad);
    }
     
     public List<TipoOrden> getCatalogoTipoOrdenes(){
        verificaCatalogo(CatalogoEnum.CAT_TIPO_ORDEN);
        return tipoOrdenes;
    }
    
     public TipoOrden buscarTipoOrden(TipoOrden tipoOrden) {
        verificaCatalogo(CatalogoEnum.CAT_TIPO_ORDEN);
        for (TipoOrden catTipoOrdenIter : tipoOrdenes) {
            if (catTipoOrdenIter.equals(tipoOrden)) {
                return catTipoOrdenIter;
            }
            
        }
        return null;
    }
     
    public TipoOrden buscarCatTipoOrden(long idTipoOrden) {
        TipoOrden tipoOrden = new TipoOrden();
        tipoOrden.setIdTipoOrden(idTipoOrden);
        return buscarTipoOrden(tipoOrden);
    }
     
    public List<CatalogoInputVO> buscarErrores() throws Exception {
        List<CatalogoInputVO> catalogoErrores = new ArrayList<>(0);        
        catalogoErrores = catalogoInputDao.obtenerErrores();
        return catalogoErrores;
    } 
     
}
