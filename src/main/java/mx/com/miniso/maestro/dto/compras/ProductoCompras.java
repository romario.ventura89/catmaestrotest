package mx.com.miniso.maestro.dto.compras;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "productoCompra")
public class ProductoCompras implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idProductoCompra")
    private long idProductoCompra;

    @Column(name = "fechaRegistro")
    private Timestamp fechaRegistro;

    @Column(name = "sku")
    private String sku;

    @Column(name = "codigoBarras")
    private String codigoBarras;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "descripcionIngles")
    private String descripcionIngles;

    @Column(name = "familia")
    private String familia;

    @Column(name = "subFamilia")
    private String subFamilia;

    @Column(name = "subSubFamilia")
    private String subSubFamilia;

    @Column(name = "estatus")
    private boolean estatus;

    public long getIdProductoCompra() {
        return idProductoCompra;
    }

    public void setIdProductoCompra(long idProductoCompra) {
        this.idProductoCompra = idProductoCompra;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionIngles() {
        return descripcionIngles;
    }

    public void setDescripcionIngles(String descripcionIngles) {
        this.descripcionIngles = descripcionIngles;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getSubFamilia() {
        return subFamilia;
    }

    public void setSubFamilia(String subFamilia) {
        this.subFamilia = subFamilia;
    }

    public String getSubSubFamilia() {
        return subSubFamilia;
    }

    public void setSubSubFamilia(String subSubFamilia) {
        this.subSubFamilia = subSubFamilia;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (int) (this.idProductoCompra ^ (this.idProductoCompra >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoCompras other = (ProductoCompras) obj;
        if (this.idProductoCompra != other.idProductoCompra) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProductoCompra{" + "idProductoCompra=" + idProductoCompra + ", fechaRegistro=" + fechaRegistro + ", sku=" + sku + ", codigoBarras=" + codigoBarras + ", descripcion=" + descripcion + ", descripcionIngles=" + descripcionIngles + ", familia=" + familia + ", subFamilia=" + subFamilia + ", subSubFamilia=" + subSubFamilia + ", estatus=" + estatus + '}';
    }

}
