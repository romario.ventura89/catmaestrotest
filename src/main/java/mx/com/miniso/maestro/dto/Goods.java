package mx.com.miniso.maestro.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "goods")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idGoods")
    private long idGoods;
    
    @Column(name = "brand")
    private String brand;
    
    @Column(name = "code")
    private String code;
    
    @Column(name = "manufactor")
    private String manufactor;
    
    @Column(name = "munit")
    private String munit;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "origin")
    private String origin;
    
    @Column(name = "qpc")
    private long qpc;
    
    @Column(name = "rtlPrc")
    private String rtlPrc;
    
    @Column(name = "sort")
    private String sort;
    
    @Column(name = "spec")
    private String spec;
    
    @Column(name = "tm")
    private String tm;
    
    @Column(name = "uuid")
    private String uuid;

    @Column(name = "fechaRegistro")
    private Timestamp fechaRegistro;

    @Column(name = "fechaActualizacion")
    private Timestamp fechaActualizacion;

    @Column(name = "estatus")
    private Integer estatus;
    
    @Column(name ="numeroJson")
    private String numeroJson;

    @Column(name ="descripcion")
    private String descripcion;
    
    public long getIdGoods() {
        return idGoods;
    }

    public void setIdGoods(long idGoods) {
        this.idGoods = idGoods;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getManufactor() {
        return manufactor;
    }

    public void setManufactor(String manufactor) {
        this.manufactor = manufactor;
    }

    public String getMunit() {
        return munit;
    }

    public void setMunit(String munit) {
        this.munit = munit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public long getQpc() {
        return qpc;
    }

    public void setQpc(long qpc) {
        this.qpc = qpc;
    }

    public String getRtlPrc() {
        return rtlPrc;
    }

    public void setRtlPrc(String rtlPrc) {
        this.rtlPrc = rtlPrc;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getTm() {
        return tm;
    }

    public void setTm(String tm) {
        this.tm = tm;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Timestamp fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public String getNumeroJson() {
        return numeroJson;
    }

    public void setNumeroJson(String numeroJson) {
        this.numeroJson = numeroJson;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (int) (this.idGoods ^ (this.idGoods >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Goods other = (Goods) obj;
        if (this.idGoods != other.idGoods) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Goods{" + "idGoods=" + idGoods + ", brand=" + brand + ", code=" + code + ", manufactor=" + manufactor + ", munit=" + munit + ", name=" + name + ", origin=" + origin + ", qpc=" + qpc + ", rtlPrc=" + rtlPrc + ", sort=" + sort + ", spec=" + spec + ", tm=" + tm + ", uuid=" + uuid + ", fechaRegistro=" + fechaRegistro + ", fechaActualizacion=" + fechaActualizacion + ", estatus=" + estatus + ", numeroJson=" + numeroJson + ", descripcion=" + descripcion + '}';
    }

    
}
