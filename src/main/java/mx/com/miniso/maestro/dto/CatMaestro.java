package mx.com.miniso.maestro.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "catMaestro")
public class CatMaestro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCatMaestro")
    private long idCatMaestro;
    
    @Column(name = "codProveedor")
    private String codProveedor;
    
    @Column(name = "codBarra")
    private String codBarra;
    
    @Column(name = "clave_de_linea_de_producto")
    private String clave_de_linea_de_producto;

    @Column(name = "familia")
    private String familia;
    
    @Column(name = "subFamilia")
    private String subFamilia;
    
    @Column(name = "subSubFamilia")
    private String subSubFamilia;
    
    @Column(name = "descripcion")
    private String descripcion;
    
    @Column(name = "category")
    private String category;
    
    @Column(name = "middleCategory")
    private String middleCategory;
    
    @Column(name = "subcategory")
    private String subcategory;
    
    @Column(name = "smallCategory")
    private String smallCategory;
    
    @Column(name = "product")
    private String product;
    
    @Column(name = "fraccAr")
    private String fraccAr;
    
    @Column(name = "inner_")
    private String inner_;
    
    @Column(name = "validacion")
    private String validacion;
    
    @Column(name = "fvMxp")
    private String fvMxp;
    
    @Column(name = "fvRmb")
    private String fvRmb;
    
    @Column(name = "ctRmb")
    private String ctRmb;
    
    @Column(name = "textil")
    private String textil;
    
    @Column(name = "calzado")
    private String calzado;

    @Column(name = "fechaRegistro")
    private Timestamp fechaRegistro;

    @Column(name = "fechaActualizacion")
    private Timestamp fechaActualizacion;

    @Column(name ="archivoExcel")
    private String archivoExcel;
    
    @Column(name ="estatus")
    private Integer estatus;
    
    @Column(name ="descripcionError")
    private String descripcionError;

    public long getIdCatMaestro() {
        return idCatMaestro;
    }

    public void setIdCatMaestro(long idCatMaestro) {
        this.idCatMaestro = idCatMaestro;
    }

    public String getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(String codProveedor) {
        this.codProveedor = codProveedor;
    }

    public String getCodBarra() {
        return codBarra;
    }

    public void setCodBarra(String codBarra) {
        this.codBarra = codBarra;
    }

    public String getClave_de_linea_de_producto() {
        return clave_de_linea_de_producto;
    }

    public void setClave_de_linea_de_producto(String clave_de_linea_de_producto) {
        this.clave_de_linea_de_producto = clave_de_linea_de_producto;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getSubFamilia() {
        return subFamilia;
    }

    public void setSubFamilia(String subFamilia) {
        this.subFamilia = subFamilia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMiddleCategory() {
        return middleCategory;
    }

    public void setMiddleCategory(String middleCategory) {
        this.middleCategory = middleCategory;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getSmallCategory() {
        return smallCategory;
    }

    public void setSmallCategory(String smallCategory) {
        this.smallCategory = smallCategory;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getFraccAr() {
        return fraccAr;
    }

    public void setFraccAr(String fraccAr) {
        this.fraccAr = fraccAr;
    }

    public String getInner_() {
        return inner_;
    }

    public void setInner_(String inner_) {
        this.inner_ = inner_;
    }

    public String getValidacion() {
        return validacion;
    }

    public void setValidacion(String validacion) {
        this.validacion = validacion;
    }

    public String getFvMxp() {
        return fvMxp;
    }

    public void setFvMxp(String fvMxp) {
        this.fvMxp = fvMxp;
    }

    public String getFvRmb() {
        return fvRmb;
    }

    public void setFvRmb(String fvRmb) {
        this.fvRmb = fvRmb;
    }
    
    public String getTextil() {
        return textil;
    }

    public void setTextil(String textil) {
        this.textil = textil;
    }

    public String getCalzado() {
        return calzado;
    }

    public void setCalzado(String calzado) {
        this.calzado = calzado;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Timestamp fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getArchivoExcel() {
        return archivoExcel;
    }

    public void setArchivoExcel(String archivoExcel) {
        this.archivoExcel = archivoExcel;
    }

    public String getSubSubFamilia() {
        return subSubFamilia;
    }

    public void setSubSubFamilia(String subSubFamilia) {
        this.subSubFamilia = subSubFamilia;
    }

    public String getCtRmb() {
        return ctRmb;
    }

    public void setCtRmb(String ctRmb) {
        this.ctRmb = ctRmb;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    } 

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (this.idCatMaestro ^ (this.idCatMaestro >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CatMaestro other = (CatMaestro) obj;
        if (this.idCatMaestro != other.idCatMaestro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CatMaestro{" + "idCatMaestro=" + idCatMaestro + ", codProveedor=" + codProveedor + ", codBarra=" + codBarra + ", clave_de_linea_de_producto=" + clave_de_linea_de_producto + ", familia=" + familia + ", subFamilia=" + subFamilia + ", subSubFamilia=" + subSubFamilia + ", descripcion=" + descripcion + ", category=" + category + ", middleCategory=" + middleCategory + ", subcategory=" + subcategory + ", smallCategory=" + smallCategory + ", product=" + product + ", fraccAr=" + fraccAr + ", inner_=" + inner_ + ", validacion=" + validacion + ", fvMxp=" + fvMxp + ", fvRmb=" + fvRmb + ", ctRmb=" + ctRmb + ", textil=" + textil + ", calzado=" + calzado + ", fechaRegistro=" + fechaRegistro + ", fechaActualizacion=" + fechaActualizacion + ", archivoExcel=" + archivoExcel + ", estatus=" + estatus + ", descripcionError=" + descripcionError + '}';
    }

}
