package mx.com.miniso.maestro.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "navegacion")
public class Navegacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idNavegacion")
    private long idNavegacion;

    @Column(name = "path")
    private String path;

    @Column(name = "fechaCreacion")
    private Timestamp fechaCreacion;

    @Column(name = "objeto")
    private byte[] objeto;

    @Column(name = "estatus")
    private boolean estatus;

    public long getIdNavegacion() {
        return idNavegacion;
    }

    public void setIdNavegacion(long idNavegacion) {
        this.idNavegacion = idNavegacion;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Timestamp getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public byte[] getObjeto() {
        return objeto;
    }

    public void setObjeto(byte[] objeto) {
        this.objeto = objeto;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (this.idNavegacion ^ (this.idNavegacion >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Navegacion other = (Navegacion) obj;
        if (this.idNavegacion != other.idNavegacion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Navegacion{" + "idNavegacion=" + idNavegacion + ", path=" + path + ", fechaCreacion=" + fechaCreacion + ", objeto=" + objeto + ", estatus=" + estatus + '}';
    }

}
