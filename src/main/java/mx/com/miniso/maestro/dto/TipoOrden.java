
package mx.com.miniso.maestro.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "tipoOrden")
public class TipoOrden implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "idTipoOrden")
    private long idTipoOrden;
    
    @Column(name = "descripcion")
    private String descripcion;

    public long getIdTipoOrden() {
        return idTipoOrden;
    }

    public void setIdTipoOrden(long idTipoOrden) {
        this.idTipoOrden = idTipoOrden;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (int) (this.idTipoOrden ^ (this.idTipoOrden >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoOrden other = (TipoOrden) obj;
        if (this.idTipoOrden != other.idTipoOrden) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoOrden{" + "idTipoOrden=" + idTipoOrden + ", descripcion=" + descripcion + '}';
    }
    
}
