package mx.com.miniso.maestro.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "gdinput")
public class Input implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idgdinput")
    private long idgdinput;
    
    @Column(name = "code")
    private String code;
    
    @Column(name = "munit")
    private String munit;
    
    @Column(name = "qpc")
    private long qpc;
    
    @Column(name = "qpcStr")
    private String qpcStr;
    
    @Column(name = "rtlPrc")
    private String rtlPrc;
    
    @Column(name = "uuid")
    private String uuid;

    @Column(name = "fechaRegistro")
    private Timestamp fechaRegistro;

    @Column(name = "fechaActualizacion")
    private Timestamp fechaActualizacion;

    @Column(name = "estatus")
    private Integer estatus;
    
    @Column(name ="numeroJson")
    private String numeroJson;
    
    @Column(name ="descripcion")
    private String descripcion;

    public long getIdgdinput() {
        return idgdinput;
    }

    public void setIdgdinput(long idgdinput) {
        this.idgdinput = idgdinput;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMunit() {
        return munit;
    }

    public void setMunit(String munit) {
        this.munit = munit;
    }

    public long getQpc() {
        return qpc;
    }

    public void setQpc(long qpc) {
        this.qpc = qpc;
    }

    public String getQpcStr() {
        return qpcStr;
    }

    public void setQpcStr(String qpcStr) {
        this.qpcStr = qpcStr;
    }

    public String getRtlPrc() {
        return rtlPrc;
    }

    public void setRtlPrc(String rtlPrc) {
        this.rtlPrc = rtlPrc;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Timestamp fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public String getNumeroJson() {
        return numeroJson;
    }

    public void setNumeroJson(String numeroJson) {
        this.numeroJson = numeroJson;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
  
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (int) (this.idgdinput ^ (this.idgdinput >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Input other = (Input) obj;
        if (this.idgdinput != other.idgdinput) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Input{" + "idgdinput=" + idgdinput + ", code=" + code + ", munit=" + munit + ", qpc=" + qpc + ", qpcStr=" + qpcStr + ", rtlPrc=" + rtlPrc + ", uuid=" + uuid + ", fechaRegistro=" + fechaRegistro + ", fechaActualizacion=" + fechaActualizacion + ", estatus=" + estatus + ", numeroJson=" + numeroJson + ", descripcion=" + descripcion + '}';
    } 
   
}
