package mx.com.miniso.maestro.constantes;



public enum SkuEstatusEnum {
    NUEVO(1),
    ACTUALIZADO(2),
    ERRONEO(3),
    NOTIFICACION(4),
    VISTA_ALMACEN(5),
    SELECCIONADA(6),
    EN_PXXX(7),
    RECHAZADA(8),
    ACEPTADA(9);

    private final int id;

    private SkuEstatusEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
