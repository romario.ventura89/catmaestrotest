
package mx.com.miniso.maestro.constantes;

public enum SociedadMenu {
    
     MEXICO_CHINA(1),
     MEXICO_COLOMBIA(2);
   
    int id;
   
    SociedadMenu(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


}
