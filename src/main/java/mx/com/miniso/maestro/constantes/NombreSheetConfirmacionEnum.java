package mx.com.miniso.maestro.constantes;

public enum NombreSheetConfirmacionEnum {
    GENERAL("General"),
    QIANHAI("Bonded(Qianhai)"),
    PINGSHAN("Bonded(Pingshan)");

    private final String nombre;

    private NombreSheetConfirmacionEnum(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
