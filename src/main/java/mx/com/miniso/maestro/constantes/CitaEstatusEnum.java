package mx.com.miniso.maestro.constantes;



public enum CitaEstatusEnum {
    EN_PROCESO(1),
    CANCELADA_CLIENTE(2),
    REAGENDADA(3),
    NOTIFICACION(4),
    VISTA_ALMACEN(5),
    SELECCIONADA(6),
    EN_PXXX(7),
    RECHAZADA(8),
    ACEPTADA(9);

    private final int id;

    private CitaEstatusEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
