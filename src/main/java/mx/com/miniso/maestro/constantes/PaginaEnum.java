package mx.com.miniso.maestro.constantes;

public enum PaginaEnum {

    LOGIN(0, "redirect:/sitio/publico/login.jsp"),
    BIENVENIDA(1, "redirect:/sitio/privado/usr/bienvenida.jsp"),
    SALIR(2, "redirect:/service/login/salir"),
    CATMAESTRO(3, "redirect:/sitio/privado/catalogo/mi_catalogo.jsp"),
    CARGAJSON(4, "redirect:/sitio/privado/catalogo/carga_goods.jsp"),
    CARGAJSONINPUT(5, "redirect:/sitio/privado/catalogo/carga_input.jsp"),
    CATMAESTROINPUT(6, "redirect:/sitio/privado/catalogo/mi_catalogoInput.jsp"),
    ACTUALIZACATMAESTRO(7, "redirect:/sitio/privado/actualizacion/carga_excel.jsp"),
    CATACTUALIZADO(8, "redirect:/sitio/privado/actualizacion/catalogoActualizado.jsp"),
    CARGANOM(9, "redirect:/sitio/privado/archivoNom/carga_nom.jsp"),
    CATNOM(10, "redirect:/sitio/privado/archivoNom/catalogoNom.jsp"),
    REPORTE_PEDIDOS(11, "redirect:/sitio/privado/reporte/pedidos.jsp"),
    REACTIVACION_FACTURA(12, "redirect:/sitio/privado/facturas/reactivacion_facturas.jsp");

    int id;
    String path;

    PaginaEnum(int id, String path) {
        this.id = id;
        this.path = path;

    }

    public int getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

}
