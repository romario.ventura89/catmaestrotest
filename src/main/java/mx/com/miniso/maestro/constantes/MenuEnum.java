package mx.com.miniso.maestro.constantes;

public enum MenuEnum {

    DEFAULT(0, "redirect:/sitio/publico/menuPrincipal.jsp"),
    CHINA(1, "redirect:/sitio/privado/menus/menuChina.jsp"),
    COLOMBIA(2, "redirect:/sitio/privado/menus/menuColombia.jsp"),
    GENERAL(3, "redirect:/sitio/privado/menus/menuGeneral.jsp");

    int id;
    String path;

    MenuEnum(int id, String path) {
        this.id = id;
        this.path = path;

    }

    public int getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

}
