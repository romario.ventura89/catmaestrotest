package mx.com.miniso.maestro.constantes;

public enum DescripcionEnum {

    SKU(1, "El valor de SKU esta vacio"),
    SKULETRAS(2, "El valor de SKU no es numerico"),
    UUID(3, "El valor de Uuid esta vacio"),
    UUIDLETRAS(4, "El valor de Uuid no es numerico"),
    CBARRAS(3, "El valor de Codigo de barras esta vacio"),
    CBARRASLETRAS(4, "El valor de Codigo de barras no es numerico"),
    NOENCONTRADO(5,"No fue encontrado el valor de SKU o Codigo de Barras"),
    CBARRASTAMANIO(6,"El Codigo de barras no cumple con el tamanio adecuado");
    int id;
    String desc;

    DescripcionEnum(int id, String desc) {
        this.id = id;
        this.desc = desc;

    }

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

}
