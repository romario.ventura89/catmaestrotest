
package mx.com.miniso.maestro.constantes;



public class MensajesVista {
    
    public static final String ERROR_EXCEPCION = "<strong>No se pudo realizar el tr&aacute;mite solicitado:</strong> por favor intente m&aacute;s tarde.";
    public static final String EMPTY_ERROR = "<strong>El archivo Json no contiene datos:</strong> por favor verifica el mismo.";

}
