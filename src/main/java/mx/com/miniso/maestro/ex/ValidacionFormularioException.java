package mx.com.miniso.maestro.ex;

import mx.com.miniso.maestro.util.RespuestaValidador;

public class ValidacionFormularioException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RespuestaValidador respuesta;

	public ValidacionFormularioException(String mensaje) {
		super(mensaje);
	}

	public ValidacionFormularioException(int codigo, String mensaje, String dato) {
		super(mensaje);
		this.respuesta = new RespuestaValidador();
		respuesta.setCodigo(codigo);
		respuesta.setMensaje(mensaje);
		respuesta.setDato(dato);
	}

	public RespuestaValidador getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaValidador respuesta) {
		this.respuesta = respuesta;
	}

}
