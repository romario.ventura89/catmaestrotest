package mx.com.miniso.maestro.ex;

public class FacturacionException extends Exception {

    public FacturacionException(String mensaje) {
        super(mensaje);
    }

}
