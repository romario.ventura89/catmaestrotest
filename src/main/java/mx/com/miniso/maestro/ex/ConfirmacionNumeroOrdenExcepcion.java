package mx.com.miniso.maestro.ex;

public class ConfirmacionNumeroOrdenExcepcion extends Exception {

    public ConfirmacionNumeroOrdenExcepcion(String numeroOrden) {
        super("El número de orden ya fue ingresado " + numeroOrden);
    }

}
