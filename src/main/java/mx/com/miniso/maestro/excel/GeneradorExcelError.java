package mx.com.miniso.maestro.excel;

import java.util.List;
import mx.com.miniso.maestro.vo.CatalogoVO;
import mx.com.miniso.maestro.vo.CatalogoInputVO;
import mx.com.miniso.maestro.vo.CatalogoMaestroVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorExcelError extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcelError.class);

    private final List<CatalogoVO> detalleGoods;
    private final String SKU = "code";
    private final String BRAND = "product name";
    private final String MANUFACTOR = "manufactor";
    private final String MUNIT = "munit";
    private final String NAME = "name";
    private final String ORIGIN = "origin";
    private final String QPC = "qpc";
    private final String RTLPRC = "rtlprc";
    private final String SORT = "sort";
    private final String SPEC = "spec";
    private final String TM = "tm";
    private final String UUID = "uuid";
    private final String ESTATUS = "estatus";
    private final List<CatalogoInputVO> detalleInput;
    private final String CODIGOBARRAS = "codigo barras";
    private final String QPCSTR = "qpcstr";
    private final List<CatalogoMaestroVO> detalleExcel;
    private final String CODPROVEDOR = "codProveedor";
    private final String CODBARRAS = "codBarra";
    private final String CLAVE = "clave_de_linea_de_producto";
    private final String FAMILIA = "familia";
    private final String SUBFAMILIA = "subFamilia";
    private final String SUBSUBFAMILIA = "subSubFamilia";
    private final String DESCRIPCION = "descripcion";
    private final String CATEGORY = "category";
    private final String MIDDLECATEGORY = "middleCategory";
    private final String SUBCATEGORY = "subcategory";
    private final String SMALLCATEGORY = "smallCategory";
    private final String PRODUCT = "product";
    private final String FRACCAR = "fraccAr";
    private final String INNER = "inner_";
    private final String VALIDACION = "validacion";
    private final String FVRMB = "fvRmb";
    private final String CTRMB = "ctRmb";
    private final String TEXTIL = "textil";
    private final String CALZADO = "calzado";

    public GeneradorExcelError(List<CatalogoVO> detalleGoods, List<CatalogoInputVO> detalleInput, List<CatalogoMaestroVO> detalleExcel) {
        super("ERRORES");
        this.detalleGoods = detalleGoods;
        this.detalleInput = detalleInput;
        this.detalleExcel = detalleExcel;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet sheet = reporte.createSheet("GOODS");
        crearGoods(sheet);

        sheet = reporte.createSheet("INPUT");
        creaInput(sheet);

        sheet = reporte.createSheet("CATALOGO EXCEL");
        creaCatMaestro(sheet);
        
    }
    
    private void crearGoods(SXSSFSheet sheet) {
        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(BRAND);

        cell = row.createCell(i++);
        cell.setCellValue(SKU);

        cell = row.createCell(i++);
        cell.setCellValue(MANUFACTOR);

        cell = row.createCell(i++);
        cell.setCellValue(MUNIT);

        cell = row.createCell(i++);
        cell.setCellValue(NAME);

        cell = row.createCell(i++);
        cell.setCellValue(ORIGIN);

        cell = row.createCell(i++);
        cell.setCellValue(QPC);

        cell = row.createCell(i++);
        cell.setCellValue(RTLPRC);

        cell = row.createCell(i++);
        cell.setCellValue(SORT);

        cell = row.createCell(i++);
        cell.setCellValue(SPEC);

        cell = row.createCell(i++);
        cell.setCellValue(TM);

        cell = row.createCell(i++);
        cell.setCellValue(UUID);

        cell = row.createCell(i++);
        cell.setCellValue(ESTATUS);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);


        aplicarAutoSize(sheet, i);
        int indexGeneral = 1;
        for (CatalogoVO detalle : detalleGoods) {
            int j = 0;
            row = sheet.createRow(indexGeneral++);
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getBrand());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getCode());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getManufactor());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getMunit());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getName());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getOrigin());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getQpc());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getRtlPrc());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSort());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSpec());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getTm());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getUuid());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getEstatus());
            
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getDescripcion());
        }
    }

    private void creaInput(SXSSFSheet sheet){
        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(CODIGOBARRAS);
        
        cell = row.createCell(i++);
        cell.setCellValue(SKU);
        
        cell = row.createCell(i++);
        cell.setCellValue(MUNIT);
        
        cell = row.createCell(i++);
        cell.setCellValue(QPC);

        cell = row.createCell(i++);
        cell.setCellValue(QPCSTR);
        
        cell = row.createCell(i++);
        cell.setCellValue(RTLPRC);

        cell = row.createCell(i++);
        cell.setCellValue(UUID);

        cell = row.createCell(i++);
        cell.setCellValue(ESTATUS);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);
        
        aplicarAutoSize(sheet, i);
        int indexGeneral = 1;
        for (CatalogoInputVO detalle : detalleInput) {
            int j = 0;
            row = sheet.createRow(indexGeneral++);
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getCodeBarras());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSku());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getMunit());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getQpc());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getQpcStr());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getRtlPrc());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getUuid());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getEstatus());
            
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getDescripcion());
        }
    }
    
    private void creaCatMaestro(SXSSFSheet sheet){
        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(CODPROVEDOR);
        
        cell = row.createCell(i++);
        cell.setCellValue(CODBARRAS);
        
        cell = row.createCell(i++);
        cell.setCellValue(CLAVE);
        
        cell = row.createCell(i++);
        cell.setCellValue(FAMILIA);

        cell = row.createCell(i++);
        cell.setCellValue(SUBFAMILIA);
        
        cell = row.createCell(i++);
        cell.setCellValue(SUBSUBFAMILIA);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);

        cell = row.createCell(i++);
        cell.setCellValue(CATEGORY);
        
        cell = row.createCell(i++);
        cell.setCellValue(MIDDLECATEGORY);
        
        cell = row.createCell(i++);
        cell.setCellValue(SUBCATEGORY);
        
        cell = row.createCell(i++);
        cell.setCellValue(SMALLCATEGORY);

        cell = row.createCell(i++);
        cell.setCellValue(PRODUCT);
        
        cell = row.createCell(i++);
        cell.setCellValue(FRACCAR);

        cell = row.createCell(i++);
        cell.setCellValue(INNER);

        cell = row.createCell(i++);
        cell.setCellValue(VALIDACION);

        cell = row.createCell(i++);
        cell.setCellValue(MUNIT);
        
        cell = row.createCell(i++);
        cell.setCellValue(FVRMB);

        cell = row.createCell(i++);
        cell.setCellValue(CTRMB);
        
        cell = row.createCell(i++);
        cell.setCellValue(TEXTIL);

        cell = row.createCell(i++);
        cell.setCellValue(CALZADO);

        cell = row.createCell(i++);
        cell.setCellValue(ESTATUS);
        
        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);
        
        aplicarAutoSize(sheet, i);
        int indexGeneral = 1;
        for (CatalogoMaestroVO detalle : detalleExcel) {
            int j = 0;
            row = sheet.createRow(indexGeneral++);
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getCodProveedor());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getCodBarra());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getClave_de_linea_de_producto());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getFamilia());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSubFamilia());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSubSubFamilia());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getDescripcion());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getCategory());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getMiddleCategory());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSubcategory());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getSmallCategory());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getProduct());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getFraccAr());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getInner());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getValidacion());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getFvMxp());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getFvRmb());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getCtRmb());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getTextil());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getCalzado());

            cell = row.createCell(j++);
            cell.setCellValue(detalle.getEstatus());
            
            cell = row.createCell(j++);
            cell.setCellValue(detalle.getDescripcionError());
        }
        
    }
    
    private void autoSize(SXSSFSheet sheet) {

    }

    @Override
    protected void agregarEncabezados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
