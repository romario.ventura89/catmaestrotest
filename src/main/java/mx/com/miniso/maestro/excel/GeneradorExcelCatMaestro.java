package mx.com.miniso.maestro.excel;

import java.util.List;
import mx.com.miniso.maestro.vo.CatalogoExcelVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorExcelCatMaestro extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcelCatMaestro.class);

    private final List<CatalogoExcelVO> detalles;
    private final String CODPROVEDOR = "codProveedor";
    private final String CODBARRAS = "codBarra";
    private final String PREDETERMINADO = "predeterminado";
    private final String CLAVE = "clave_de_linea_de_producto";
    private final String FAMILIA = "familia";
    private final String SUBFAMILIA = "subFamilia";
    private final String SUBSUBFAMILIA = "subSubFamilia";
    private final String DESCRIPCION = "descripcion";
    private final String CATEGORY = "category";
    private final String MIDDLECATEGORY = "middleCategory";
    private final String SUBCATEGORY = "subcategory";
    private final String SMALLCATEGORY = "smallCategory";
    private final String PRODUCT = "product";
    private final String FRACCAR = "fraccAr";
    private final String INNER = "inner_";
    private final String VALIDACION = "validacion";
    private final String MUNIT = "fvMxp";
    private final String FVRMB = "fvRmb";
    private final String CTRMB = "ctRmb";
    private final String TEXTIL = "textil";
    private final String CALZADO = "calzado";
    private final String ESTATUS = "estatus";



    public GeneradorExcelCatMaestro(List<CatalogoExcelVO> detalles) {
        super("CatMaestro ");
        this.detalles = detalles;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet generalSheet = null;
        SXSSFSheet qianhaiSheet = null;
        SXSSFSheet pingshanSheet = null;
        
        int indexGeneral = 1;
        int indexPingshan = 1;
        int indexQianhai = 1;
        
        boolean paginaGeneralCreada = false;
        boolean paginaPingshanCreada = false;
        boolean paginaQianhaiCreada = false;
        
        for (CatalogoExcelVO detalle : detalles) {
//            if (detalle.isGeneral()) {
                if (!paginaGeneralCreada) {
                    generalSheet = reporte.createSheet("INPUT");
                    generalSheet = agregaTitulo(generalSheet);
                    paginaGeneralCreada = true;
                }
                agregaDatos(detalle, generalSheet, indexGeneral++);
//            } else if (detalle.isPingshan()) {
//                if (!paginaPingshanCreada) {
//                    pingshanSheet = reporte.createSheet("PINGSHAN");
//                    pingshanSheet = agregaTitulo(pingshanSheet);
//                    paginaPingshanCreada = true;
//                }
//                agregaDatos(detalle, pingshanSheet, indexPingshan++);
//            } else if (detalle.isQianhai()) {
//                if (!paginaQianhaiCreada) {
//                    qianhaiSheet = reporte.createSheet("QIANHAI");
//                    qianhaiSheet = agregaTitulo(qianhaiSheet);
//                    paginaQianhaiCreada = true;
//                }
//                agregaDatos(detalle, qianhaiSheet, indexQianhai++);
//            }
        }
        autoSize(generalSheet);
//        autoSize(qianhaiSheet);
//        autoSize(pingshanSheet);
    }

    private void autoSize(SXSSFSheet sheet) {

    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(CODPROVEDOR);
        
        cell = row.createCell(i++);
        cell.setCellValue(CODBARRAS);
        
        cell = row.createCell(i++);
        cell.setCellValue(CLAVE);
        
        cell = row.createCell(i++);
        cell.setCellValue(FAMILIA);

        cell = row.createCell(i++);
        cell.setCellValue(SUBFAMILIA);
        
        cell = row.createCell(i++);
        cell.setCellValue(SUBSUBFAMILIA);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);

        cell = row.createCell(i++);
        cell.setCellValue(CATEGORY);
        
        cell = row.createCell(i++);
        cell.setCellValue(MIDDLECATEGORY);
        
        cell = row.createCell(i++);
        cell.setCellValue(SUBCATEGORY);
        
        cell = row.createCell(i++);
        cell.setCellValue(SMALLCATEGORY);

        cell = row.createCell(i++);
        cell.setCellValue(PRODUCT);
        
        cell = row.createCell(i++);
        cell.setCellValue(FRACCAR);

        cell = row.createCell(i++);
        cell.setCellValue(INNER);

        cell = row.createCell(i++);
        cell.setCellValue(VALIDACION);

        cell = row.createCell(i++);
        cell.setCellValue(MUNIT);
        
        cell = row.createCell(i++);
        cell.setCellValue(FVRMB);

        cell = row.createCell(i++);
        cell.setCellValue(CTRMB);
        
        cell = row.createCell(i++);
        cell.setCellValue(TEXTIL);

        cell = row.createCell(i++);
        cell.setCellValue(CALZADO);

        cell = row.createCell(i++);
        cell.setCellValue(ESTATUS);        
        
        aplicarAutoSize(sheet, i);
        return sheet;
    }

    @Override
    protected void agregarEncabezados() {

    }

    private void agregaDatos(CatalogoExcelVO detalle, SXSSFSheet sheet, int i) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(detalle.getCodProveedor());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getCodBarra());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getClave_de_linea_de_producto());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getFamilia());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getSubFamilia());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getSubSubFamilia());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getDescripcion());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getCategory());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getMiddleCategory());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getSubcategory());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getSmallCategory());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getProduct());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getFraccAr());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getInner_());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getValidacion());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getFvMxp());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getFvRmb());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getCtRmb());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getTextil());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getCalzado());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getEstatus());
        

    }
}
