package mx.com.miniso.maestro.excel;

import java.util.List;
import mx.com.miniso.maestro.vo.CatalogoInputVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorExcelInput extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcelInput.class);

    private final List<CatalogoInputVO> detalles;
    private final String CODIGOBARRAS = "codigo barras";
    private final String SKU = "SKU";
    private final String MUNIT = "munit";
    private final String QPC = "qpc";
    private final String QPCSTR = "qpcstr";
    private final String RTLPRC = "rtlprc";
    private final String UUID = "uuid";
    private final String ESTATUS = "estatus";
    private final String DESCRIPCION = "descripcion";
    
    public GeneradorExcelInput(List<CatalogoInputVO> detalles) {
        super("Input ");
        this.detalles = detalles;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet generalSheet = null;
        int indexGeneral = 1;
        boolean paginaGeneralCreada = false;
        
        for (CatalogoInputVO detalle : detalles) {
            if (!paginaGeneralCreada) {
                generalSheet = reporte.createSheet("INPUT");
                generalSheet = agregaTitulo(generalSheet);
                paginaGeneralCreada = true;
            }
            agregaDatos(detalle, generalSheet, indexGeneral++);
        }
        autoSize(generalSheet);
    }

    private void autoSize(SXSSFSheet sheet) {

    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(CODIGOBARRAS);
        
        cell = row.createCell(i++);
        cell.setCellValue(SKU);
        
        cell = row.createCell(i++);
        cell.setCellValue(MUNIT);
        
        cell = row.createCell(i++);
        cell.setCellValue(QPC);

        cell = row.createCell(i++);
        cell.setCellValue(QPCSTR);
        
        cell = row.createCell(i++);
        cell.setCellValue(RTLPRC);

        cell = row.createCell(i++);
        cell.setCellValue(UUID);

        cell = row.createCell(i++);
        cell.setCellValue(ESTATUS);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);

        aplicarAutoSize(sheet, i);
        return sheet;
    }

    @Override
    protected void agregarEncabezados() {

    }

    private void agregaDatos(CatalogoInputVO detalle, SXSSFSheet sheet, int i) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(detalle.getCodeBarras());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getSku());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getMunit());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getQpc());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getQpcStr());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getRtlPrc());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getUuid());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getEstatus());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getDescripcion());
    }
}
