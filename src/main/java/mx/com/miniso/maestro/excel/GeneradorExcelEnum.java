
package mx.com.miniso.maestro.excel;


public enum GeneradorExcelEnum {
    REPORTE_VERIFICACION("VERIFICACIONES", new String[]{
        "UNIDAD RESPONSABLE",
        "TOTAL DE BIENES",
        "TOTAL DE BIENES CON CEDULAS ERRÓNEAS",
        "TOTAL DE ETIQUETAS",
        "TOTAL DE BIENES CON CEDULAS CREADAS",
        "VERIFICADOS",
        "INCIDENCIA SICOBIM",
        "TOTAL VERIFICADOS",
        "%",
        "NO VERIFICADOS"}, new String[]{"rUnitName", "goodTotal",
        "cedulasErroneas", "solEtiquetas",
        "verifTotal", "verificaciones", "incidenciaSicobim",
        "totalVerificados", "percentage", "faltantes", "incidenciaSiamobil"
    }
    ),
    REPORTE_PROVEEDOR("CONSULTA DE PROVEEDORES", new String[]{
        "ID",
        "NOMBRE",
        "RFC"}, new String[]{"id", "name",
        "rfc"
    }
    ),
    REPORTE_CABM("CONSULTA DE CABM", new String[]{
        "CLAVE",
        "PARTIDA PRESUPUESTAL CONTABLE",
        "DESCRIPCION CABM"}, new String[]{"claveCabm", "partidaPresConta",
        "desCabmDet"
    }
    ),
    REPORTE_UR("CONSULTA DE UNIDAD RESPONSABLE", new String[]{
        "UNIDAD RESPONSABLE",
        "CLAVE DE LA UNIDAD RESPONSABLE"},
            new String[]{"nombreUr",
                "claveUr"
            }
    ),
    REPORTE_CT("CONSULTA DE CENTRO DE TRABAJO", new String[]{
        "CLAVE UR",
        "UNIDAD RESPONSABLE",
        "CLAVE CT",
        "CENTRO DE TRABAJO",
        "FINANZAS"}, new String[]{"claveUr", "nombreUr",
        "claveCt", "nombreCt", "finanzas"
    }
    ),
    REPORTE_PF("CONSULTA DE PUESTO PERIFERICO", new String[]{
        "UR",
        "CT",
        "AUX",
        "DENOMINACION",
        "FINANZAS",
        "FONDO"}, new String[]{"claveUr", "claveCt", "clavePf", "nombrePf", "finanzas",
        "pfFondo"
    }
    ),
    REPORTE_VERIFICACIONNO("NO_VERIFICADOS", new String[]{
        "NO. DE INVENTARIO",
        "UR",
        "CT",
        "PF",
        "DESCRIPCION DETALLE",
        "TIPO BIEN",
        "UBICACION"
    }, new String[]{"inv", "ur", "ct", "pf", "tipoBien",
        "descr", "local"
    }
    ),
    REPORTE_VERIFICACIONVERI("VERIFICADOS", new String[]{
        "NO. DE INVENTARIO",
        "UR",
        "CT",
        "PF",
        "TIPO BIEN",
        "DESCRIPCION CABM",
        "DESCRIPCION DETALLE",
        "RFC", "UBICACION"
    }, new String[]{"stockNumber", "runitName", "wcenterName", "pplaceName", "tipoBien", "cabmDescription",
        "description", "kindOfIssue", "local"
    }
    ),
    REPORTE_VERIFICACIONETI("SOLICITUD REIMPRESION ETIQUETAS", new String[]{
        "NO. DE INVENTARIO",
        "UR",
        "CT",
        "PF",
        "DESCRIPCION DETALLE",
        "UBICACION"
    }, new String[]{"stockNumber", "cl_ur", "cl_ct", "cl_pf", "description", "local"
    }
    ),
    REPORTE_VERIFICACIONCREA("CEDULAS DE RESGUARDO CREADAS", new String[]{
        "NO. DE INVENTARIO",
        "UR",
        "CT",
        "PF", "RFC",
        "DESCRIPCION DETALLE",
        "UBICACION"
    }, new String[]{"stockNumber", "runitName", "wcenterName", "pplaceName", "kindOfIssue", "description", "local"
    }
    ),
    REPORTE_VERIFICACIONERROR("CEDULAS ERRONEAS", new String[]{
        "NO. DE INVENTARIO",
        "UR",
        "CT",
        "PF", "RFC",
        "DESCRIPCION DETALLE", "ESTATUS",
        "UBICACION"
    }, new String[]{"stockNumber", "cl_ur", "cl_ct", "cl_pf", "rfc", "description", "good.statusTexto", "location"
    }
    ),
    REPORTE_VERIFICACIONSIAMOVIL("INCIDENCIAS SIAMOVIL", new String[]{
        "NO. DE INVENTARIO",
        "UR",
        "CT",
        "PF", "CÉDULA DE RESGUARDO",
        "DESCRIPCION",
        "UBICACION"
    }, new String[]{"stockNumber", "cl_ur", "cl_ct", "cl_pf", "rfc", "description", "local"
    }
    ),
    REPORTE_VERIFICACIONSICOBIM("INCIDENCIAS SICOBIM", new String[]{
        "NO. DE INVENTARIO",
        "UR",
        "CT",
        "PF", "CÉDULA DE RESGUARDO",
        "DESCRIPCION",
        "UBICACION"
    }, new String[]{"stockNumber", "cl_ur", "cl_ct", "cl_pf", "rfc", "description", "local"
    }
    ),
    REPORTE_CABM_CLAVE("CABM POR CLAVE", new String[]{
        "CABM",
        "PARTIDA PRESUPUESTAL",
        "PARTIDA PRESUPUESTAL CONTABLE",
        "DESCRIPCIÓN CABM"}, new String[]{"cabmCode", "entry", "entryAccountant", "cabmDesc"}
    ),
    REPORTE_BAJAS_MASIVAS("REPORTE DE BAJAS MASIVAS", new String[]{
        "URESP", "UCENT", "UPERIF", "NUM_CUENTA", "NUM_INV",
        "CONSEC", "CVE_GEN", "DESC_GEN", "MARCA", "NUM_SERIE",
        "VALOR", "FECH_ADQ", "DESC_BIEN", "PARTIDA", "UBIC",
        "P_BAJA", "ID_EDO_SOL", "ID_USUARIO", "FE_MOVIMIENTO", "DS_CAMBIO",
        "SUBMOTIVO", "NUM_OFICIO", "FECH_OFICIO"},
            new String[]{"uresp", "ucent", "uperif", "num_cuenta", "num_inv",
                "consec", "cve_gen", "desc_gen", "marca", "num_serie",
                "valor", "fech_adq_st", "desc_bien", "partida", "ubic",
                "p_baja_st", "id_edo_sol", "cv_usuario", "fe_movimiento_st", "ds_cambio",
                "ds_submotivo", "num_oficio", "fech_oficio_st"}
    ),
    REPORTE_CONTROL_FINANZAS("REPORTE CIFRAS DE CONTROL FINANZAS", new String[]{
        "UNIDAD RESPONSABLE", "BIENES ACTIVOS", "IMPORTE BIENES ACTIVOS"},
            new String[]{"runitName", "bienReasignacion", "importeReasignacion"}
    ),
    REPORTE_CANCELACIONES_MASIVAS(
            "REPORTE DE CANCELACIONES MASIVAS", new String[]{
        "URESP", "UCENT", "UPERIF", "NUM_CUENTA", "NUM_INV",
        "CONSEC", "CVE_GEN", "DESC_GEN", "MARCA", "NUM_SERIE",
        "VALOR", "FECH_ADQ", "DESC_BIEN", "PARTIDA", "UBIC",
        "P_CANCELADOS", "ID_EDO_SOL", "ID_USUARIO", "FE_MOVIMIENTO", "DS_CAMBIO",
        "NUM_OFICIO", "FECH_OFICIO"},
            new String[]{"uresp", "ucent", "uperif", "num_cuenta", "num_inv",
                "consec", "cve_gen", "desc_gen", "marca", "num_serie",
                "valor", "fech_adq_st", "desc_bien", "partida", "ubic",
                "p_cancelados_st", "id_edo_sol", "cv_usuario", "fe_movimiento_st", "ds_cambio",
                "num_oficio", "fech_oficio_st"}
    );

    private final String nombreArchivo;
    private final String[] encabezados;
    private final String[] atributos;

    GeneradorExcelEnum(String nombreArchivo, String[] encabezados, String[] atributos) {
        this.nombreArchivo = nombreArchivo;
        this.encabezados = encabezados;
        this.atributos = atributos;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public String[] getEncabezados() {
        return encabezados;
    }

    public String[] getAtributos() {
        return atributos;
    }

}
