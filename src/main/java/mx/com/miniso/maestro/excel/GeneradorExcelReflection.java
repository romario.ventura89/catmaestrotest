package mx.com.miniso.maestro.excel;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorExcelReflection<T> extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcelReflection.class);

    private final List<T> lista;
    private final GeneradorExcelEnum generadorExcelEnum;

    public GeneradorExcelReflection(GeneradorExcelEnum generadorExcelEnum, List<T> lista) {
        super(generadorExcelEnum.getNombreArchivo());
        this.lista = lista;
        this.generadorExcelEnum = generadorExcelEnum;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {
        SXSSFSheet sheetS = reporte.createSheet("REPORTE");
        int i = 1;
        if (lista != null) {
            for (T dato : lista) {
                SXSSFRow row = sheetS.createRow(i++);
                int j = 0;
                try {
                    for (String atributo : generadorExcelEnum.getAtributos()) {
                        Object valor = PropertyUtils.getProperty(dato, atributo);
                        agregaValorCelda(row, valor, j++);
                    }
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
                    LOGGER.error("Falló al obtener los valores de la clase en el atributo: "
                            + generadorExcelEnum.getAtributos()[j] + ", REPORTE: "
                            + generadorExcelEnum.getNombreArchivo(), ex);
                }
            }
        }
    }

    @Override
    protected void agregarEncabezados() {
        String[] encabezados = generadorExcelEnum.getEncabezados();
        for (String encabezado : encabezados) {
            aplicarValorEncabezados(encabezado);
        }

    }
}
