package mx.com.miniso.maestro.excel;

import java.util.List;
import mx.com.miniso.maestro.vo.CatalogoVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class GeneradorExcelGoods extends GeneradorExcel {

    private static final Log LOGGER = LogFactory.getLog(GeneradorExcelGoods.class);

    private final List<CatalogoVO> detalles;
    private final String SKU = "code";
    private final String BRAND = "product name";
    private final String MANUFACTOR = "manufactor";
    private final String MUNIT = "munit";
    private final String NAME = "name";
    private final String ORIGIN = "origin";
    private final String QPC = "qpc";
    private final String RTLPRC = "rtlprc";
    private final String SORT = "sort";
    private final String SPEC = "spec";
    private final String TM = "tm";
    private final String UUID = "uuid";
    private final String ESTATUS = "estatus";
    private final String DESCRIPCION = "descripcion";

    public GeneradorExcelGoods(List<CatalogoVO> detalles) {
        super("Goods ");
        this.detalles = detalles;
    }

    @Override
    protected void crearReporte(SXSSFWorkbook reporte) {

        SXSSFSheet generalSheet = null;
        int indexGeneral = 1;
        boolean paginaGeneralCreada = false;

        for (CatalogoVO detalle : detalles) {
            if (!paginaGeneralCreada) {
                generalSheet = reporte.createSheet("GOODS");
                generalSheet = agregaTitulo(generalSheet);
                paginaGeneralCreada = true;
            }
            agregaDatos(detalle, generalSheet, indexGeneral++);
        }
        autoSize(generalSheet);
    }

    private void autoSize(SXSSFSheet sheet) {

    }

    private SXSSFSheet agregaTitulo(SXSSFSheet sheet) {

        Row row = sheet.createRow(0);
        int i = 0;
        Cell cell = row.createCell(i++);
        cell.setCellValue(BRAND);

        cell = row.createCell(i++);
        cell.setCellValue(SKU);

        cell = row.createCell(i++);
        cell.setCellValue(MANUFACTOR);

        cell = row.createCell(i++);
        cell.setCellValue(MUNIT);

        cell = row.createCell(i++);
        cell.setCellValue(NAME);
        
        cell = row.createCell(i++);
        cell.setCellValue(ORIGIN);

        cell = row.createCell(i++);
        cell.setCellValue(QPC);

        cell = row.createCell(i++);
        cell.setCellValue(RTLPRC);

        cell = row.createCell(i++);
        cell.setCellValue(SORT);
        
        cell = row.createCell(i++);
        cell.setCellValue(SPEC);

        cell = row.createCell(i++);
        cell.setCellValue(TM);

        cell = row.createCell(i++);
        cell.setCellValue(UUID);

        cell = row.createCell(i++);
        cell.setCellValue(ESTATUS);

        cell = row.createCell(i++);
        cell.setCellValue(DESCRIPCION);
        
        aplicarAutoSize(sheet, i);
        return sheet;
    }

    @Override
    protected void agregarEncabezados() {

    }

    private void agregaDatos(CatalogoVO detalle, SXSSFSheet sheet, int i) {
        int j = 0;
        Row row = sheet.createRow(i);
        Cell cell = row.createCell(j++);
        cell.setCellValue(detalle.getBrand());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getCode());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getManufactor());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getMunit());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getName());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getOrigin());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getQpc());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getRtlPrc());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getSort());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getSpec());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getTm());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getUuid());
        
        cell = row.createCell(j++);
        cell.setCellValue(detalle.getEstatus());

        cell = row.createCell(j++);
        cell.setCellValue(detalle.getDescripcion());
    }
}
