package mx.com.miniso.maestro.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import mx.com.miniso.maestro.ex.ValidacionFormularioException;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Validador<T> {

    final static Logger logger = LoggerFactory.getLogger(Validador.class);

    private final int CODIGO_OBLIGATORIO = 1;
    private final int CODIGO_TAMANIO = 2;
    private final int CODIGO_ES_NUMERO = 3;
    private final int CODIGO_ES_NUMERO_ENTERO = 4;
    private final int CODIGO_RANGO_NUMERO_ENTERO = 5;
    private final int CODIGO_TAMANIO_MAXIMO = 6;
    private final int CODIGO_ESPACIOS_BLANCO = 7;
    private final int CODIGO_CARACTERES_ESPECIALES = 8;
    private final int CODIGO_SOLO_LETRAS = 9;
    private final int CODIGO_CURP = 10;
    private final int CODIGO_RFC = 11;
    private final int CODIGO_EMAIL = 12;
    private final int CODIGO_IGUAL = 13;
    private final int CODIGO_TAMANIO_MINIMO = 14;
    private final int CODIGO_FECHA = 15;

    public abstract boolean realizarValidacion(T t) throws ValidacionFormularioException;

    protected void validaTamanio(int tam, String dato) throws ValidacionFormularioException {
        String msm = "El campo debe contener " + tam + " caracteres.";
        if (tam != dato.length()) {
            throw new ValidacionFormularioException(CODIGO_TAMANIO, msm, dato);
        }

    }

    protected void validaPassword(String contrasena, String repetirContrasena) throws ValidacionFormularioException {
        String msm = "El texto de la contraseña no corresponde al texto de repetir contraseña. ";
        if (contrasena == null || !contrasena.equals(repetirContrasena)) {
            throw new ValidacionFormularioException(CODIGO_IGUAL, msm, contrasena);
        }
    }

    protected void validaCaptcha(String dato, String capcha) throws ValidacionFormularioException {
        String msm = "El texto escrito en el campo de código de verificación no corresponde a la imagen";
        if (dato == null || !dato.equals(capcha)) {
            throw new ValidacionFormularioException(CODIGO_IGUAL, msm, dato);
        }
    }

    protected void validaEsNumero(String dato) throws ValidacionFormularioException {
        String msm = "El campo debe ser num\u00e9rico.";
        if (!NumberUtils.isNumber(dato)) {
            throw new ValidacionFormularioException(CODIGO_ES_NUMERO, msm, dato);
        }

    }

    protected void validaEsNumeroEntero(String dato) throws ValidacionFormularioException {
        String msm = "El campo debe ser num\u00e9rico.";
        if (!NumberUtils.isNumber(dato)) {
            throw new ValidacionFormularioException(CODIGO_ES_NUMERO_ENTERO, msm, dato);
        }
        msm = "El campo debe ser entero.";
        try {
            Integer.parseInt(dato);
        } catch (NumberFormatException ex) {
            throw new ValidacionFormularioException(CODIGO_ES_NUMERO_ENTERO, msm, dato);
        }

    }

    protected void validaRangoNumericoEntero(String strValor, int valMin, int valMax)
            throws ValidacionFormularioException {
        int valor = Integer.parseInt(strValor);
        if (valor < valMin || valor > valMax) {
            throw new ValidacionFormularioException(CODIGO_RANGO_NUMERO_ENTERO,
                    "El valor ingresado esta fuera del rango siguiente: de " + valMin + " a " + valMax, strValor);
        }

    }

    protected void validaObligatorio(String dato) throws ValidacionFormularioException {
        String msm = "El campo es obligatorio.";
        if (dato == null || "".equals(dato)) {
            throw new ValidacionFormularioException(CODIGO_OBLIGATORIO, msm, dato);
        }
    }

    protected void validaTamanioMaximo(int tam, String dato) throws ValidacionFormularioException {
        String msm = "El campo supera el tama\u00F1o m\u00E1ximo de " + tam + " caracteres.";
        if (dato.length() > tam) {
            throw new ValidacionFormularioException(CODIGO_TAMANIO_MAXIMO, msm, dato);
        }
    }

    protected void validaTamanioMinimo(int tam, String dato) throws ValidacionFormularioException {
        String msm = "El campo es inferior al tama\u00F1o mínimo de " + tam + " caracteres.";
        if (dato.length() < tam) {
            throw new ValidacionFormularioException(CODIGO_TAMANIO_MINIMO, msm, dato);
        }
    }

    protected void validaEspaciosEnBlanco(String dato) throws ValidacionFormularioException {
        String msm = "El campo contiene espacios en blanco.";
        if (StringUtils.isEmpty(dato.trim())) {
            throw new ValidacionFormularioException(CODIGO_ESPACIOS_BLANCO, msm, dato);
        }
    }

    protected void validaNoContengaNingunEspacioEnBlanco(String dato) throws ValidacionFormularioException {
        String msm = "El campo contiene espacios en blanco.";
        if (dato.contains(" ")) {
            throw new ValidacionFormularioException(CODIGO_ESPACIOS_BLANCO, msm, dato);
        }
    }

    protected void validaCaracteresEspeciales(String dato) throws ValidacionFormularioException {
        String msm = "El campo contiene caracteres especiales.";
        if (!StringUtils.isAlphanumericSpace(dato)) {
            throw new ValidacionFormularioException(CODIGO_CARACTERES_ESPECIALES, msm, dato);
        }
    }

    protected void validaCaracteresEspecialesSinGuionBajo(String dato) throws ValidacionFormularioException {
        String msm = "El campo solo puede contener letras, n\00fameros y guiones bajos.";

        String regex = "^\\w+[\\_\\w]*$";
        Pattern patron = Pattern.compile(regex);
        if (!patron.matcher(dato).matches()) {
            throw new ValidacionFormularioException(CODIGO_CARACTERES_ESPECIALES, msm, dato);
        }
    }

    protected void validaFecha(String dato) throws ValidacionFormularioException {
        String msm = "El campo debe ser en el formato de fecha DD/MM/YYYY";

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = null;
        try {
            fecha = sdf.parse(dato);
        } catch (ParseException ex) {
            throw new ValidacionFormularioException(CODIGO_FECHA, msm, dato);
        }
    }

    protected void validaSoloLertras(String dato) throws ValidacionFormularioException {
        String msm = "El campo contiene caracteres no alfab\u00e9ticos.";
        if (!StringUtils.isAlphaSpace(dato)) {
            throw new ValidacionFormularioException(CODIGO_SOLO_LETRAS, msm, dato);
        }
    }

    protected void validaRangoTexto(String dato, int valMin, int valMax) throws ValidacionFormularioException {
        if (dato.length() < valMin || dato.length() > valMax) {
            throw new ValidacionFormularioException(CODIGO_TAMANIO, "El texto ingresado esta fuera del rango siguiente: de " + valMin + " a " + valMax + " caracteres", dato);
        }
    }

    protected void validaCurpRegex(String curp) throws ValidacionFormularioException {
        String msm = "El CURP " + curp + " no es v\u00E1lido.";
        String regex = "[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}" + "(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])"
                + "[HM]{1}"
                + "(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)"
                + "[B-DF-HJ-NP-TV-Z]{3}" + "[0-9A-Z]{1}[0-9]{1}$";
        Pattern patron = Pattern.compile(regex);
        if (!patron.matcher(curp).matches()) {
            throw new ValidacionFormularioException(CODIGO_CURP, msm, curp);
        }
    }

    protected void validaRFCRegex(String rfc) throws ValidacionFormularioException {
        String msm = "El RFC " + rfc + " no es v\u00E1lido";
        String regex = "^([A-Z\\u00D1\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\\d]{3}))?$";
        Pattern patron = Pattern.compile(regex);
        if (!patron.matcher(rfc).matches()) {
            throw new ValidacionFormularioException(CODIGO_RFC, msm, rfc);
        }
    }

    protected void validaEmailRegex(String email) throws ValidacionFormularioException {
        String msm = "El correo " + email + " no es v\u00E1lido";

        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException e) {
            logger.error("Email no  v\\u00E1lido", e);
            throw new ValidacionFormularioException(CODIGO_EMAIL, msm, email);
        }

    }

}
