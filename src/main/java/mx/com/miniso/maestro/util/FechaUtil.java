package mx.com.miniso.maestro.util;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FechaUtil {

    private static final String PATRON_FECHA = "dd/MM/yyyy";
    private static final String PATRON_FECHA_ARCHIVO = "yyyy_MM_dd";
    private static final String PATRON_HORA = "H:mm";
    private static final String PATRON_TIMESTAMP = "dd/MM/yyyy H:mm";

    public static String formateaFecha(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat(PATRON_FECHA);
        return sdf.format(fecha);
    }

    public static String formateaFecha(long time) {
        return formateaFecha(new Date(time));
    }

    public static java.sql.Date convierteFechaStringTODate(String fechaString) {
        SimpleDateFormat sdf = new SimpleDateFormat(PATRON_FECHA);
        try {
            Date fecha = sdf.parse(fechaString);
            return new java.sql.Date(fecha.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(FechaUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static java.sql.Timestamp convierteFechaStringTOTimestamp(String fechaString) {
        SimpleDateFormat sdf = new SimpleDateFormat(PATRON_FECHA);
        try {
            Date fecha = sdf.parse(fechaString);
            return new java.sql.Timestamp(fecha.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(FechaUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static java.sql.Time convierteFechaStringTOTime(String fechaString) {
        SimpleDateFormat sdf = new SimpleDateFormat(PATRON_HORA);
        try {
            Date fecha = sdf.parse(fechaString);
            return new java.sql.Time(fecha.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(FechaUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static String formateHora(Time tempHorario) {
        SimpleDateFormat sdf = new SimpleDateFormat(PATRON_HORA);
        return sdf.format(tempHorario);
    }

    public static String formateaTimeStamp(Timestamp fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat(PATRON_TIMESTAMP);
        return sdf.format(fecha);
    }

    public static Timestamp creaFechaActualTimestamp() {
        Timestamp time = new Timestamp(System.currentTimeMillis());
        return time;
    }

    public static String formateaFechaArchivo(java.sql.Date fecha) {
         SimpleDateFormat sdf = new SimpleDateFormat(PATRON_FECHA_ARCHIVO);
        return sdf.format(fecha);
    }

    public static void main(String[] args) {
          SimpleDateFormat sdf = new SimpleDateFormat(PATRON_FECHA);
         Date fecha = null;
        try {
            fecha = sdf.parse("qwwe");
        } catch (ParseException ex) {
            Logger.getLogger(FechaUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
         System.out.println(fecha);
    }
}
