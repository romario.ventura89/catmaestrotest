package mx.com.miniso.maestro.controller;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import mx.com.miniso.maestro.constantes.MensajesVista;
import mx.com.miniso.maestro.excel.GeneradorExcel;
import mx.com.miniso.maestro.excel.GeneradorExcelGoods;
import mx.com.miniso.maestro.service.CargaJsonService;
import mx.com.miniso.maestro.service.CatalogoService;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CargaGoodsVO;
import mx.com.miniso.maestro.vo.CatalogoVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoVO;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/catalogo")
public class CatalogoGoodsController {

    final static Log logger = LogFactory.getLog(CatalogoGoodsController.class);

    @Autowired
    private CatalogoService catalogoService;

    @Autowired
    private CargaJsonService cargaJsonService;
        
    @RequestMapping(value = "/goods", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> confirmacion(MultipartHttpServletRequest request) {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {
                CargaGoodsVO cargaGoodsVO = new CargaGoodsVO();
                cargaGoodsVO.setFechaCarga(request.getParameter("fechaCarga"));
                cargaGoodsVO.setNumeroJson(request.getParameter("numeroJson"));
                Map<String, MultipartFile> fileMap = request.getFileMap();
                for (MultipartFile archivo : fileMap.values()) {
                    cargaGoodsVO = cargaJsonService.obtenerDatos(cargaGoodsVO, archivo);
                }
                cargaJsonService.guarda(cargaGoodsVO);
                return new ResponseEntity<>(cargaGoodsVO, HttpStatus.OK);
            }
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo Goods", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo de Goods", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
    }
    
    @RequestMapping(value = "/validaJson", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> validaJson(@RequestBody FiltrosCatalogoVO filtrosCatalogoVO) {
        List<CatalogoVO> catalogoVO;
        try {
            catalogoVO = catalogoService.buscarCatalogo(filtrosCatalogoVO);
            return new ResponseEntity<>(catalogoVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }
    
    @RequestMapping(value = "/buscar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> buscar(@RequestBody FiltrosCatalogoVO filtrosCatalogoVO) {
        List<CatalogoVO> catalogoVO;
        try {
            catalogoVO = catalogoService.buscarCatalogo(filtrosCatalogoVO);
            return new ResponseEntity<>(catalogoVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }
    
    @RequestMapping(value = "/detalle", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> detalle(@RequestBody CatalogoVO catalogoVO) {
        try {
            List<CatalogoVO> detalles = catalogoService.obtenerDetalle(catalogoVO);
            return new ResponseEntity<>(detalles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }
    
    @RequestMapping(value = "/actualizar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> actualizar(@RequestBody CatalogoVO catalogoVO) {
        try {
            catalogoVO = catalogoService.actualizar(catalogoVO,catalogoVO.getIdGoods());
            return new ResponseEntity<>(catalogoVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/eliminar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> eliminar(@RequestBody CatalogoVO catalogoVO) {
        try {
            catalogoService.eliminar(catalogoVO);
            return new ResponseEntity<>(catalogoVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(@RequestParam(name = "uuid") String uuid,@RequestParam(name = "sku") String sku, 
            @RequestParam(name = "fecha") String fecha, @RequestParam(name = "fechaInicio") String fechaInicio, 
            @RequestParam(name = "fechaFin") String fechaFin, @RequestParam(name = "status") String status, HttpServletResponse response) throws Exception {
        FiltrosCatalogoVO filtrosCatalogoVO = new FiltrosCatalogoVO();
        filtrosCatalogoVO.setUuid(uuid);
        filtrosCatalogoVO.setSku(sku);
        filtrosCatalogoVO.setFecha(fecha);
        filtrosCatalogoVO.setFechaInicio(fechaInicio);
        filtrosCatalogoVO.setFechaFin(fechaFin);
        filtrosCatalogoVO.setEstatus(status);
        List<CatalogoVO> catalogoVO = null;
        try {
            catalogoVO = catalogoService.buscarExcel(filtrosCatalogoVO);
            if(catalogoVO!=null){
                GeneradorExcel generador = new GeneradorExcelGoods(catalogoVO);
                response.addHeader("Content-disposition", "attachment;filename="
                        + FechaUtil.creaFechaActualTimestamp()
                        + "_GOODS.xlsx");
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                OutputStream output = response.getOutputStream();
                output.write(generador.obtenerArchivo());
                output.close();
            }
        } catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }
    }
}
