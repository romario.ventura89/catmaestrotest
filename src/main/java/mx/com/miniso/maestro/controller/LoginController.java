package mx.com.miniso.maestro.controller;

import javax.servlet.http.HttpServletRequest;
import mx.com.miniso.maestro.constantes.Constantes;
import mx.com.miniso.maestro.constantes.MensajesVista;
import mx.com.miniso.maestro.constantes.PaginaEnum;
import mx.com.miniso.maestro.dto.Usuario;
import mx.com.miniso.maestro.ex.ValidacionFormularioException;
import mx.com.miniso.maestro.service.LoginService;
import mx.com.miniso.maestro.vo.LoginVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/login")
public class LoginController {

    final static Log logger = LogFactory.getLog(LoginController.class);

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/entraLogin", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> entraLogin(@RequestBody LoginVO login, HttpServletRequest request) {
        request.getSession().removeAttribute(Constantes.SESION_USUARIO);
        request.getSession().removeAttribute(Constantes.SESION_OBJETO);
        try {
            if (login == null) {
                logger.error("El valor del usuario es nulo.");
                return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
            }

            Usuario usuario = new Usuario();
            usuario.setUserName(login.getUsuario());
            usuario.setPassword(login.getPassword());
            usuario = loginService.obtenerUsuarioXuserName(usuario);

            if (usuario == null) {
                return new ResponseEntity<>("Tu usuario no es válido", HttpStatus.CONFLICT);

            }
            if (!usuario.isEstatus()) {
                return new ResponseEntity<>("El usuario solicitado no se encuentra activo.", HttpStatus.CONFLICT);
            }
            if (usuario.getPassword().equals(login.getPassword())) {
                usuario.setPassword("********");
                request.getSession().setAttribute("USUARIO", usuario);
            } else {
                return new ResponseEntity<>("Tu contraseña no es válida", HttpStatus.CONFLICT);
            }

        } catch (Exception ex) {
            logger.error("Error al obtener la información de Login", ex);
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(login, HttpStatus.OK);
    }

    @RequestMapping(value = "/redirectRol", method = RequestMethod.GET)
    public String redirectRol(HttpServletRequest request) {

        int usuarioChina = 1;
        int usuarioMexico = 2;

        Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
        if (usuario == null) {
            return "redirect:/index.html";
        }

        if (usuario.getRol().getIdRol() == usuarioChina) {

            return "redirect:/sitio/privado/usr/bienvenida.jsp";

        }

        if (usuario.getRol().getIdRol() == usuarioMexico) {
            return "redirect:/sitio/privado/usr/bienvenida.jsp";
        }

        return "";
    }

    @RequestMapping(value = "/menuRol", method = RequestMethod.GET)
    public String menuRol(HttpServletRequest request) {

        int usuarioChina = 1;
        int usuarioMexico = 2;

        Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");

        if (usuario.getRol().getIdRol() == usuarioChina) {
            return "redirect:/sitio/privado/menus/menuChina.jsp";
        }

        if (usuario.getRol().getIdRol() == usuarioMexico) {
            return "redirect:/sitio/privado/menus/menuMexico.jsp";
        }

        return "redirect:/index.html";
    }

    //remover el objeto de sesión, opción salir
    @RequestMapping(value = "/salir", method = RequestMethod.GET)
    public String salir(HttpServletRequest request) {
        request.getSession().removeAttribute(Constantes.SESION_USUARIO);
        request.getSession().removeAttribute(Constantes.SESION_OBJETO);
        return PaginaEnum.LOGIN.getPath();
    }
}
