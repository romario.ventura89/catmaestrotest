package mx.com.miniso.maestro.controller;

import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import static mx.com.miniso.maestro.controller.CatalogoGoodsController.logger;
import mx.com.miniso.maestro.excel.GeneradorExcel;
import mx.com.miniso.maestro.excel.GeneradorExcelError;
import mx.com.miniso.maestro.service.CargaExcelService;
import mx.com.miniso.maestro.service.CatalogoInputService;
import mx.com.miniso.maestro.vo.CatalogoVO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import mx.com.miniso.maestro.service.CatalogoService;
import mx.com.miniso.maestro.vo.CatalogoInputVO;
import mx.com.miniso.maestro.vo.CatalogoMaestroVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping("/reporteErrores")

public class ReporteErroresController {

    @Autowired
    private CatalogoService catalogoService;

    @Autowired
    private CatalogoInputService catalogoInputService;
    
    @Autowired
    private CargaExcelService cargaExcelService;

    @RequestMapping(value = "/buscarErrores", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void buscarErrores(HttpServletResponse response) {
        List<CatalogoVO> catalogoVO;
        List<CatalogoInputVO> catalogoInputVO;        
        List<CatalogoMaestroVO> catalogoMaestroVO;  
        try {
            catalogoVO = catalogoService.buscarErrores();
            catalogoInputVO = catalogoInputService.buscarErrores();
            catalogoMaestroVO = cargaExcelService.buscarErrores();
            GeneradorExcel generador = new GeneradorExcelError(catalogoVO,catalogoInputVO,catalogoMaestroVO);
            response.addHeader("Content-disposition", "attachment;filename="
                    + "Reporte_Errores.xlsx");
            OutputStream output = response.getOutputStream();
            output.write(generador.obtenerArchivo());
            output.close();
        } catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }

    }
}
