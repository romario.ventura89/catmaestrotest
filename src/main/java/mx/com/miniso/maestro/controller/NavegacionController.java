package mx.com.miniso.maestro.controller;

import javax.servlet.http.HttpServletRequest;
import mx.com.miniso.maestro.constantes.Constantes;
import mx.com.miniso.maestro.constantes.MenuEnum;
import mx.com.miniso.maestro.constantes.PaginaEnum;
import mx.com.miniso.maestro.dto.Navegacion;
import mx.com.miniso.maestro.dto.Usuario;
import mx.com.miniso.maestro.service.NavegacionService;
import mx.com.miniso.maestro.vo.NavegacionVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/navegacion")
public class NavegacionController {

    final static Log logger = LogFactory.getLog(LoginController.class);

    @Autowired
    private NavegacionService navegacionService;

    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public String menu(HttpServletRequest request) {

        Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.SESION_USUARIO);
        MenuEnum menu = (MenuEnum) request.getSession().getAttribute(Constantes.SESION_MENU);
        if (usuario == null) {
            return MenuEnum.DEFAULT.getPath();
        } else if (menu != null) {
            return menu.getPath();
        }

        return MenuEnum.GENERAL.getPath();
    }

    @RequestMapping(value = "/seleccionaMenu", method = RequestMethod.GET)
    public String seleccionaMenu(@RequestParam(name = "idMenu") String idMenu, HttpServletRequest request) {

        MenuEnum miMenu = null;
        for (MenuEnum menu : MenuEnum.values()) {
            if (menu.getId() == Integer.parseInt(idMenu)) {
                request.getSession().setAttribute(Constantes.SESION_MENU, menu);
                miMenu = menu;
                break;
            }
        }
        if (miMenu != null) {
            return miMenu.getPath();
        }

        return "";
    }

    @RequestMapping(value = "/pagina", method = RequestMethod.GET)
    public String pagina(@RequestParam(name = "id") int id, HttpServletRequest request) {
        Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.SESION_USUARIO);
        if (usuario != null) {
            for (PaginaEnum pagina : PaginaEnum.values()) {
                if (pagina.getId() == id) {
                    try {
                        navegacionService.guarda(usuario, pagina.getPath(), null);
                        return pagina.getPath();
                    } catch (Exception ex) {
                        logger.error("Error al guardar la sesion del usuario en base de datos", ex);
                        return PaginaEnum.LOGIN.getPath();
                    }
                }
            }
        }

        return PaginaEnum.LOGIN.getPath();
    }

    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public String redirect(HttpServletRequest request) {

        Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.SESION_USUARIO);
        if (usuario != null) {
            try {
                Navegacion navegacion = navegacionService.buscar(usuario);
                if (navegacion != null) {
                    request.getSession().setAttribute(Constantes.SESION_OBJETO, navegacion.getObjeto());
                    return navegacion.getPath();
                }
            } catch (Exception ex) {
                logger.error("Error al guardar la sesion del usuario en base de datos", ex);
                return PaginaEnum.LOGIN.getPath();
            }
        }

        return PaginaEnum.LOGIN.getPath();
    }

    @RequestMapping(value = "/sesion", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> sesion(HttpServletRequest request) {
        NavegacionVO navegacionVO = new NavegacionVO();
        Usuario usuario = (Usuario) request.getSession().getAttribute(Constantes.SESION_USUARIO);
        if (usuario == null) {
            navegacionVO.setLogin(true);
            
            return new ResponseEntity<>(navegacionVO, HttpStatus.OK);
        }
        try {

            Navegacion navegacion = navegacionService.buscar(usuario);
            if (navegacion != null) {

                request.getSession().setAttribute(Constantes.SESION_OBJETO, navegacion.getObjeto());
                navegacionVO.setPath(navegacion.getPath());
                return new ResponseEntity<>(navegacionVO, HttpStatus.OK);
            }
            navegacionVO.setLogin(true);
        } catch (Exception ex) {
            logger.error("Error al procesar la navegación en base de datos ", ex);
        }
        return new ResponseEntity<>(navegacionVO, HttpStatus.OK);
    }
}
