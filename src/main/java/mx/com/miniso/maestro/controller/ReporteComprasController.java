package mx.com.miniso.maestro.controller;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import static mx.com.miniso.maestro.controller.CatalogoGoodsController.logger;
import mx.com.miniso.maestro.excel.GeneradorExcel;
import mx.com.miniso.maestro.excel.GeneradorExcelCatMaestro;
import mx.com.miniso.maestro.service.CargaExcelService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import mx.com.miniso.maestro.service.compras.ReporteComprasService;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CatalogoExcelVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping("/reporteCompras")

public class ReporteComprasController {

    @Autowired
    private ReporteComprasService reporteComprasService;

    @Autowired
    private CargaExcelService cargaExcelService;
    
    @RequestMapping(value = "/buscarSkuCompras", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void buscarCompras(HttpServletResponse response) throws Exception {
        List<String> reporteConfirmacion;
        List<CatalogoExcelVO> catalogoVO;
        List<CatalogoExcelVO> catalogoExcel = new ArrayList<>(0);
        CatalogoExcelVO catalogoExcelVO = new CatalogoExcelVO();
        try {
            reporteConfirmacion = reporteComprasService.ComprasCatalogo();
            catalogoVO = cargaExcelService.obtenerDetalle(catalogoExcelVO);
            for(CatalogoExcelVO catalogo: catalogoVO){
                if(reporteConfirmacion.contains(catalogo.getCodProveedor())){
                    catalogoExcel.add(catalogo);
                };
            }
           if(catalogoExcel != null && catalogoExcel.size()>0){
                GeneradorExcel generador = new GeneradorExcelCatMaestro(catalogoExcel);
                response.addHeader("Content-disposition", "attachment;filename="
                        + FechaUtil.creaFechaActualTimestamp()
                        + "_GOODS_Compras.xlsx");
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                OutputStream output = response.getOutputStream();
                output.write(generador.obtenerArchivo());
                output.close();
            }
        }catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }
    }
}
