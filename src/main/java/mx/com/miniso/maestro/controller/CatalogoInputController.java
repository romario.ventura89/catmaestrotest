package mx.com.miniso.maestro.controller;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import mx.com.miniso.maestro.constantes.MensajesVista;
import static mx.com.miniso.maestro.controller.CargarJsonController.logger;
import mx.com.miniso.maestro.excel.GeneradorExcel;
import mx.com.miniso.maestro.excel.GeneradorExcelInput;
import mx.com.miniso.maestro.service.CargaInputService;
import mx.com.miniso.maestro.service.CatalogoInputService;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CargaInputVO;
import mx.com.miniso.maestro.vo.CatalogoInputVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoInputVO;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/catalogoInput")
public class CatalogoInputController {

    final static Log logger = LogFactory.getLog(CatalogoInputController.class);
    
    @Autowired
    private CatalogoInputService catalogoInputService;
    
    @Autowired 
    private CargaInputService cargaInputService;

    @RequestMapping(value = "/input", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> input(MultipartHttpServletRequest request) {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {
                CargaInputVO cargaInputVO = new CargaInputVO();
                cargaInputVO.setFechaCarga(request.getParameter("fechaCarga"));
                cargaInputVO.setNumeroJson(request.getParameter("numeroInput"));

                Map<String, MultipartFile> fileMap = request.getFileMap();
                
                for (MultipartFile archivo : fileMap.values()) {
                    cargaInputVO = cargaInputService.obtenerDatos(cargaInputVO, archivo);
                }
                cargaInputService.guarda(cargaInputVO);
                return new ResponseEntity<>(cargaInputVO, HttpStatus.OK);
            }
       
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo GdInput", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo GdInput", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);

    }
    
    @RequestMapping(value = "/buscar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> buscar(@RequestBody FiltrosCatalogoInputVO filtrosCatalogoInputVO) {
        List<CatalogoInputVO> catalogoInputVO;
        try {
            catalogoInputVO = catalogoInputService.buscarCatalogo(filtrosCatalogoInputVO);
            return new ResponseEntity<>(catalogoInputVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/detalle", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> detalle(@RequestBody CatalogoInputVO catalogoInputVO) {
        try {
            List<CatalogoInputVO> detalles = catalogoInputService.obtenerDetalle(catalogoInputVO);
            return new ResponseEntity<>(detalles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/actualizar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> actualizar(@RequestBody CatalogoInputVO catalogoInputVO) {
        try {
            catalogoInputVO = catalogoInputService.actualizar(catalogoInputVO,catalogoInputVO.getIdGdInput());
            return new ResponseEntity<>(catalogoInputVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }
    
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> eliminar(@RequestBody CatalogoInputVO catalogoInputVO) {
        try {
            catalogoInputService.eliminar(catalogoInputVO);
            return new ResponseEntity<>(catalogoInputVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(@RequestParam(name = "uuid") String uuid,@RequestParam(name = "cbarras") String cbarras, @RequestParam(name = "sku") String sku, 
            @RequestParam(name = "fecha") String fecha, @RequestParam(name = "fechaInicio") String fechaInicio, 
            @RequestParam(name = "fechaFin") String fechaFin, @RequestParam(name = "status") String status, HttpServletResponse response) throws Exception { 
        List<CatalogoInputVO> catalogoInputVO = null;
        FiltrosCatalogoInputVO filtrosCatalogoInputVO = new FiltrosCatalogoInputVO();        
        filtrosCatalogoInputVO.setUuid(uuid);
        filtrosCatalogoInputVO.setSku(sku);
        filtrosCatalogoInputVO.setFecha(fecha);
        filtrosCatalogoInputVO.setFechaInicio(fechaInicio);
        filtrosCatalogoInputVO.setFechaFin(fechaFin);
        filtrosCatalogoInputVO.setEstatus(status);
        filtrosCatalogoInputVO.setcBarras(cbarras);
        try {
            catalogoInputVO = catalogoInputService.buscarExcel(filtrosCatalogoInputVO);
            if(catalogoInputVO!=null){
                GeneradorExcel generador = new GeneradorExcelInput(catalogoInputVO);
                response.addHeader("Content-disposition", "attachment;filename="
                        + FechaUtil.creaFechaActualTimestamp()
                        + "_INPUT.xlsx");
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                OutputStream output = response.getOutputStream();
                output.write(generador.obtenerArchivo());
                output.close();    
            }
        } catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }
    }
}
