package mx.com.miniso.maestro.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import mx.com.miniso.maestro.constantes.MensajesVista;
import mx.com.miniso.maestro.excel.GeneradorExcel;
import mx.com.miniso.maestro.excel.GeneradorExcelCatMaestro;
import mx.com.miniso.maestro.service.CargaNomService;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CargaNomVO;
import mx.com.miniso.maestro.vo.CatalogoNomVO;
import mx.com.miniso.maestro.vo.CatalogoXMLVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoNomVO;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/catalogoNom")
public class CatalogoNomController {

    final static Log logger = LogFactory.getLog(CatalogoNomController.class);
    
    @Autowired
    private CargaNomService cargaNomService;

    @RequestMapping(value = "/nom", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> nom(MultipartHttpServletRequest request) throws IOException, Exception {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {
                CargaNomVO cargaNomVO = new CargaNomVO();
                cargaNomVO.setFechaCarga(request.getParameter("fechaCarga"));
                cargaNomVO.setNumeroExcel(request.getParameter("numeroExcel"));
                Map<String, MultipartFile> fileMap = request.getFileMap();
                for (MultipartFile archivo : fileMap.values()) {
                    cargaNomVO = cargaNomService.obtenerDatos(cargaNomVO, archivo);
                }
                    cargaNomVO = cargaNomService.guarda(cargaNomVO);
                return new ResponseEntity<>(cargaNomVO, HttpStatus.OK);
            }
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo de pedido", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo de factura", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
    }
    
    @RequestMapping(value = "/buscar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> buscar(@RequestBody FiltrosCatalogoNomVO filtrosCatalogoNomVO) {
        List<CatalogoNomVO> catalogoNomVO;
        try {
            catalogoNomVO = cargaNomService.buscarCatalogo(filtrosCatalogoNomVO);
            return new ResponseEntity<>(catalogoNomVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }
    
    @RequestMapping(value = "/detalle", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> detalle(@RequestBody CatalogoNomVO catalogoNomVO) {
        try {
            List<CatalogoNomVO> detalles = cargaNomService.obtenerDetalle(catalogoNomVO);
            return new ResponseEntity<>(detalles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/actualizar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> actualizar(@RequestBody CatalogoNomVO catalogoNomVO) {
        try {
            catalogoNomVO = cargaNomService.actualizar(catalogoNomVO,catalogoNomVO.getIdCatMaestro());
            return new ResponseEntity<>(catalogoNomVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(@RequestParam(name = "uuid") String uuid,@RequestParam(name = "cbarras") String cbarras, @RequestParam(name = "sku") String sku, 
            @RequestParam(name = "fecha") String fecha, @RequestParam(name = "fechaInicio") String fechaInicio, 
            @RequestParam(name = "fechaFin") String fechaFin, @RequestParam(name = "status") String status, HttpServletResponse response) throws Exception { 
        List<CatalogoNomVO> catalogoNomVO;
        FiltrosCatalogoNomVO filtrosCatalogoNomVO = new FiltrosCatalogoNomVO();        
        filtrosCatalogoNomVO.setUuid(uuid);
        filtrosCatalogoNomVO.setSku(sku);
        filtrosCatalogoNomVO.setFecha(fecha);
        filtrosCatalogoNomVO.setFechaInicio(fechaInicio);
        filtrosCatalogoNomVO.setFechaFin(fechaFin);
        filtrosCatalogoNomVO.setEstatus(status);
        filtrosCatalogoNomVO.setcBarras(cbarras);
        try {
            catalogoNomVO = cargaNomService.buscarExcel(filtrosCatalogoNomVO);
//            GeneradorExcel generador = new GeneradorExcelCatMaestro(catalogoNomVO);
//            response.addHeader("Content-disposition", "attachment;filename="
//                    + FechaUtil.creaFechaActualTimestamp()
//                    + "_CATMAESTRO.xlsx");
//            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//            OutputStream output = response.getOutputStream();
//            output.write(generador.obtenerArchivo());
//            output.close();
        } catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }
    }
    
    @RequestMapping(value = "/xml", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public void xml(@RequestParam(name = "uuid") String uuid,@RequestParam(name = "cbarras") String cbarras, @RequestParam(name = "sku") String sku, 
            @RequestParam(name = "fecha") String fecha, @RequestParam(name = "fechaInicio") String fechaInicio, 
            @RequestParam(name = "fechaFin") String fechaFin, @RequestParam(name = "status") String status, HttpServletResponse response) throws Exception { 
        List<CatalogoNomVO> catalogoNomVO;
        FiltrosCatalogoNomVO filtrosCatalogoNomVO = new FiltrosCatalogoNomVO();        
        filtrosCatalogoNomVO.setUuid(uuid);
        filtrosCatalogoNomVO.setSku(sku);
        filtrosCatalogoNomVO.setFecha(fecha);
        filtrosCatalogoNomVO.setFechaInicio(fechaInicio);
        filtrosCatalogoNomVO.setFechaFin(fechaFin);
        filtrosCatalogoNomVO.setEstatus(status);
        filtrosCatalogoNomVO.setcBarras(cbarras);
        CatalogoXMLVO catalogoXMLVO = new CatalogoXMLVO();
        try {
//            catalogoExcelVO = cargaNomService.buscarExcel(filtrosCatalogoInputVO);
//            catalogoXMLVO.setDetalles(catalogoExcelVO);
            JAXBContext context = JAXBContext.newInstance(CatalogoXMLVO.class);

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.marshal(catalogoXMLVO, baos);

            response.addHeader("Content-disposition", "attachment;filename="
                    + FechaUtil.creaFechaActualTimestamp()
                    + "_CATMAESTRO.xml");
            response.setContentType("application/xml");

            OutputStream output = response.getOutputStream();
            output.write(baos.toByteArray());
            output.close();
        } catch (Exception ex) {
            logger.error("Falló la carga del xml", ex);
        }
    }
}
