package mx.com.miniso.maestro.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import mx.com.miniso.maestro.constantes.MensajesVista;
import mx.com.miniso.maestro.excel.GeneradorExcel;
import mx.com.miniso.maestro.excel.GeneradorExcelCatMaestro;
import mx.com.miniso.maestro.service.CargaExcelService;
import mx.com.miniso.maestro.service.CatalogoInputService;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CargaExcelVO;
import mx.com.miniso.maestro.vo.CatalogoExcelVO;
import mx.com.miniso.maestro.vo.CatalogoInputVO;
import mx.com.miniso.maestro.vo.CatalogoXMLVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoInputVO;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/catalogoExcel")
public class CatalogoExcelController {

    final static Log logger = LogFactory.getLog(CatalogoExcelController.class);
    
    @Autowired
    private CargaExcelService cargaExcelService;
    
    @Autowired
    private CatalogoInputService catalogoInputService;

    @RequestMapping(value = "/excel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> excel(MultipartHttpServletRequest request) throws IOException, Exception {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {
                CargaExcelVO cargaVO = new CargaExcelVO();
                cargaVO.setFechaCarga(request.getParameter("fechaCarga"));
                cargaVO.setNumeroExcel(request.getParameter("numeroExcel"));
                Map<String, MultipartFile> fileMap = request.getFileMap();
                for (MultipartFile archivo : fileMap.values()) {
                    cargaVO = cargaExcelService.obtenerDatos(cargaVO, archivo);
                }
                    cargaVO = cargaExcelService.guarda(cargaVO);
                return new ResponseEntity<>(cargaVO, HttpStatus.OK);
            }
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo de pedido", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo de factura", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
    }
    
    @RequestMapping(value = "/buscar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> buscar(@RequestBody FiltrosCatalogoInputVO filtrosCatalogoInputVO) {
        List<CatalogoExcelVO> catalogoExcelVO;
        try {
            catalogoExcelVO = cargaExcelService.buscarCatalogo(filtrosCatalogoInputVO);
            return new ResponseEntity<>(catalogoExcelVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }
    
    @RequestMapping(value = "/detalle", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> detalle(@RequestBody CatalogoExcelVO catalogoExcelVO) {
        try {
            List<CatalogoExcelVO> detalles = cargaExcelService.obtenerDetalle(catalogoExcelVO);
            return new ResponseEntity<>(detalles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/actualizar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> actualizar(@RequestBody CatalogoExcelVO catalogoExcelVO) {
        try {
            catalogoExcelVO = cargaExcelService.actualizar(catalogoExcelVO,catalogoExcelVO.getIdCatMaestro());
            return new ResponseEntity<>(catalogoExcelVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
    }
    
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> eliminar(@RequestBody CatalogoInputVO catalogoInputVO) {
        try {
            catalogoInputService.eliminar(catalogoInputVO);
            return new ResponseEntity<>(catalogoInputVO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/excel", method = RequestMethod.GET, produces = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @ResponseBody
    public void excel(@RequestParam(name = "uuid") String uuid,@RequestParam(name = "cbarras") String cbarras, @RequestParam(name = "sku") String sku, 
            @RequestParam(name = "fecha") String fecha, @RequestParam(name = "fechaInicio") String fechaInicio, 
            @RequestParam(name = "fechaFin") String fechaFin, @RequestParam(name = "status") String status, HttpServletResponse response) throws Exception { 
        List<CatalogoExcelVO> catalogoExcelVO;
        FiltrosCatalogoInputVO filtrosCatalogoInputVO = new FiltrosCatalogoInputVO();        
        filtrosCatalogoInputVO.setUuid(uuid);
        filtrosCatalogoInputVO.setSku(sku);
        filtrosCatalogoInputVO.setFecha(fecha);
        filtrosCatalogoInputVO.setFechaInicio(fechaInicio);
        filtrosCatalogoInputVO.setFechaFin(fechaFin);
        filtrosCatalogoInputVO.setEstatus(status);
        filtrosCatalogoInputVO.setcBarras(cbarras);
        try {
            catalogoExcelVO = cargaExcelService.buscarExcel(filtrosCatalogoInputVO);
            GeneradorExcel generador = new GeneradorExcelCatMaestro(catalogoExcelVO);
            response.addHeader("Content-disposition", "attachment;filename="
                    + FechaUtil.creaFechaActualTimestamp()
                    + "_CATMAESTRO.xlsx");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            OutputStream output = response.getOutputStream();
            output.write(generador.obtenerArchivo());
            output.close();
        } catch (Exception ex) {
            logger.error("Error al procesar el archivo", ex);
        }
    }
    
    @RequestMapping(value = "/xml", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public void xml(@RequestParam(name = "uuid") String uuid,@RequestParam(name = "cbarras") String cbarras, @RequestParam(name = "sku") String sku, 
            @RequestParam(name = "fecha") String fecha, @RequestParam(name = "fechaInicio") String fechaInicio, 
            @RequestParam(name = "fechaFin") String fechaFin, @RequestParam(name = "status") String status, HttpServletResponse response) throws Exception { 
        List<CatalogoExcelVO> catalogoExcelVO;
        FiltrosCatalogoInputVO filtrosCatalogoInputVO = new FiltrosCatalogoInputVO();        
        filtrosCatalogoInputVO.setUuid(uuid);
        filtrosCatalogoInputVO.setSku(sku);
        filtrosCatalogoInputVO.setFecha(fecha);
        filtrosCatalogoInputVO.setFechaInicio(fechaInicio);
        filtrosCatalogoInputVO.setFechaFin(fechaFin);
        filtrosCatalogoInputVO.setEstatus(status);
        filtrosCatalogoInputVO.setcBarras(cbarras);
        CatalogoXMLVO catalogoXMLVO = new CatalogoXMLVO();
        try {
            catalogoExcelVO = cargaExcelService.buscarExcel(filtrosCatalogoInputVO);
            catalogoXMLVO.setDetalles(catalogoExcelVO);
            JAXBContext context = JAXBContext.newInstance(CatalogoXMLVO.class);

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.marshal(catalogoXMLVO, baos);

            response.addHeader("Content-disposition", "attachment;filename="
                    + FechaUtil.creaFechaActualTimestamp()
                    + "_CATMAESTRO.xml");
            response.setContentType("application/xml");

            OutputStream output = response.getOutputStream();
            output.write(baos.toByteArray());
            output.close();
        } catch (Exception ex) {
            logger.error("Fallo la carga del xml", ex);
        }
    }

}
