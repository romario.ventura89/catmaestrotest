package mx.com.miniso.maestro.controller;

import java.io.IOException;
import java.util.Map;
import mx.com.miniso.maestro.constantes.MensajesVista;
import mx.com.miniso.maestro.service.CargaExcelService;
import mx.com.miniso.maestro.service.CargaInputService;
import mx.com.miniso.maestro.service.CargaJsonService;
import mx.com.miniso.maestro.vo.CargaExcelVO;
import mx.com.miniso.maestro.vo.CargaGoodsVO;
import mx.com.miniso.maestro.vo.CargaInputVO;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/cargaJson")
public class CargarJsonController {

    final static Logger logger = LoggerFactory.getLogger(CargarJsonController.class);

    @Autowired
    private CargaJsonService cargaJsonService;
    
    @Autowired 
    private CargaInputService cargaInputService;
    
    @Autowired 
    private CargaExcelService cargaExcelService;

    @RequestMapping(value = "/goods", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> confirmacion(MultipartHttpServletRequest request) {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {
                CargaGoodsVO cargaGoodsVO = new CargaGoodsVO();
                cargaGoodsVO.setFechaCarga(request.getParameter("fechaCarga"));
                cargaGoodsVO.setNumeroJson(request.getParameter("numeroJson"));
                Map<String, MultipartFile> fileMap = request.getFileMap();
                for (MultipartFile archivo : fileMap.values()) {
                    cargaGoodsVO = cargaJsonService.obtenerDatos(cargaGoodsVO, archivo);
                }
                cargaJsonService.guarda(cargaGoodsVO);
                return new ResponseEntity<>(cargaGoodsVO, HttpStatus.OK);
            }
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo Goods", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo de Goods", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);

    }
    
    @RequestMapping(value = "/input", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> input(MultipartHttpServletRequest request) {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {
                CargaInputVO cargaInputVO = new CargaInputVO();
                cargaInputVO.setFechaCarga(request.getParameter("fechaCarga"));
                cargaInputVO.setNumeroJson(request.getParameter("numeroInput"));

                Map<String, MultipartFile> fileMap = request.getFileMap();
                
                for (MultipartFile archivo : fileMap.values()) {
                    cargaInputVO = cargaInputService.obtenerDatos(cargaInputVO, archivo);
                }
                cargaInputService.guarda(cargaInputVO);
                return new ResponseEntity<>(cargaInputVO, HttpStatus.OK);
            }
       
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo GdInput", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo GdInput", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);

    }
    

    @RequestMapping(value = "/excel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> excel(MultipartHttpServletRequest request) throws IOException, Exception {
        try {
            if (ServletFileUpload.isMultipartContent(request)) {
                CargaExcelVO cargaVO = new CargaExcelVO();
                cargaVO.setFechaCarga(request.getParameter("fechaCarga"));
                cargaVO.setNumeroExcel(request.getParameter("numeroExcel"));
                Map<String, MultipartFile> fileMap = request.getFileMap();
                for (MultipartFile archivo : fileMap.values()) {
                    cargaVO = cargaExcelService.obtenerDatos(cargaVO, archivo);
                }
                    cargaVO = cargaExcelService.guarda(cargaVO);
                return new ResponseEntity<>(cargaVO, HttpStatus.OK);
            }
        } catch (ExceptionInInitializerError e) {
            logger.error("Error generado por la carga del archivo de pedido", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.PARTIAL_CONTENT);
        } catch (Exception e) {
            logger.error("Error al cargar el archivo de factura", e);
            return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<String>(MensajesVista.ERROR_EXCEPCION, HttpStatus.CONFLICT);
    }
}
