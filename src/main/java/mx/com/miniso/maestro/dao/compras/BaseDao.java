package mx.com.miniso.maestro.dao.compras;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory; 

public abstract class BaseDao<T> {

    private final static Log LOG = LogFactory.getLog(BaseDao.class);

    protected Session abrirSesion(SessionFactory sesionFactory) {
        Session session = sesionFactory.openSession();
        LOG.info("Sesion abierta");
        return session;
    }

    protected void cerrarSesion(Session session) {
        if (session != null) {
            session.close();
            LOG.info("Sesion cerrada");
        } else {
            LOG.info("Sesion nula");
        }
    }

    protected T guardar(SessionFactory sesionFactory, T dto) throws HibernateException {
        Transaction tx = null;
        Session session = null;
        try {
            session = sesionFactory.openSession();
            tx = session.beginTransaction();
            session.save(dto);
            tx.commit();
            return dto;
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.error("Error al guardar ", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }
    
     protected T update(SessionFactory sesionFactory, T dto) throws HibernateException {
        Transaction tx = null;
        Session session = null;
        try {
            session = sesionFactory.openSession();
            tx = session.beginTransaction();
            session.update(dto);
            tx.commit();
            return dto;
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.error("Error al guardar ", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }
     
    protected T delete(SessionFactory sesionFactory, T dto) throws HibernateException {
        Transaction tx = null;
        Session session = null;
        try {
            session = sesionFactory.openSession();
            tx = session.beginTransaction();
            session.delete(dto);
            tx.commit();
            return dto;
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOG.error("Error al eliminar... ", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }

    } 

}
