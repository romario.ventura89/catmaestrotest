
package mx.com.miniso.maestro.dao;

import mx.com.miniso.maestro.dto.Usuario;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class UsuarioDao extends BaseDao<Usuario>  {
    
    private final static Log LOG = LogFactory.getLog(UsuarioDao.class);

    @Autowired
    private SessionFactory sessionFactory;
    
    public Usuario obtenerUsuarioXId(Usuario usuario) throws Exception {

        Session session = null;
        if (usuario == null) {
            LOG.error("No se puede buscar el usuario por que es nulo");
            return usuario;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from usuario u where u.idUsuario = :idUsuario");
            query.setParameter("idUsuario", usuario.getIdUsuario());
            usuario = (Usuario) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return usuario;

    }
    
    public Usuario obtenerUsuarioXuserName(Usuario usuario) throws Exception {

        Session session = null;
        if (usuario == null) {
            LOG.error("No se puede buscar el usuario por que es nulo");
            return usuario;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from usuario u where u.userName = :userName");
            query.setParameter("userName", usuario.getUserName());
            usuario = (Usuario) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return usuario;

    }
    
    
}
