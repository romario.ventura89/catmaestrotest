package mx.com.miniso.maestro.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mx.com.miniso.maestro.dto.CatMaestro;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CatalogoExcelVO;
import mx.com.miniso.maestro.vo.CatalogoMaestroVO;
import mx.com.miniso.maestro.vo.CatalogoNomVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoInputVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoNomVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public class CatalogoNomDao extends BaseDao<CatMaestro> {

    private final static Log LOG = LogFactory.getLog(CatalogoNomDao.class);

    @Autowired
    private SessionFactory sessionFactory;
    
    public CatMaestro guardar(CatMaestro catMaestro) {
        LOG.info("Guardar Excel CatalogoExcelDao");
        guardar(sessionFactory, catMaestro);
        return catMaestro;
    }

    public void borrar(CatMaestro catMaestro) {
        delete(sessionFactory, catMaestro);
    }

    public void actualizar(CatMaestro catMaestro) {
        update(sessionFactory, catMaestro);
    }
    
    public List<CatalogoExcelVO> obtenerCombinaciones(String sku, String cBarras){
        Session session = null;
        List<CatalogoExcelVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            
            sb.append(" SELECT idCatMaestro, codProveedor, codBarra, clave_de_linea_de_producto, familia, subFamilia, subSubFamilia, descripcion, category, ");
            sb.append(" middleCategory, subcategory, smallCategory, product, fraccAr, inner_, validacion, fvMxp, fvRmb, ctRmb, ");
            sb.append(" textil, calzado, fechaRegistro, fechaActualizacion, archivoExcel, estatus ");
            sb.append(" from catMaestro ");
            sb.append(" where 1=1 ");
            
            if(!StringUtils.isEmpty(sku)){
                sb.append(" and codProveedor ='");
                sb.append(sku);
                sb.append("'");
            }
            
            if (!StringUtils.isEmpty(cBarras)) {
                sb.append(" and codBarra = '");
                sb.append(cBarras);
                sb.append("' ");
            }
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoExcelVO catalogoExcelVO = new CatalogoExcelVO();
                catalogoExcelVO.setIdCatMaestro(String.valueOf(mapa.get("idCatMaestro")));
                catalogoExcelVO.setCodProveedor(String.valueOf(mapa.get("codProveedor")));
                catalogoExcelVO.setCodBarra(String.valueOf(mapa.get("codBarra")));
                catalogoExcelVO.setClave_de_linea_de_producto(String.valueOf(mapa.get("clave_de_linea_de_producto")));
                catalogoExcelVO.setFamilia(String.valueOf(mapa.get("familia")));
                catalogoExcelVO.setSubFamilia(String.valueOf(mapa.get("subFamilia")));
                catalogoExcelVO.setSubSubFamilia(String.valueOf(mapa.get("subSubFamilia")));
                catalogoExcelVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoExcelVO.setCategory(String.valueOf(mapa.get("category")));
                catalogoExcelVO.setMiddleCategory(String.valueOf(mapa.get("middleCategory")));
                catalogoExcelVO.setSubcategory(String.valueOf(mapa.get("subcategory")));
                catalogoExcelVO.setSmallCategory(String.valueOf(mapa.get("smallCategory")));
                catalogoExcelVO.setProduct(String.valueOf(mapa.get("product")));
                catalogoExcelVO.setFraccAr(String.valueOf(mapa.get("fraccAr")));
                catalogoExcelVO.setInner_(String.valueOf(mapa.get("inner_")));
                catalogoExcelVO.setValidacion(String.valueOf(mapa.get("validacion")));
                catalogoExcelVO.setFvMxp(String.valueOf(mapa.get("fvMxp")));
                catalogoExcelVO.setFvRmb(String.valueOf(mapa.get("fvRmb")));
                catalogoExcelVO.setCtRmb(String.valueOf(mapa.get("ctRmb")));
                catalogoExcelVO.setTextil(String.valueOf(mapa.get("textil")));
                catalogoExcelVO.setCalzado(String.valueOf(mapa.get("calzado")));
                catalogoExcelVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoExcelVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoExcelVO.setArchivoExcel(String.valueOf(mapa.get("archivoExcel")));
                catalogoExcelVO.setEstatus(String.valueOf(mapa.get("estatus")));
                
                if(catalogoExcelVO.getEstatus().equals("1")){
                    catalogoExcelVO.setEstatus("Nuevo");
                }
                if(catalogoExcelVO.getEstatus().equals("2")){
                    catalogoExcelVO.setEstatus("Actualizado");
                }
                if(catalogoExcelVO.getEstatus().equals("3")){
                    catalogoExcelVO.setEstatus("Erroneo");
                }
                catalogoList.add(catalogoExcelVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }
    
    public CatMaestro obtenerCombinacion(String sku, String cBarras) {
        Session session = null;
        CatMaestro catMaestro = new CatMaestro();
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from catMaestro c where c.codProveedor = :codProveedor and c.codBarra = :codBarra");
            query.setParameter("codProveedor", sku);
            query.setParameter("codBarra", cBarras);
            catMaestro = (CatMaestro) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catMaestro;
    }
    
    public List<CatalogoNomVO> obtenerXParametros(FiltrosCatalogoNomVO filtrosCatalogoNomVO){
        Session session = null;
        String campoFecha="";
        List<CatalogoNomVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            
            sb.append(" SELECT idCatMaestro, codProveedor, codBarra, clave_de_linea_de_producto, familia, subFamilia, subSubFamilia, descripcion, category, ");
            sb.append(" middleCategory, subcategory, smallCategory, product, fraccAr, inner_, validacion, fvMxp, fvRmb, ctRmb, ");
            sb.append(" textil, calzado, fechaRegistro, fechaActualizacion, archivoExcel, estatus ");
            sb.append(" from catMaestro ");
            sb.append(" where 1=1 ");
            if(filtrosCatalogoNomVO.getEstatus()!= null){
                if(!filtrosCatalogoNomVO.getEstatus().equals("0")){
                    if(filtrosCatalogoNomVO.getEstatus().equals("1")){
                        sb.append(" and estatus = 1 ");
                        campoFecha="fechaRegistro";
                    }
                    if(filtrosCatalogoNomVO.getEstatus().equals("2")){
                        sb.append(" and estatus =2 ");
                        campoFecha ="fechaActualizacion";
                    }
                    if(filtrosCatalogoNomVO.getEstatus().equals("3")){
                        sb.append(" and estatus = 3 ");
                        campoFecha ="fechaRegistro";
                    }
                }else{
                    campoFecha="fechaRegistro";
                }
            }

            if (!StringUtils.isEmpty(filtrosCatalogoNomVO.getFecha())) {
                sb.append(" and fechaRegistro = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoNomVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoNomVO.getSku())) {
                sb.append(" and codProveedor like '%");
                sb.append(filtrosCatalogoNomVO.getSku());
                sb.append("%'");
            }
            
            if (!StringUtils.isEmpty(filtrosCatalogoNomVO.getcBarras())) {
                sb.append(" and codBarra like '%");
                sb.append(filtrosCatalogoNomVO.getcBarras());
                sb.append("%'");
            }

            if(filtrosCatalogoNomVO.getFechaInicio()!=null && filtrosCatalogoNomVO.getFechaFin()!=null){
                if(!filtrosCatalogoNomVO.getFechaInicio().isEmpty() && !filtrosCatalogoNomVO.getFechaFin().isEmpty()){
                    sb.append(" and ");
                    sb.append(campoFecha);
                    sb.append(" between '");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoNomVO.getFechaInicio()));
                    sb.append("' ");
                    sb.append(" and ");
                    sb.append("' ");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoNomVO.getFechaFin()));
                    sb.append("' ");
                }
            }
            
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoNomVO catalogoNomVO = new CatalogoNomVO();
                catalogoNomVO.setIdCatMaestro(String.valueOf(mapa.get("idCatMaestro")));
                catalogoNomVO.setCodProveedor(String.valueOf(mapa.get("codProveedor")));
                catalogoNomVO.setCodBarra(String.valueOf(mapa.get("codBarra")));
                catalogoNomVO.setClave_de_linea_de_producto(String.valueOf(mapa.get("clave_de_linea_de_producto")));
                catalogoNomVO.setFamilia(String.valueOf(mapa.get("familia")));
                catalogoNomVO.setSubFamilia(String.valueOf(mapa.get("subFamilia")));
                catalogoNomVO.setSubSubFamilia(String.valueOf(mapa.get("subSubFamilia")));
                catalogoNomVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoNomVO.setCategory(String.valueOf(mapa.get("category")));
                catalogoNomVO.setMiddleCategory(String.valueOf(mapa.get("middleCategory")));
                catalogoNomVO.setSubcategory(String.valueOf(mapa.get("subcategory")));
                catalogoNomVO.setSmallCategory(String.valueOf(mapa.get("smallCategory")));
                catalogoNomVO.setProduct(String.valueOf(mapa.get("product")));
                catalogoNomVO.setFraccAr(String.valueOf(mapa.get("fraccAr")));
                catalogoNomVO.setInner_(String.valueOf(mapa.get("inner_")));
                catalogoNomVO.setValidacion(String.valueOf(mapa.get("validacion")));
                catalogoNomVO.setFvMxp(String.valueOf(mapa.get("fvMxp")));
                catalogoNomVO.setFvRmb(String.valueOf(mapa.get("fvRmb")));
                catalogoNomVO.setCtRmb(String.valueOf(mapa.get("ctRmb")));
                catalogoNomVO.setTextil(String.valueOf(mapa.get("textil")));
                catalogoNomVO.setCalzado(String.valueOf(mapa.get("calzado")));
                catalogoNomVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoNomVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoNomVO.setArchivoExcel(String.valueOf(mapa.get("archivoExcel")));
                catalogoNomVO.setEstatus(String.valueOf(mapa.get("estatus")));
                
                if(catalogoNomVO.getEstatus().equals("1")){
                    catalogoNomVO.setEstatus("Nuevo");
                }
                if(catalogoNomVO.getEstatus().equals("2")){
                    catalogoNomVO.setEstatus("Actualizado");
                }
                if(catalogoNomVO.getEstatus().equals("3")){
                    catalogoNomVO.setEstatus("Erroneo");
                }
                catalogoList.add(catalogoNomVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }
    
    public List<CatalogoNomVO> obtenerExcel(FiltrosCatalogoNomVO filtrosCatalogoNomVO){
        Session session = null;
        String campoFecha="";
        List<CatalogoNomVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            
            sb.append(" SELECT idCatMaestro, codProveedor, codBarra, clave_de_linea_de_producto, familia, subFamilia, subSubFamilia, descripcion, category, ");
            sb.append(" middleCategory, subcategory, smallCategory, product, fraccAr, inner_, validacion, fvMxp, fvRmb, ctRmb, ");
            sb.append(" textil, calzado, fechaRegistro, fechaActualizacion, archivoExcel, estatus ");
            sb.append(" from catMaestro ");
            sb.append(" where 1=1 ");
            if(filtrosCatalogoNomVO.getEstatus()!= null){
                if(!filtrosCatalogoNomVO.getEstatus().equals("0")){
                    if(filtrosCatalogoNomVO.getEstatus().equals("1")){
                        sb.append(" and estatus = 1 ");
                        campoFecha="fechaRegistro";
                    }
                    if(filtrosCatalogoNomVO.getEstatus().equals("2")){
                        sb.append(" and estatus =2 ");
                        campoFecha ="fechaActualizacion";
                    }
                    if(filtrosCatalogoNomVO.getEstatus().equals("3")){
                        sb.append(" and estatus = 3 ");
                        campoFecha ="fechaRegistro";
                    }
                }else{
                    campoFecha="fechaRegistro";
                }
            }

            if (!StringUtils.isEmpty(filtrosCatalogoNomVO.getFecha())) {
                sb.append(" and fechaRegistro = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoNomVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoNomVO.getSku())) {
                sb.append(" and codProveedor like '%");
                sb.append(filtrosCatalogoNomVO.getSku());
                sb.append("%'");
            }
            
            if (!StringUtils.isEmpty(filtrosCatalogoNomVO.getcBarras())) {
                sb.append(" and codBarra like '%");
                sb.append(filtrosCatalogoNomVO.getcBarras());
                sb.append("%'");
            }

            if(filtrosCatalogoNomVO.getFechaInicio()!=null && filtrosCatalogoNomVO.getFechaFin()!=null){
                if(!filtrosCatalogoNomVO.getFechaInicio().isEmpty() && !filtrosCatalogoNomVO.getFechaFin().isEmpty()){
                    sb.append(" and ");
                    sb.append(campoFecha);
                    sb.append(" between '");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoNomVO.getFechaInicio()));
                    sb.append("' ");
                    sb.append(" and ");
                    sb.append("' ");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoNomVO.getFechaFin()));
                    sb.append("' ");
                }
            }
            
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoNomVO catalogoNomVO = new CatalogoNomVO();
                catalogoNomVO.setIdCatMaestro(String.valueOf(mapa.get("idCatMaestro")));
                catalogoNomVO.setCodProveedor(String.valueOf(mapa.get("codProveedor")));
                catalogoNomVO.setCodBarra(String.valueOf(mapa.get("codBarra")));
                catalogoNomVO.setClave_de_linea_de_producto(String.valueOf(mapa.get("clave_de_linea_de_producto")));
                catalogoNomVO.setFamilia(String.valueOf(mapa.get("familia")));
                catalogoNomVO.setSubFamilia(String.valueOf(mapa.get("subFamilia")));
                catalogoNomVO.setSubSubFamilia(String.valueOf(mapa.get("subSubFamilia")));
                catalogoNomVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoNomVO.setCategory(String.valueOf(mapa.get("category")));
                catalogoNomVO.setMiddleCategory(String.valueOf(mapa.get("middleCategory")));
                catalogoNomVO.setSubcategory(String.valueOf(mapa.get("subcategory")));
                catalogoNomVO.setSmallCategory(String.valueOf(mapa.get("smallCategory")));
                catalogoNomVO.setProduct(String.valueOf(mapa.get("product")));
                catalogoNomVO.setFraccAr(String.valueOf(mapa.get("fraccAr")));
                catalogoNomVO.setInner_(String.valueOf(mapa.get("inner_")));
                catalogoNomVO.setValidacion(String.valueOf(mapa.get("validacion")));
                catalogoNomVO.setFvMxp(String.valueOf(mapa.get("fvMxp")));
                catalogoNomVO.setFvRmb(String.valueOf(mapa.get("fvRmb")));
                catalogoNomVO.setCtRmb(String.valueOf(mapa.get("ctRmb")));
                catalogoNomVO.setTextil(String.valueOf(mapa.get("textil")));
                catalogoNomVO.setCalzado(String.valueOf(mapa.get("calzado")));
                catalogoNomVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoNomVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoNomVO.setArchivoExcel(String.valueOf(mapa.get("archivoExcel")));
                catalogoNomVO.setEstatus(String.valueOf(mapa.get("estatus")));
                
                if(catalogoNomVO.getEstatus().equals("1")){
                    catalogoNomVO.setEstatus("Nuevo");
                }
                if(catalogoNomVO.getEstatus().equals("2")){
                    catalogoNomVO.setEstatus("Actualizado");
                }
                if(catalogoNomVO.getEstatus().equals("3")){
                    catalogoNomVO.setEstatus("Erroneo");
                }
                catalogoList.add(catalogoNomVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }
    
    public List<CatalogoNomVO> obtenerDetalle(CatalogoNomVO catalogoNomVO){
        Session session = null;
        List<CatalogoNomVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" SELECT idCatMaestro, codProveedor, codBarra, clave_de_linea_de_producto, familia, subFamilia, subSubFamilia, descripcion, category, ");
            sb.append(" middleCategory, subcategory, smallCategory, product, fraccAr, inner_, validacion, fvMxp, fvRmb, ctRmb, ");
            sb.append(" textil, calzado, fechaRegistro, fechaActualizacion, archivoExcel, estatus ");
            sb.append(" from catMaestro ");
            sb.append(" where 1=1 ");
            
            if (!StringUtils.isEmpty(catalogoNomVO.getCodBarra())) {
                sb.append(" and codBarra like '%");
                sb.append(catalogoNomVO.getCodBarra());
                sb.append("%'");
            }
            
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoNomVO catalogoVO = new CatalogoNomVO();
                catalogoVO.setIdCatMaestro(String.valueOf(mapa.get("idCatMaestro")));
                catalogoVO.setCodProveedor(String.valueOf(mapa.get("codProveedor")));
                catalogoVO.setCodBarra(String.valueOf(mapa.get("codBarra")));
                catalogoVO.setClave_de_linea_de_producto(String.valueOf(mapa.get("clave_de_linea_de_producto")));
                catalogoVO.setFamilia(String.valueOf(mapa.get("familia")));
                catalogoVO.setSubFamilia(String.valueOf(mapa.get("subFamilia")));
                catalogoVO.setSubSubFamilia(String.valueOf(mapa.get("subSubFamilia")));
                catalogoVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoVO.setCategory(String.valueOf(mapa.get("category")));
                catalogoVO.setMiddleCategory(String.valueOf(mapa.get("middleCategory")));
                catalogoVO.setSubcategory(String.valueOf(mapa.get("subcategory")));
                catalogoVO.setSmallCategory(String.valueOf(mapa.get("smallCategory")));
                catalogoVO.setProduct(String.valueOf(mapa.get("product")));
                catalogoVO.setFraccAr(String.valueOf(mapa.get("fraccAr")));
                catalogoVO.setInner_(String.valueOf(mapa.get("inner_")));
                catalogoVO.setValidacion(String.valueOf(mapa.get("validacion")));
                catalogoVO.setFvMxp(String.valueOf(mapa.get("fvMxp")));
                catalogoVO.setFvRmb(String.valueOf(mapa.get("fvRmb")));
                catalogoVO.setCtRmb(String.valueOf(mapa.get("ctRmb")));
                catalogoVO.setTextil(String.valueOf(mapa.get("textil")));
                catalogoVO.setCalzado(String.valueOf(mapa.get("calzado")));
                catalogoVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoVO.setArchivoExcel(String.valueOf(mapa.get("archivoExcel")));
                catalogoVO.setEstatus(String.valueOf(mapa.get("estatus")));
                
                if(catalogoVO.getEstatus().equals("1")){
                    catalogoVO.setEstatus("Nuevo");
                }
                if(catalogoVO.getEstatus().equals("2")){
                    catalogoVO.setEstatus("Actualizado");
                }
                if(catalogoVO.getEstatus().equals("3")){
                    catalogoVO.setEstatus("Erroneo");
                }
                catalogoList.add(catalogoNomVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }
    
     public List<CatalogoMaestroVO> obtenerErrores() throws Exception {
        Session session = null;
        List<CatalogoMaestroVO> catalogoErrores = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            
            sb.append(" SELECT idCatMaestro, codProveedor, codBarra, clave_de_linea_de_producto, familia, subFamilia, subSubFamilia, descripcion, category, ");
            sb.append(" middleCategory, subcategory, smallCategory, product, fraccAr, inner_, validacion, fvMxp, fvRmb, ctRmb, ");
            sb.append(" textil, calzado, fechaRegistro, fechaActualizacion, archivoExcel, estatus,descripcionError ");
            sb.append(" from catMaestro ");
            sb.append(" where estatus = 3");
            
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoMaestroVO catalogoMaestroVO = new CatalogoMaestroVO();
                                
                catalogoMaestroVO.setIdCatMaestro(String.valueOf(mapa.get("idCatMaestro")));
                catalogoMaestroVO.setCodProveedor(String.valueOf(mapa.get("codProveedor")));
                catalogoMaestroVO.setCodBarra(String.valueOf(mapa.get("codBarra")));
                catalogoMaestroVO.setClave_de_linea_de_producto(String.valueOf(mapa.get("clave_de_linea_de_producto")));
                catalogoMaestroVO.setFamilia(String.valueOf(mapa.get("familia")));
                catalogoMaestroVO.setSubFamilia(String.valueOf(mapa.get("subFamilia")));
                catalogoMaestroVO.setSubSubFamilia(String.valueOf(mapa.get("subSubFamilia")));
                catalogoMaestroVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoMaestroVO.setCategory(String.valueOf(mapa.get("category")));
                catalogoMaestroVO.setMiddleCategory(String.valueOf(mapa.get("middleCategory")));
                catalogoMaestroVO.setSubcategory(String.valueOf(mapa.get("subcategory")));
                catalogoMaestroVO.setSmallCategory(String.valueOf(mapa.get("smallCategory")));
                catalogoMaestroVO.setProduct(String.valueOf(mapa.get("product")));
                catalogoMaestroVO.setFraccAr(String.valueOf(mapa.get("fraccAr")));
                catalogoMaestroVO.setInner(String.valueOf(mapa.get("inner_")));
                catalogoMaestroVO.setValidacion(String.valueOf(mapa.get("validacion")));
                catalogoMaestroVO.setFvMxp(String.valueOf(mapa.get("fvMxp")));
                catalogoMaestroVO.setFvRmb(String.valueOf(mapa.get("fvRmb")));
                catalogoMaestroVO.setCtRmb(String.valueOf(mapa.get("ctRmb")));
                catalogoMaestroVO.setTextil(String.valueOf(mapa.get("textil")));
                catalogoMaestroVO.setCalzado(String.valueOf(mapa.get("calzado")));
                catalogoMaestroVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoMaestroVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoMaestroVO.setArchivoExcel(String.valueOf(mapa.get("archivoExcel")));
                catalogoMaestroVO.setEstatus(String.valueOf(mapa.get("estatus")));
                catalogoMaestroVO.setDescripcionError(String.valueOf(mapa.get("descripcionError")));                
                
                catalogoErrores.add(catalogoMaestroVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoErrores;
    }
}