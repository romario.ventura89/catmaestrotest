package mx.com.miniso.maestro.dao;

import java.util.List;
import mx.com.miniso.maestro.dto.TipoOrden;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TipoOrdenDao extends BaseDao<TipoOrden> {

    private final static Log LOG = LogFactory.getLog(TipoOrdenDao.class);

    @Autowired
    private SessionFactory sessionFactory;

    public TipoOrden obtenerXID(TipoOrden tipoOrden) {
        Session session = null;
        if (tipoOrden == null) {
            LOG.error("No se puede buscar el tipo de orden por que es nulo");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from tipoOrden t where t.idTipoOrden = :idTipoOrden");
            query.setParameter("idTipoOrden", tipoOrden.getIdTipoOrden());
            tipoOrden = (TipoOrden) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return tipoOrden;
    }

    public List<TipoOrden> buscarTodos() {
        Session session = null;
        List<TipoOrden> listTipoOrden = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from tipoOrden t order by t.descripcion ");
            listTipoOrden = query.list();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return listTipoOrden;
    }

}
