package mx.com.miniso.maestro.dao.compras;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mx.com.miniso.maestro.dto.Usuario;
import mx.com.miniso.maestro.dto.compras.ProductoCompras;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class ReporteComprasDao extends BaseDao<ProductoCompras> {

    private final static Log LOG = LogFactory.getLog(ReporteComprasDao.class);

    @Autowired
    @Qualifier("sessionFactoryCompras")
    private SessionFactory sessionFactory;

    public List<ProductoCompras> obtenerSKU() throws Exception {
        List<ProductoCompras> reporteList = new ArrayList<>();
        ProductoCompras productoCompra = new ProductoCompras();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from productoCompra p ");
            productoCompra =  (ProductoCompras) query.list();
        } catch (HibernateException ex) {
            LOG.error("No se puede conectar a la base de datos de compras", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return reporteList;
    }
    
    public List<String> obtenerSkuCompras(){
        Session session = null;
        session = sessionFactory.openSession();
        List<String> skus = new ArrayList<String>(0);
        StringBuilder sb = new StringBuilder();
        sb.append("select sku from productoCompra where 1=1 ");
        try {
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for(Map<String, Object> mapa : resulSet) {
                skus.add(String.valueOf(mapa.get("sku")));
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return skus;
    }

    public ProductoCompras guardar(ProductoCompras productoCompra) throws Exception {
        LOG.info("Guardando desperfecto: " + productoCompra);
        return guardar(sessionFactory, productoCompra);
    }

    public Usuario obtenerUsuarioXuserName(Usuario usuario) throws Exception {
        Session session = null;
        if (usuario == null) {
            LOG.error("No se puede buscar el usuario por que es nulo");
            return usuario;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from usuario u where u.userName = :userName");
            query.setParameter("userName", usuario.getUserName());
            usuario = (Usuario) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return usuario;
    }

    public ProductoCompras buscarXID(ProductoCompras productoCompra) {
        Session session = null;
        if (productoCompra == null) {
            LOG.error("No se puede buscar el skupor que es nulo");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from productoCompra p where p.idProductoCompra = :idProductoCompra");
            query.setParameter("idProductoCompra", productoCompra.getIdProductoCompra());
            productoCompra = (ProductoCompras) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return productoCompra;
    }

    public ProductoCompras buscarXSKU(ProductoCompras productoCompra) {
        Session session = null;
        if (productoCompra == null) {
            LOG.error("No se puede buscar el skupor que es nulo");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from productoCompra p where p.sku = :sku");
            query.setParameter("sku", productoCompra.getSku());
            productoCompra = (ProductoCompras) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return productoCompra;
    }

}
