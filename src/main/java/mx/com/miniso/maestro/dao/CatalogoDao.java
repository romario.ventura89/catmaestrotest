package mx.com.miniso.maestro.dao;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import mx.com.miniso.maestro.dto.Goods;
import mx.com.miniso.maestro.dto.Input;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CatalogoVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public class CatalogoDao extends BaseDao<Goods> {

    private final static Log LOG = LogFactory.getLog(CatalogoDao.class);

    @Autowired
    private SessionFactory sessionFactory;

    public List<CatalogoVO> obtenerXParametros(FiltrosCatalogoVO filtrosCatalogoVO) throws Exception {
        String campoFecha="";
        Session session = null;
        List<CatalogoVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select g.idGoods,g.brand,g.code,gd.code as cbarras,g.manufactor,g.munit,g.name,g.origin,g.qpc,g.rtlPrc,g.sort,g.spec,g.tm,g.uuid,g.fechaRegistro, g.fechaActualizacion, g.estatus, g.numeroJson,g.descripcion, gd.uuid as gduuid ");
            sb.append(" from goods g left outer join gdinput gd on g.uuid=gd.uuid ");
            sb.append(" where 1=1 ");
            if(filtrosCatalogoVO.getEstatus()!=null){
                if(!filtrosCatalogoVO.getEstatus().equals("0")){
                    if(filtrosCatalogoVO.getEstatus().equals("1")){
                        sb.append(" and g.estatus = 1 ");
                        campoFecha="g.fechaRegistro";
                    }
                    if(filtrosCatalogoVO.getEstatus().equals("2")){
                        sb.append(" and g.estatus =2 ");
                        campoFecha ="g.fechaActualizacion";
                    }
                    if(filtrosCatalogoVO.getEstatus().equals("3")){
                        sb.append(" and g.estatus = 3 ");
                        campoFecha ="g.fechaRegistro";
                    }
                }else{
                    campoFecha="g.fechaRegistro";
                }
            }
            if (!StringUtils.isEmpty(filtrosCatalogoVO.getFecha())) {
                sb.append(" and g.fechaRegistro = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoVO.getSku())) {
                sb.append(" and g.code like '%");
                sb.append(filtrosCatalogoVO.getSku());
                sb.append("%'");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoVO.getUuid())) {
                sb.append(" and g.uuid like '%");
                sb.append(filtrosCatalogoVO.getUuid());
                sb.append("%'");
            }
            if(filtrosCatalogoVO.getFechaInicio()!=null && filtrosCatalogoVO.getFechaFin()!=null){
                    if(!filtrosCatalogoVO.getFechaInicio().isEmpty() && !filtrosCatalogoVO.getFechaFin().isEmpty()){
                    sb.append(" and ");
                    sb.append(campoFecha);
                    sb.append(" between '");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoVO.getFechaInicio()));
                    sb.append("' ");
                    sb.append(" and ");
                    sb.append("' ");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoVO.getFechaFin()));
                    sb.append("' ");
                }
            }
            sb.append(" order by g.fechaRegistro, g.code ,g.uuid");

            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoVO catalogoVO = new CatalogoVO();
                catalogoVO.setIdGoods(String.valueOf(mapa.get("idGoods")));
                catalogoVO.setBrand(String.valueOf(mapa.get("brand")));
                catalogoVO.setCode(String.valueOf(mapa.get("code")));
                catalogoVO.setCodeBarras(String.valueOf(mapa.get("cbarras")));
                catalogoVO.setManufactor(String.valueOf(mapa.get("manufactor")));
                catalogoVO.setMunit(String.valueOf(mapa.get("munit")));
                catalogoVO.setName(String.valueOf(mapa.get("name")));
                catalogoVO.setOrigin(String.valueOf(mapa.get("origin")));
                catalogoVO.setQpc(String.valueOf(mapa.get("qpc")));
                catalogoVO.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogoVO.setSort(String.valueOf(mapa.get("sort")));
                catalogoVO.setSpec(String.valueOf(mapa.get("spec")));
                catalogoVO.setTm(String.valueOf(mapa.get("tm")));
                catalogoVO.setUuid(String.valueOf(mapa.get("uuid")));
                catalogoVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoVO.setEstatus(String.valueOf(mapa.get("estatus")));
                if(catalogoVO.getEstatus().equals("1")){
                    catalogoVO.setEstatus("Nuevo");
                }
                if(catalogoVO.getEstatus().equals("2")){
                    catalogoVO.setEstatus("Actualizado");
                }
                if(catalogoVO.getEstatus().equals("3")){
                    catalogoVO.setEstatus("Erroneo");
                }

                catalogoVO.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoVO.setNumeroJson(String.valueOf(mapa.get("descripcion")));
                catalogoVO.setUuidGd(String.valueOf(mapa.get("gduuid")));
                if(catalogoVO.getUuidGd().equals("null")){
                        catalogoVO.setProcesoDeOperacion("1/4");
                }else{
                    catalogoVO.setProcesoDeOperacion("2/4");
                }
                catalogoList.add(catalogoVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }

    
    public List<CatalogoVO> obtenerExcel(FiltrosCatalogoVO filtrosCatalogoVO) throws Exception {
        String campoFecha="";
        Session session = null;
        List<CatalogoVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select g.idGoods,g.brand,g.code,g.manufactor,g.munit,g.name,g.origin,g.qpc,g.rtlPrc,g.sort,g.spec,g.tm,g.uuid,g.fechaRegistro, g.fechaActualizacion, g.estatus, g.numeroJson,g.descripcion, gd.uuid as gduuid ");
            sb.append(" from goods g left outer join gdinput gd on g.uuid=gd.uuid ");
            sb.append(" where 1=1 ");
            if(filtrosCatalogoVO.getEstatus()!=null){
                if(!filtrosCatalogoVO.getEstatus().equals("0")){
                    if(filtrosCatalogoVO.getEstatus().equals("1")){
                        sb.append(" and g.estatus = 1 ");
                        campoFecha="g.fechaRegistro";
                    }
                    if(filtrosCatalogoVO.getEstatus().equals("2")){
                        sb.append(" and g.estatus =2 ");
                        campoFecha ="g.fechaActualizacion";
                    }
                    if(filtrosCatalogoVO.getEstatus().equals("3")){
                        sb.append(" and g.estatus = 3 ");
                        campoFecha ="g.fechaRegistro";
                    }
                }else{
                    campoFecha="g.fechaRegistro";
                }
            }
            if (!StringUtils.isEmpty(filtrosCatalogoVO.getFecha())) {
                sb.append(" and g.fechaRegistro = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoVO.getSku())) {
                sb.append(" and g.code like '%");
                sb.append(filtrosCatalogoVO.getSku());
                sb.append("%'");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoVO.getUuid())) {
                sb.append(" and g.uuid like '%");
                sb.append(filtrosCatalogoVO.getUuid());
                sb.append("%'");
            }
            if(filtrosCatalogoVO.getFechaInicio()!=null && filtrosCatalogoVO.getFechaFin()!=null){
                    if(!filtrosCatalogoVO.getFechaInicio().isEmpty() && !filtrosCatalogoVO.getFechaFin().isEmpty()){
                    sb.append(" and ");
                    sb.append(campoFecha);
                    sb.append(" between '");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoVO.getFechaInicio()));
                    sb.append("' ");
                    sb.append(" and ");
                    sb.append("' ");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoVO.getFechaFin()));
                    sb.append("' ");
                }
            }
            sb.append(" order by g.fechaRegistro, g.code ,g.uuid");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoVO catalogoVO = new CatalogoVO();
                catalogoVO.setIdGoods(String.valueOf(mapa.get("idGoods")));
                catalogoVO.setBrand(String.valueOf(mapa.get("brand")));
                catalogoVO.setCode(String.valueOf(mapa.get("code")));
                catalogoVO.setManufactor(String.valueOf(mapa.get("manufactor")));
                catalogoVO.setMunit(String.valueOf(mapa.get("munit")));
                catalogoVO.setName(String.valueOf(mapa.get("name")));
                catalogoVO.setOrigin(String.valueOf(mapa.get("origin")));
                catalogoVO.setQpc(String.valueOf(mapa.get("qpc")));
                catalogoVO.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogoVO.setSort(String.valueOf(mapa.get("sort")));
                catalogoVO.setSpec(String.valueOf(mapa.get("spec")));
                catalogoVO.setTm(String.valueOf(mapa.get("tm")));
                catalogoVO.setUuid(String.valueOf(mapa.get("uuid")));
                catalogoVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoVO.setEstatus(String.valueOf(mapa.get("estatus")));
                if(catalogoVO.getEstatus().equals("1")){
                    catalogoVO.setEstatus("Nuevo");
                }
                if(catalogoVO.getEstatus().equals("2")){
                    catalogoVO.setEstatus("Actualizado");
                }
                if(catalogoVO.getEstatus().equals("3")){
                    catalogoVO.setEstatus("Erroneo");
                }

                catalogoVO.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoVO.setNumeroJson(String.valueOf(mapa.get("descripcion")));
                catalogoVO.setUuidGd(String.valueOf(mapa.get("gduuid")));
                if(catalogoVO.getUuidGd().equals("null")){
                        catalogoVO.setProcesoDeOperacion("1/4");
                }else{
                    catalogoVO.setProcesoDeOperacion("2/4");
                }
                catalogoList.add(catalogoVO);

            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }
   
    public Goods guardarGoods(Goods goods) {
        guardar(sessionFactory, goods);
        return goods;
    }
    
    public List<String> obtenerListaSKU() throws Exception {
        Session session = null;
        List<String> skus = new ArrayList<String>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select code ");
            sb.append(" from goods ");
            sb.append(" where 1=1 ");
            sb.append(" order by code asc ");

            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                skus.add(String.valueOf(mapa.get("code")));
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return skus;
    }

    public List<CatalogoVO> obtenDetalleErreneo(CatalogoVO catalogoVO) {
        Session session = null;
        List<CatalogoVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select idGoods,brand,code,manufactor,munit,name,origin,qpc,rtlPrc,sort,spec,tm,uuid,fechaRegistro, fechaActualizacion, estatus, numeroJson, descripcion ");
            sb.append(" from goods ");
            sb.append(" where 1=1 ");

            if (!StringUtils.isEmpty(catalogoVO.getCode())) {
                sb.append(" and code like '%");
                sb.append(catalogoVO.getCode());
                sb.append("%'");
            }

            sb.append(" order by fechaRegistro, code ,uuid");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoVO catalogo = new CatalogoVO();
                catalogo.setIdGoods(String.valueOf(mapa.get("idGoods")));
                catalogo.setBrand(String.valueOf(mapa.get("brand")));
                catalogo.setCode(String.valueOf(mapa.get("code")));
                catalogo.setManufactor(String.valueOf(mapa.get("manufactor")));
                catalogo.setMunit(String.valueOf(mapa.get("munit")));
                catalogo.setName(String.valueOf(mapa.get("name")));
                catalogo.setOrigin(String.valueOf(mapa.get("origin")));
                catalogo.setQpc(String.valueOf(mapa.get("qpc")));
                catalogo.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogo.setSort(String.valueOf(mapa.get("sort")));
                catalogo.setSpec(String.valueOf(mapa.get("spec")));
                catalogo.setTm(String.valueOf(mapa.get("tm")));
                catalogo.setUuid(String.valueOf(mapa.get("uuid")));
                catalogo.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogo.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogo.setEstatus(String.valueOf(mapa.get("estatus")));
                if(catalogo.getEstatus().equals("1")){
                    catalogo.setEstatus("Nuevo");
                }
                if(catalogo.getEstatus().equals("2")){
                    catalogo.setEstatus("Actualizado");
                }
                if(catalogo.getEstatus().equals("3")){
                    catalogo.setEstatus("Erroneo");
                }
                if(catalogo.getEstatus().equals("4")){
                    catalogo.setEstatus("Editado");
                }
                catalogo.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogo.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoList.add(catalogo);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }
    
    public List<CatalogoVO> obtenDetalleXId(CatalogoVO catalogoVO) {
        Session session = null;
        List<CatalogoVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select idGoods,brand,code,manufactor,munit,name,origin,qpc,rtlPrc,sort,spec,tm,uuid,fechaRegistro, fechaActualizacion, estatus, numeroJson ");
            sb.append(" from goods ");
            sb.append(" where 1=1 ");

            if (!StringUtils.isEmpty(catalogoVO.getCode())) {
                sb.append(" and idGoods like '%");
                sb.append(catalogoVO.getCode());
                sb.append("%'");
            }

            sb.append(" order by fechaRegistro, code ,uuid");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoVO catalogo = new CatalogoVO();
                catalogo.setIdGoods(String.valueOf(mapa.get("idGoods")));
                catalogo.setBrand(String.valueOf(mapa.get("brand")));
                catalogo.setCode(String.valueOf(mapa.get("code")));
                catalogo.setManufactor(String.valueOf(mapa.get("manufactor")));
                catalogo.setMunit(String.valueOf(mapa.get("munit")));
                catalogo.setName(String.valueOf(mapa.get("name")));
                catalogo.setOrigin(String.valueOf(mapa.get("origin")));
                catalogo.setQpc(String.valueOf(mapa.get("qpc")));
                catalogo.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogo.setSort(String.valueOf(mapa.get("sort")));
                catalogo.setSpec(String.valueOf(mapa.get("spec")));
                catalogo.setTm(String.valueOf(mapa.get("tm")));
                catalogo.setUuid(String.valueOf(mapa.get("uuid")));
                catalogo.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogo.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogo.setEstatus(String.valueOf(mapa.get("estatus")));
                if(catalogo.getEstatus().equals("1")){
                    catalogo.setEstatus("Nuevo");
                }
                if(catalogo.getEstatus().equals("2")){
                    catalogo.setEstatus("Actualizado");
                }
                if(catalogo.getEstatus().equals("3")){
                    catalogo.setEstatus("Erroneo");
                }                
                catalogo.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoList.add(catalogo);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }

    public Goods obtenerXSku(Goods goods) {
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("Select idGoods,brand,code,manufactor,munit,name,origin,qpc,rtlPrc,sort,spec,tm,uuid,fechaRegistro, fechaActualizacion ");
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from goods g where g.code = :codes and g.uuid = :uuids");
            /*query.setParameter(0,goods.getCode());
            query.setParameter(1,goods.getCode());*/
            
            query.setParameter("codes", goods.getCode());
            query.setParameter("uuids", goods.getUuid());
            goods = (Goods) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return goods;
    }
    
    public Input obtenerXSkuInput(Input input) {
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("Select idgdinput,code,munit,qpc,qpcStr,rtlPrc,rtlPrc,fechaRegistro,fechaActualizacion,estatus,numeroJson ");
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from gdinput g where g.code = :code");
            query.setParameter("code", input.getCode());
            input = (Input) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return input;
    }
    
    public Goods obtenerXErroneo(Goods goods) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from goods g where g.code = :code");
            query.setParameter("code", goods.getCode());
            goods = (Goods) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return goods;
    }

    public void borrar(Goods goods) {
        delete(sessionFactory, goods);
    }

    public void actualizar(Goods goods) {
        System.out.println("goods brand" +goods.getBrand());
        update(sessionFactory, goods);
    }
    
    public List<CatalogoVO> obtenerErrores() throws Exception {
        Session session = null;
        List<CatalogoVO> catalogoListErrores = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select idGoods,brand,code,manufactor,munit,name,origin,qpc,rtlPrc,sort,spec,tm,uuid,fechaRegistro, fechaActualizacion, estatus, numeroJson,descripcion ");
            sb.append(" from goods ");
            sb.append(" where estatus = 3");           
            sb.append(" order by fechaRegistro, code ,uuid");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoVO catalogoVO = new CatalogoVO();
                catalogoVO.setIdGoods(String.valueOf(mapa.get("idGoods")));
                catalogoVO.setBrand(String.valueOf(mapa.get("brand")));
                catalogoVO.setCode(String.valueOf(mapa.get("code")));
                catalogoVO.setManufactor(String.valueOf(mapa.get("manufactor")));
                catalogoVO.setMunit(String.valueOf(mapa.get("munit")));
                catalogoVO.setName(String.valueOf(mapa.get("name")));
                catalogoVO.setOrigin(String.valueOf(mapa.get("origin")));
                catalogoVO.setQpc(String.valueOf(mapa.get("qpc")));
                catalogoVO.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogoVO.setSort(String.valueOf(mapa.get("sort")));
                catalogoVO.setSpec(String.valueOf(mapa.get("spec")));
                catalogoVO.setTm(String.valueOf(mapa.get("tm")));
                catalogoVO.setUuid(String.valueOf(mapa.get("uuid")));
                catalogoVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoVO.setEstatus(String.valueOf(mapa.get("estatus")));                
                catalogoVO.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoListErrores.add(catalogoVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoListErrores;
    }
}
