package mx.com.miniso.maestro.dao;

import mx.com.miniso.maestro.dto.Navegacion;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NavegacionDao extends BaseDao<Navegacion> {

    private final static Log LOG = LogFactory.getLog(NavegacionDao.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Navegacion guardar(Navegacion navegacion) throws Exception {
        guardar(sessionFactory, navegacion);
        return navegacion;

    }

    public void actualizar(Navegacion navegacion) throws Exception {
        update(sessionFactory, navegacion);
    }

    public void borrar(Navegacion navegacion) {
        delete(sessionFactory, navegacion);
    }

    public Navegacion obtenerXId(Navegacion navegacion) throws Exception {
        Session session = null;
        if (navegacion == null) {
            LOG.error("Instancia de navegación nula, no se puede ejecutar el query");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from navegacion n where n.idNavegacion = :idNavegacion");
            query.setParameter("idNavegacion", navegacion.getIdNavegacion());
            navegacion = (Navegacion) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return navegacion;
    }

    
}
