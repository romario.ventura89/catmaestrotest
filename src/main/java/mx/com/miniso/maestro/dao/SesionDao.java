package mx.com.miniso.maestro.dao;

import mx.com.miniso.maestro.dto.Sesion;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SesionDao extends BaseDao<Sesion> {

    private final static Log LOG = LogFactory.getLog(SesionDao.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Sesion guardar(Sesion sesion) throws Exception {
        guardar(sessionFactory, sesion);
        return sesion;

    }

    public void actualizar(Sesion sesion) throws Exception {
        update(sessionFactory, sesion);
    }

    public void borrar(Sesion sesion) {
        delete(sessionFactory, sesion);
    }

    public Sesion obtenerXUsuario(Sesion sesion) throws Exception {
        Session session = null;
        if (sesion == null) {
            LOG.error("Instancia de sesión es nula, no se puede ejecutar el query");
            return null;
        }
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from sesion s where s.idUsuario = :idUsuario");
            query.setParameter("idUsuario", sesion.getIdUsuario());
            sesion = (Sesion) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return sesion;
    }

}
