package mx.com.miniso.maestro.dao;
import java.util.ArrayList;
import java.util.Map;

import java.util.List;
import mx.com.miniso.maestro.dto.Goods;
import mx.com.miniso.maestro.dto.Input;
import mx.com.miniso.maestro.util.FechaUtil;
import mx.com.miniso.maestro.vo.CatalogoInputVO;
import mx.com.miniso.maestro.vo.FiltrosCatalogoInputVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public class CatalogoInputDao extends BaseDao<Input> {

    private final static Log LOG = LogFactory.getLog(CatalogoInputDao.class);

    @Autowired
    private SessionFactory sessionFactory;

    public List<CatalogoInputVO> obtenerXParametros(FiltrosCatalogoInputVO filtrosCatalogoInputVO) throws Exception {
        String campoFecha="";
        Session session = null;
        List<CatalogoInputVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select gd.idgdinput, g.code as sku, gd.code, gd.munit, gd.qpc, qpcStr, gd.rtlPrc, gd.uuid, ");
            sb.append("gd.fechaRegistro, gd.fechaActualizacion, gd.estatus, gd.numeroJson, gd.descripcion ");
            sb.append(" from gdinput gd left outer join goods g on gd.uuid = g.uuid ");
            sb.append(" where 1=1 ");
            if(filtrosCatalogoInputVO.getEstatus()!=null){
                if(!filtrosCatalogoInputVO.getEstatus().equals("0")){
                    if(filtrosCatalogoInputVO.getEstatus().equals("1")){
                        sb.append(" and gd.estatus = 1 ");
                        campoFecha="gd.fechaRegistro";
                    }
                    if(filtrosCatalogoInputVO.getEstatus().equals("2")){
                        sb.append(" and gd.estatus =2 ");
                        campoFecha ="gd.fechaActualizacion";
                    }
                    if(filtrosCatalogoInputVO.getEstatus().equals("3")){
                        sb.append(" and gd.estatus = 3 ");
                        campoFecha ="gd.fechaRegistro";
                    }
                }else{
                    campoFecha="gd.fechaRegistro";
                }
            }

            
            if(!StringUtils.isEmpty(filtrosCatalogoInputVO.getSku())){
                sb.append(" and g.code like '%");
                sb.append(filtrosCatalogoInputVO.getSku());
                sb.append("%'");
            }
            
            if (!StringUtils.isEmpty(filtrosCatalogoInputVO.getFecha())) {
                sb.append(" and gd.fechaRegistro = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoInputVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoInputVO.getcBarras())) {
                sb.append(" and gd.code like '%");
                sb.append(filtrosCatalogoInputVO.getcBarras());
                sb.append("%'");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoInputVO.getUuid())) {
                sb.append(" and gd.uuid like '%");
                sb.append(filtrosCatalogoInputVO.getUuid());
                sb.append("%'");
            }
            if(filtrosCatalogoInputVO.getFechaFin() != null && filtrosCatalogoInputVO.getFechaFin()!= null){
                if(!filtrosCatalogoInputVO.getFechaInicio().isEmpty() && !filtrosCatalogoInputVO.getFechaFin().isEmpty()){
                    sb.append(" and ");
                    sb.append(campoFecha);
                    sb.append(" between '");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoInputVO.getFechaInicio()));
                    sb.append("' ");
                    sb.append(" and ");
                    sb.append("' ");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoInputVO.getFechaFin()));
                    sb.append("' ");
                }
            }
            sb.append(" order by gd.fechaRegistro, gd.code");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoInputVO catalogoInputVO = new CatalogoInputVO();
                catalogoInputVO.setIdGdInput(String.valueOf(mapa.get("idgdinput")));
                catalogoInputVO.setSku(String.valueOf(mapa.get("sku")));
                catalogoInputVO.setCodeBarras(String.valueOf(mapa.get("code")));
                catalogoInputVO.setMunit(String.valueOf(mapa.get("munit")));
                catalogoInputVO.setQpc(String.valueOf(mapa.get("qpc")));
                catalogoInputVO.setQpcStr(String.valueOf(mapa.get("qpcStr")));
                catalogoInputVO.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogoInputVO.setUuid(String.valueOf(mapa.get("uuid")));
                catalogoInputVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoInputVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoInputVO.setEstatus(String.valueOf(mapa.get("estatus")));
                if(catalogoInputVO.getEstatus().equals("1")){
                    catalogoInputVO.setEstatus("Nuevo");
                }
                if(catalogoInputVO.getEstatus().equals("2")){
                    catalogoInputVO.setEstatus("Actualizado");
                }
                if(catalogoInputVO.getEstatus().equals("3")){
                    catalogoInputVO.setEstatus("Erroneo");
                }
                catalogoInputVO.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoInputVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                if(catalogoInputVO.getSku().equals("null")){
                    catalogoInputVO.setProcesoDeOperacion("1/4");
                }else{
                    catalogoInputVO.setProcesoDeOperacion("2/4");
                }
                catalogoList.add(catalogoInputVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }
    
    public List<CatalogoInputVO> obtenerExcel(FiltrosCatalogoInputVO filtrosCatalogoInputVO) throws Exception {
        String campoFecha="";
        Session session = null;
        List<CatalogoInputVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select gd.idgdinput, g.code as sku, gd.code, gd.munit, gd.qpc, qpcStr, gd.rtlPrc, gd.uuid, ");
            sb.append("gd.fechaRegistro, gd.fechaActualizacion, gd.estatus, gd.numeroJson, gd.descripcion ");
            sb.append(" from gdinput gd left outer join goods g on gd.uuid = g.uuid ");
            sb.append(" where 1=1 ");
            if(filtrosCatalogoInputVO.getEstatus()!=null){
                if(!filtrosCatalogoInputVO.getEstatus().equals("0")){
                    if(filtrosCatalogoInputVO.getEstatus().equals("1")){
                        sb.append(" and gd.estatus = 1 ");
                        campoFecha="gd.fechaRegistro";
                    }
                    if(filtrosCatalogoInputVO.getEstatus().equals("2")){
                        sb.append(" and gd.estatus =2 ");
                        campoFecha ="gd.fechaActualizacion";
                    }
                    if(filtrosCatalogoInputVO.getEstatus().equals("3")){
                        sb.append(" and gd.estatus = 3 ");
                        campoFecha ="gd.fechaRegistro";
                    }
                }else{
                    campoFecha="gd.fechaRegistro";
                }
            }

            if(!StringUtils.isEmpty(filtrosCatalogoInputVO.getSku())){
                sb.append(" and g.code like '%");
                sb.append(filtrosCatalogoInputVO.getSku());
                sb.append("%'");
            }
            
            if (!StringUtils.isEmpty(filtrosCatalogoInputVO.getFecha())) {
                sb.append(" and gd.fechaRegistro = '");
                sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoInputVO.getFecha()));
                sb.append("' ");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoInputVO.getcBarras())) {
                sb.append(" and gd.code like '%");
                sb.append(filtrosCatalogoInputVO.getcBarras());
                sb.append("%'");
            }
            if (!StringUtils.isEmpty(filtrosCatalogoInputVO.getUuid())) {
                sb.append(" and gd.uuid like '%");
                sb.append(filtrosCatalogoInputVO.getUuid());
                sb.append("%'");
            }
            if(filtrosCatalogoInputVO.getFechaFin() != null && filtrosCatalogoInputVO.getFechaFin()!= null){
                if(!filtrosCatalogoInputVO.getFechaInicio().isEmpty() && !filtrosCatalogoInputVO.getFechaFin().isEmpty()){
                    sb.append(" and ");
                    sb.append(campoFecha);
                    sb.append(" between '");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoInputVO.getFechaInicio()));
                    sb.append("' ");
                    sb.append(" and ");
                    sb.append("' ");
                    sb.append(FechaUtil.convierteFechaStringTODate(filtrosCatalogoInputVO.getFechaFin()));
                    sb.append("' ");
                }
            }
            sb.append(" order by gd.fechaRegistro, gd.code");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoInputVO catalogoInputVO = new CatalogoInputVO();
                catalogoInputVO.setIdGdInput(String.valueOf(mapa.get("idgdinput")));
                catalogoInputVO.setSku(String.valueOf(mapa.get("sku")));
                catalogoInputVO.setCodeBarras(String.valueOf(mapa.get("code")));
                catalogoInputVO.setMunit(String.valueOf(mapa.get("munit")));
                catalogoInputVO.setQpc(String.valueOf(mapa.get("qpc")));
                catalogoInputVO.setQpcStr(String.valueOf(mapa.get("qpcStr")));
                catalogoInputVO.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogoInputVO.setUuid(String.valueOf(mapa.get("uuid")));
                catalogoInputVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoInputVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoInputVO.setEstatus(String.valueOf(mapa.get("estatus")));
                if(catalogoInputVO.getEstatus().equals("1")){
                    catalogoInputVO.setEstatus("Nuevo");
                }
                if(catalogoInputVO.getEstatus().equals("2")){
                    catalogoInputVO.setEstatus("Actualizado");
                }
                if(catalogoInputVO.getEstatus().equals("3")){
                    catalogoInputVO.setEstatus("Erroneo");
                }
                catalogoInputVO.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoInputVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                if(catalogoInputVO.getSku().equals("null")){
                    catalogoInputVO.setProcesoDeOperacion("1/4");
                }else{
                    catalogoInputVO.setProcesoDeOperacion("2/4");
                }
                catalogoList.add(catalogoInputVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }

    public Input guardar(Input input) {
        guardar(sessionFactory, input);
        return input;
    }

    public List<CatalogoInputVO> obtenDetalleErreneo(CatalogoInputVO catalogoInputVO) {
        Session session = null;
        List<CatalogoInputVO> catalogoList = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select gd.idgdinput, g.code as sku, gd.code, gd.munit, gd.qpc, qpcStr, gd.rtlPrc, gd.uuid, ");
            sb.append("gd.fechaRegistro, gd.fechaActualizacion, gd.estatus, gd.numeroJson, gd.descripcion ");
            sb.append(" from gdinput gd left join goods g on gd.uuid = g.uuid ");
            sb.append(" where 1=1 ");
            
            if (!StringUtils.isEmpty(catalogoInputVO.getCodeBarras())) {
                sb.append(" and gd.code like '%");
                sb.append(catalogoInputVO.getCodeBarras());
                sb.append("%'");
            }

            sb.append(" order by gd.code");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoInputVO catalogo = new CatalogoInputVO();
                catalogo.setIdGdInput(String.valueOf(mapa.get("idgdinput")));
                catalogo.setSku(String.valueOf(mapa.get("sku")));
                catalogo.setCodeBarras(String.valueOf(mapa.get("code")));
                catalogo.setMunit(String.valueOf(mapa.get("munit")));
                catalogo.setQpc(String.valueOf(mapa.get("qpc")));
                catalogo.setQpc(String.valueOf(mapa.get("qpcStr")));
                catalogo.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogo.setUuid(String.valueOf(mapa.get("uuid")));
                catalogo.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogo.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogo.setEstatus(String.valueOf(mapa.get("estatus")));
                catalogo.setDescripcion(String.valueOf(mapa.get("descripcion")));
                if(catalogo.getEstatus().equals("1")){
                    catalogo.setEstatus("Nuevo");
                }
                if(catalogo.getEstatus().equals("2")){
                    catalogo.setEstatus("Actualizado");
                }
                if(catalogo.getEstatus().equals("3")){
                    catalogo.setEstatus("Erroneo");
                }
                if(catalogo.getEstatus().equals("4")){
                    catalogo.setEstatus("Editado");
                }
                catalogo.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoList.add(catalogo);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoList;
    }

    public Input obtenerXSku(Input input) {
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("select idgdinput,code,munit,qpc,qpcStr,rtlPrc,uuid,fechaRegistro, fechaActualizacion, estatus, numeroJson ");
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from gdinput g where g.code = :code");
            query.setParameter("code", input.getCode());
            
            input = (Input) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return input;
    }
    
    public Input obtenerXSkuInput(Input input) {
        Session session = null;
        StringBuilder sb = new StringBuilder();
        sb.append("Select idgdinput,code,munit,qpc,qpcStr,rtlPrc,rtlPrc,fechaRegistro,fechaActualizacion,estatus,numeroJson ");
        
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from gdinput g where g.code = :code");
            query.setParameter("code", input.getCode());
            input = (Input) query.uniqueResult();
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return input;
    }
    
    public Goods obtenerXErroneo(Goods goods) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from goods g where g.code = :code");
            query.setParameter("code", goods.getCode());
//            query.setParameter("estatus", "3");
            goods = (Goods) query.uniqueResult();
            System.out.println("consulta "+query.toString());
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return goods;
    }

    public void borrar(Input input) {
        delete(sessionFactory, input);
    }

    public void actualizar(Input input) {
        update(sessionFactory, input);
    }
    
     public List<CatalogoInputVO> obtenerErrores() throws Exception {       
        Session session = null;
        List<CatalogoInputVO> catalogoErrores = new ArrayList<>(0);
        try {
            session = sessionFactory.openSession();
            StringBuilder sb = new StringBuilder();
            sb.append(" select idgdinput, code, munit, qpc, qpcStr, rtlPrc, uuid, ");
            sb.append(" fechaRegistro, fechaActualizacion, estatus, numeroJson,descripcion");
            sb.append(" from gdinput ");
            sb.append(" where estatus = 3");           
            sb.append(" order by fechaRegistro,code");
            Query query = session.createSQLQuery(sb.toString());
            List<Map<String, Object>> resulSet = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).setMaxResults(150).list();
            for (Map<String, Object> mapa : resulSet) {
                CatalogoInputVO catalogoInputVO = new CatalogoInputVO();
                catalogoInputVO.setIdGdInput(String.valueOf(mapa.get("idgdinput")));               
                catalogoInputVO.setCodeBarras(String.valueOf(mapa.get("code")));
                catalogoInputVO.setMunit(String.valueOf(mapa.get("munit")));
                catalogoInputVO.setQpc(String.valueOf(mapa.get("qpc")));
                catalogoInputVO.setQpcStr(String.valueOf(mapa.get("qpcStr")));
                catalogoInputVO.setRtlPrc(String.valueOf(mapa.get("rtlPrc")));
                catalogoInputVO.setUuid(String.valueOf(mapa.get("uuid")));
                catalogoInputVO.setFechaRegistro(String.valueOf(mapa.get("fechaRegistro")));
                catalogoInputVO.setFechaActualizacion(String.valueOf(mapa.get("fechaActualizacion")));
                catalogoInputVO.setEstatus(String.valueOf(mapa.get("estatus")));             
                catalogoInputVO.setNumeroJson(String.valueOf(mapa.get("numeroJson")));
                catalogoInputVO.setDescripcion(String.valueOf(mapa.get("descripcion")));
                catalogoErrores.add(catalogoInputVO);
            }
        } catch (HibernateException ex) {
            LOG.error("Error en base de datos", ex);
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return catalogoErrores;
    }

}
