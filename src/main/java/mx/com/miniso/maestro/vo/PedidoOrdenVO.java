package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class PedidoOrdenVO implements Serializable {

    private String idConfirmacion;
    private String piezas;
    private String sku;

    public String getIdConfirmacion() {
        return idConfirmacion;
    }

    public void setIdConfirmacion(String idConfirmacion) {
        this.idConfirmacion = idConfirmacion;
    }

    public String getPiezas() {
        return piezas;
    }

    public void setPiezas(String piezas) {
        this.piezas = piezas;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

}
