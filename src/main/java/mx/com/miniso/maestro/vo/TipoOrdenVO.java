package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class TipoOrdenVO implements Serializable {

    private boolean idTipoOrden;
    private String descipcion;

    public boolean isIdTipoOrden() {
        return idTipoOrden;
    }

    public void setIdTipoOrden(boolean idTipoOrden) {
        this.idTipoOrden = idTipoOrden;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    

}
