
package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class FiltroPedidosVO implements Serializable{
    
    String fechaInicio;
    String fechaFin;

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    } 
}
