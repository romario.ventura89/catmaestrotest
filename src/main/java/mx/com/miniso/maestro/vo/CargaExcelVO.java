package mx.com.miniso.maestro.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CargaExcelVO implements Serializable {

    private String fechaCarga;
    private String numeroExcel;
    private List<CargaExcelDatosVO> datos;
    private List<CargaExcelDatosVO> correctos;
    private List<CargaExcelDatosVO> actualizados;
    private List<CargaExcelDatosVO> erroneos;
        

    private List<CargaExcelDatosVO> catalogo_oficial_actual;

    private boolean fechaConfirmacionError;
    private boolean numeroOrdenError;

    private String fechaConfirmacionErrorTxt;
    private String numeroOrdenErrorTxt;

    private boolean error;
    private boolean remplazar;

    private boolean errorListado;

    public CargaExcelVO() {
        this.datos = new ArrayList<>(0);
        this.correctos = new ArrayList<>(0);
        this.actualizados = new ArrayList<>(0);
        this.erroneos = new ArrayList<>(0);
        this.catalogo_oficial_actual = new ArrayList<>(0);
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getNumeroExcel() {
        return numeroExcel;
    }

    public void setNumeroExcel(String numeroExcel) {
        this.numeroExcel = numeroExcel;
    }

    public List<CargaExcelDatosVO> getDatos() {
        return datos;
    }

    public void setDatos(List<CargaExcelDatosVO> datos) {
        this.datos = datos;
    }

    public List<CargaExcelDatosVO> getCatalogo_oficial_actual(){
        return catalogo_oficial_actual;
    }

    public void setCatalogo_oficial_actual(List<CargaExcelDatosVO> catalogo_oficial_actual) {
        this.catalogo_oficial_actual = catalogo_oficial_actual;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isRemplazar() {
        return remplazar;
    }

    public void setRemplazar(boolean remplazar) {
        this.remplazar = remplazar;
    }

    public boolean isFechaConfirmacionError() {
        return fechaConfirmacionError;
    }

    public void setFechaConfirmacionError(boolean fechaConfirmacionError) {
        this.fechaConfirmacionError = fechaConfirmacionError;
    }

    public boolean isNumeroOrdenError() {
        return numeroOrdenError;
    }

    public void setNumeroOrdenError(boolean numeroOrdenError) {
        this.numeroOrdenError = numeroOrdenError;
    }

    public String getFechaConfirmacionErrorTxt() {
        return fechaConfirmacionErrorTxt;
    }

    public void setFechaConfirmacionErrorTxt(String fechaConfirmacionErrorTxt) {
        this.fechaConfirmacionErrorTxt = fechaConfirmacionErrorTxt;
    }

    public String getNumeroOrdenErrorTxt() {
        return numeroOrdenErrorTxt;
    }

    public void setNumeroOrdenErrorTxt(String numeroOrdenErrorTxt) {
        this.numeroOrdenErrorTxt = numeroOrdenErrorTxt;
    }

    public boolean isErrorListado() {
        return errorListado;
    }

    public void setErrorListado(boolean errorListado) {
        this.errorListado = errorListado;
    }

    public List<CargaExcelDatosVO> getActualizados() {
        return actualizados;
    }

    public void setActualizados(List<CargaExcelDatosVO> actualizados) {
        this.actualizados = actualizados;
    }

    public List<CargaExcelDatosVO> getCorrectos() {
        return correctos;
    }

    public void setCorrectos(List<CargaExcelDatosVO> correctos) {
        this.correctos = correctos;
    }
    
    public List<CargaExcelDatosVO> getErroneos() {
        return erroneos;
    }

    public void setErroneos(List<CargaExcelDatosVO> erroneos) {
        this.erroneos = erroneos;
    }

}
