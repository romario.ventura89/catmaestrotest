package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class SociedadVO implements Serializable {

    private String idSociedad;
    private String descripcion;

    public String getIdSociedad() {
        return idSociedad;
    }

    public void setIdSociedad(String idSociedad) {
        this.idSociedad = idSociedad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    

}
