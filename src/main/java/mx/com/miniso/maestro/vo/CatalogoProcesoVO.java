package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class CatalogoProcesoVO implements Serializable {

    private String sku;
    private String cBarras;
    private String uuid;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getcBarras() {
        return cBarras;
    }

    public void setcBarras(String cBarras) {
        this.cBarras = cBarras;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    
}
