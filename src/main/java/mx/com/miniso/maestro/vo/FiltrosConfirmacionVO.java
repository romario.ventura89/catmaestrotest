package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class FiltrosConfirmacionVO implements Serializable {

    private String sku;
    private String fecha;
    private String codigoBarras;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }


}
