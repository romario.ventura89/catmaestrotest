package mx.com.miniso.maestro.vo;

import java.io.Serializable; 

public class CatalogoMaestroVO implements Serializable {

    private String idCatMaestro;
    private String codProveedor;
    private String codBarra; 
    private String clave_de_linea_de_producto;
    private String familia;
    private String subFamilia;
    private String subSubFamilia;
    private String descripcion;
    private String category;
    private String middleCategory;
    private String subcategory;
    private String smallCategory;
    private String product;
    private String fraccAr;
    private String inner;
    private String validacion;
    private String fvMxp;
    private String fvRmb;
    private String ctRmb;
    private String textil;
    private String calzado;
    private String fechaRegistro;
    private String fechaActualizacion;
    private String archivoExcel;
    private String estatus;
    private String descripcionError;

    public String getIdCatMaestro() {
        return idCatMaestro;
    }

    public void setIdCatMaestro(String idCatMaestro) {
        this.idCatMaestro = idCatMaestro;
    }

    public String getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(String codProveedor) {
        this.codProveedor = codProveedor;
    }

    public String getCodBarra() {
        return codBarra;
    }

    public void setCodBarra(String codBarra) {
        this.codBarra = codBarra;
    }
    
    public String getClave_de_linea_de_producto() {
        return clave_de_linea_de_producto;
    }

    public void setClave_de_linea_de_producto(String clave_de_linea_de_producto) {
        this.clave_de_linea_de_producto = clave_de_linea_de_producto;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getSubFamilia() {
        return subFamilia;
    }

    public void setSubFamilia(String subFamilia) {
        this.subFamilia = subFamilia;
    }

    public String getSubSubFamilia() {
        return subSubFamilia;
    }

    public void setSubSubFamilia(String subSubFamilia) {
        this.subSubFamilia = subSubFamilia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMiddleCategory() {
        return middleCategory;
    }

    public void setMiddleCategory(String middleCategory) {
        this.middleCategory = middleCategory;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getSmallCategory() {
        return smallCategory;
    }

    public void setSmallCategory(String smallCategory) {
        this.smallCategory = smallCategory;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getFraccAr() {
        return fraccAr;
    }

    public void setFraccAr(String fraccAr) {
        this.fraccAr = fraccAr;
    }

    public String getInner() {
        return inner;
    }

    public void setInner(String inner) {
        this.inner = inner;
    }

    public String getValidacion() {
        return validacion;
    }

    public void setValidacion(String validacion) {
        this.validacion = validacion;
    }

    public String getFvMxp() {
        return fvMxp;
    }

    public void setFvMxp(String fvMxp) {
        this.fvMxp = fvMxp;
    }

    public String getFvRmb() {
        return fvRmb;
    }

    public void setFvRmb(String fvRmb) {
        this.fvRmb = fvRmb;
    }

    public String getCtRmb() {
        return ctRmb;
    }

    public void setCtRmb(String ctRmb) {
        this.ctRmb = ctRmb;
    }

    public String getTextil() {
        return textil;
    }

    public void setTextil(String textil) {
        this.textil = textil;
    }

    public String getCalzado() {
        return calzado;
    }

    public void setCalzado(String calzado) {
        this.calzado = calzado;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getArchivoExcel() {
        return archivoExcel;
    }

    public void setArchivoExcel(String archivoExcel) {
        this.archivoExcel = archivoExcel;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

}
