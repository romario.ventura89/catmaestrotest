package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class CargaExcelDatosVO implements Serializable {

    private String cod_proveedor;
    private String cod_barra;
    private String clave_de_linea_de_producto;
    private String familia;
    private String subfamilia;
    private String sub_subfamilia;
    private String descripcion;
    private String category;
    private String middle_category;
    private String subcategory;
    private String small_category;
    private String product;
    private String fracc_ar;
    private String inner;
    private String validacion;
    private String fv_mxp;
    private String fv_rmb;
    private String ct_rmb;
    private String textil;
    private String calzado;
    private String fecha_registro;
    private String archivo_excel;
    private String descripcionError;

    private boolean skuError;
    private boolean nombreError;
    private boolean piezasSolicitadasError;
    private boolean piezasEntregadasError;
    private boolean precioError;

    private String skuErrorTXT;

    private boolean notOrder;

    public String getCod_proveedor() {
        return cod_proveedor;
    }

    public void setCod_proveedor(String cod_proveedor) {
        this.cod_proveedor = cod_proveedor;
    }

    public String getCod_barra() {
        return cod_barra;
    }

    public void setCod_barra(String cod_barra) {
        this.cod_barra = cod_barra;
    }

    public String getClave_de_linea_de_producto() {
        return clave_de_linea_de_producto;
    }

    public void setClave_de_linea_de_producto(String clave_de_linea_de_producto) {
        this.clave_de_linea_de_producto = clave_de_linea_de_producto;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getSubfamilia() {
        return subfamilia;
    }

    public void setSubfamilia(String subfamilia) {
        this.subfamilia = subfamilia;
    }

    public String getSub_subfamilia() {
        return sub_subfamilia;
    }

    public void setSub_subfamilia(String sub_subfamilia) {
        this.sub_subfamilia = sub_subfamilia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMiddle_category() {
        return middle_category;
    }

    public void setMiddle_category(String middle_category) {
        this.middle_category = middle_category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getSmall_category() {
        return small_category;
    }

    public void setSmall_category(String small_category) {
        this.small_category = small_category;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getFracc_ar() {
        return fracc_ar;
    }

    public void setFracc_ar(String fracc_ar) {
        this.fracc_ar = fracc_ar;
    }

    public String getInner() {
        return inner;
    }

    public void setInner(String inner) {
        this.inner = inner;
    }

    public String getValidacion() {
        return validacion;
    }

    public void setValidacion(String validacion) {
        this.validacion = validacion;
    }

    public String getFv_mxp() {
        return fv_mxp;
    }

    public void setFv_mxp(String fv_mxp) {
        this.fv_mxp = fv_mxp;
    }

    public String getFv_rmb() {
        return fv_rmb;
    }

    public void setFv_rmb(String fv_rmb) {
        this.fv_rmb = fv_rmb;
    }

    public String getCt_rmb() {
        return ct_rmb;
    }

    public void setCt_rmb(String ct_rmb) {
        this.ct_rmb = ct_rmb;
    }

    public String getTextil() {
        return textil;
    }

    public void setTextil(String textil) {
        this.textil = textil;
    }

    public String getCalzado() {
        return calzado;
    }

    public void setCalzado(String calzado) {
        this.calzado = calzado;
    }

    public String getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(String fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getArchivo_excel() {
        return archivo_excel;
    }

    public void setArchivo_excel(String archivo_excel) {
        this.archivo_excel = archivo_excel;
    }

    public boolean isNotOrder() {
        return notOrder;
    }

    public void setNotOrder(boolean notOrder) {
        this.notOrder = notOrder;
    }

    public boolean isSkuError() {
        return skuError;
    }

    public void setSkuError(boolean skuError) {
        this.skuError = skuError;
    }

    public boolean isNombreError() {
        return nombreError;
    }

    public void setNombreError(boolean nombreError) {
        this.nombreError = nombreError;
    }

    public boolean isPiezasSolicitadasError() {
        return piezasSolicitadasError;
    }

    public void setPiezasSolicitadasError(boolean piezasSolicitadasError) {
        this.piezasSolicitadasError = piezasSolicitadasError;
    }

    public boolean isPiezasEntregadasError() {
        return piezasEntregadasError;
    }

    public void setPiezasEntregadasError(boolean piezasEntregadasError) {
        this.piezasEntregadasError = piezasEntregadasError;
    }

    public boolean isPrecioError() {
        return precioError;
    }

    public void setPrecioError(boolean precioError) {
        this.precioError = precioError;
    }

    public String getSkuErrorTXT() {
        return skuErrorTXT;
    }

    public void setSkuErrorTXT(String skuErrorTXT) {
        this.skuErrorTXT = skuErrorTXT;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

}
