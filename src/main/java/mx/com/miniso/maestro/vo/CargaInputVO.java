package mx.com.miniso.maestro.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.dto.Input;

public class CargaInputVO implements Serializable {

    private String fechaCarga;
    private String numeroJson;

    private List<Input> correctos;
    private List<Input> erroneos;
    
    private List<Input> nuevos;
    private List<Input> actualizados;
    private List<Input> errores;
    
    private boolean fechaConfirmacionError;
    private boolean numeroOrdenError;

    private String fechaConfirmacionErrorTxt;
    private String numeroOrdenErrorTxt;

    private boolean error;
    private boolean remplazar;

    private boolean errorListado;

    public CargaInputVO() {
        this.correctos = new ArrayList<>(0);
        this.erroneos = new ArrayList<>(0);
        this.nuevos = new ArrayList<>(0);
        this.actualizados = new ArrayList<>(0);
        this.errores = new ArrayList<>(0);
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getNumeroJson() {
        return numeroJson;
    }

    public void setNumeroJson(String numeroJson) {
        this.numeroJson = numeroJson;
    }

    public List<Input> getCorrectos() {
        return correctos;
    }

    public void setCorrectos(List<Input> correctos) {
        this.correctos = correctos;
    }

    public List<Input> getErroneos() {
        return erroneos;
    }

    public void setErroneos(List<Input> erroneos) {
        this.erroneos = erroneos;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isRemplazar() {
        return remplazar;
    }

    public void setRemplazar(boolean remplazar) {
        this.remplazar = remplazar;
    }

    public boolean isFechaConfirmacionError() {
        return fechaConfirmacionError;
    }

    public void setFechaConfirmacionError(boolean fechaConfirmacionError) {
        this.fechaConfirmacionError = fechaConfirmacionError;
    }

    public boolean isNumeroOrdenError() {
        return numeroOrdenError;
    }

    public void setNumeroOrdenError(boolean numeroOrdenError) {
        this.numeroOrdenError = numeroOrdenError;
    }

    public String getFechaConfirmacionErrorTxt() {
        return fechaConfirmacionErrorTxt;
    }

    public void setFechaConfirmacionErrorTxt(String fechaConfirmacionErrorTxt) {
        this.fechaConfirmacionErrorTxt = fechaConfirmacionErrorTxt;
    }

    public String getNumeroOrdenErrorTxt() {
        return numeroOrdenErrorTxt;
    }

    public void setNumeroOrdenErrorTxt(String numeroOrdenErrorTxt) {
        this.numeroOrdenErrorTxt = numeroOrdenErrorTxt;
    }

    public boolean isErrorListado() {
        return errorListado;
    }

    public void setErrorListado(boolean errorListado) {
        this.errorListado = errorListado;
    }

    public List<Input> getNuevos() {
        return nuevos;
    }

    public void setNuevos(List<Input> nuevos) {
        this.nuevos = nuevos;
    }

    public List<Input> getActualizados() {
        return actualizados;
    }

    public void setActualizados(List<Input> actualizados) {
        this.actualizados = actualizados;
    }

    public List<Input> getErrores() {
        return errores;
    }

    public void setErrores(List<Input> errores) {
        this.errores = errores;
    }

}
