package mx.com.miniso.maestro.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import mx.com.miniso.maestro.dto.Goods;

public class CargaGoodsVO implements Serializable {

    private String fechaCarga;
    private String numeroJson;

    private List<Goods> correctos;
    private List<Goods> erroneos;
    
    private List<Goods> nuevos;
    private List<Goods> actualizados;
    private List<Goods> errores;

    private boolean fechaConfirmacionError;
    private boolean numeroOrdenError;

    private String fechaConfirmacionErrorTxt;
    private String numeroOrdenErrorTxt;

    private boolean error;
    private boolean remplazar;

    private boolean errorListado;

    public CargaGoodsVO() {
        this.correctos = new ArrayList<>(0);
        this.erroneos = new ArrayList<>(0);
        this.nuevos = new ArrayList<>(0);
        this.actualizados = new ArrayList<>(0);
        this.errores = new ArrayList<>(0);
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getNumeroJson() {
        return numeroJson;
    }

    public void setNumeroJson(String numeroJson) {
        this.numeroJson = numeroJson;
    }

    public List<Goods> getCorrectos() {
        return correctos;
    }

    public void setCorrectos(List<Goods> correctos) {
        this.correctos = correctos;
    }

    public List<Goods> getErroneos() {
        return erroneos;
    }

    public void setErroneos(List<Goods> erroneos) {
        this.erroneos = erroneos;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isRemplazar() {
        return remplazar;
    }

    public void setRemplazar(boolean remplazar) {
        this.remplazar = remplazar;
    }

    public boolean isFechaConfirmacionError() {
        return fechaConfirmacionError;
    }

    public void setFechaConfirmacionError(boolean fechaConfirmacionError) {
        this.fechaConfirmacionError = fechaConfirmacionError;
    }

    public boolean isNumeroOrdenError() {
        return numeroOrdenError;
    }

    public void setNumeroOrdenError(boolean numeroOrdenError) {
        this.numeroOrdenError = numeroOrdenError;
    }

    public String getFechaConfirmacionErrorTxt() {
        return fechaConfirmacionErrorTxt;
    }

    public void setFechaConfirmacionErrorTxt(String fechaConfirmacionErrorTxt) {
        this.fechaConfirmacionErrorTxt = fechaConfirmacionErrorTxt;
    }

    public String getNumeroOrdenErrorTxt() {
        return numeroOrdenErrorTxt;
    }

    public void setNumeroOrdenErrorTxt(String numeroOrdenErrorTxt) {
        this.numeroOrdenErrorTxt = numeroOrdenErrorTxt;
    }

    public boolean isErrorListado() {
        return errorListado;
    }

    public void setErrorListado(boolean errorListado) {
        this.errorListado = errorListado;
    }

    public List<Goods> getNuevos() {
        return nuevos;
    }

    public void setNuevos(List<Goods> nuevos) {
        this.nuevos = nuevos;
    }

    public List<Goods> getActualizados() {
        return actualizados;
    }

    public void setActualizados(List<Goods> actualizados) {
        this.actualizados = actualizados;
    }

    public List<Goods> getErrores() {
        return errores;
    }

    public void setErrores(List<Goods> errores) {
        this.errores = errores;
    }

}
