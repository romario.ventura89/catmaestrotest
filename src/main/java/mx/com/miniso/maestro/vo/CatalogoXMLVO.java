package mx.com.miniso.maestro.vo;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CatalogoXMLVO {

    private String numeroOrden;
    private String fecha;
    private List<CatalogoExcelVO> detalles;

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<CatalogoExcelVO> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<CatalogoExcelVO> detalles) {
        this.detalles = detalles;
    }

}
