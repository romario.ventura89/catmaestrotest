package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class NavegacionVO implements Serializable {

    private boolean login;
    private String path;

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
