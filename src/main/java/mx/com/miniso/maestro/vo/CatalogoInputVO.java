package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class CatalogoInputVO implements Serializable {    
    private String idGdInput;
    private String codeBarras;
    private String sku;
    private String munit;
    private String qpc;
    private String qpcStr;
    private String rtlPrc;
    private String uuid;
    private String fechaRegistro;
    private String fechaActualizacion;
    private String estatus;
    private String numeroJson;
    private String descripcion;
    private String procesoDeOperacion;

    public String getIdGdInput() {
        return idGdInput;
    }

    public void setIdGdInput(String idGdInput) {
        this.idGdInput = idGdInput;
    }

    public String getCodeBarras() {
        return codeBarras;
    }

    public void setCodeBarras(String codeBarras) {
        this.codeBarras = codeBarras;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getMunit() {
        return munit;
    }

    public void setMunit(String munit) {
        this.munit = munit;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public String getQpc() {
        return qpc;
    }

    public void setQpc(String qpc) {
        this.qpc = qpc;
    }

    public String getQpcStr() {
        return qpcStr;
    }

    public void setQpcStr(String qpcStr) {
        this.qpcStr = qpcStr;
    }

    public String getRtlPrc() {
        return rtlPrc;
    }

    public void setRtlPrc(String rtlPrc) {
        this.rtlPrc = rtlPrc;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getNumeroJson() {
        return numeroJson;
    }

    public void setNumeroJson(String numeroJson) {
        this.numeroJson = numeroJson;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProcesoDeOperacion() {
        return procesoDeOperacion;
    }

    public void setProcesoDeOperacion(String procesoDeOperacion) {
        this.procesoDeOperacion = procesoDeOperacion;
    }

}
