package mx.com.miniso.maestro.vo;

import java.io.Serializable;

public class CatalogoVO implements Serializable {
    private String idGoods;
    private String brand;
    private String code;
    private String codeBarras;
    private String manufactor;
    private String munit;
    private String name;
    private String origin;
    private String qpc;
    private String rtlPrc;
    private String sort;
    private String spec;
    private String tm;
    private String uuid;
    private String fechaRegistro;
    private String fechaActualizacion;
    private String estatus;
    private String numeroJson;
    private String descripcion;
    private String procesoDeOperacion;
    private String uuidGd;
    
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getManufactor() {
        return manufactor;
    }

    public void setManufactor(String manufactor) {
        this.manufactor = manufactor;
    }

    public String getMunit() {
        return munit;
    }

    public void setMunit(String munit) {
        this.munit = munit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getTm() {
        return tm;
    }

    public void setTm(String tm) {
        this.tm = tm;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIdGoods() {
        return idGoods;
    }

    public void setIdGoods(String idGoods) {
        this.idGoods = idGoods;
    }

    public String getQpc() {
        return qpc;
    }

    public void setQpc(String qpc) {
        this.qpc = qpc;
    }

    public String getRtlPrc() {
        return rtlPrc;
    }

    public void setRtlPrc(String rtlPrc) {
        this.rtlPrc = rtlPrc;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getNumeroJson() {
        return numeroJson;
    }

    public void setNumeroJson(String numeroJson) {
        this.numeroJson = numeroJson;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProcesoDeOperacion() {
        return procesoDeOperacion;
    }

    public void setProcesoDeOperacion(String procesoDeOperacion) {
        this.procesoDeOperacion = procesoDeOperacion;
    }

    public String getUuidGd() {
        return uuidGd;
    }

    public void setUuidGd(String uuidGd) {
        this.uuidGd = uuidGd;
    }

    public String getCodeBarras() {
        return codeBarras;
    }

    public void setCodeBarras(String codeBarras) {
        this.codeBarras = codeBarras;
    }

}
