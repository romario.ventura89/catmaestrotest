package mx.com.miniso.maestro.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CargaNomVO implements Serializable {

    private String fechaCarga;
    private String numeroExcel;
    private List<CargaNomDatosVO> datos;
    private List<CargaNomDatosVO> correctos;
    private List<CargaNomDatosVO> actualizados;
    private List<CargaNomDatosVO> erroneos;
        

    private List<CargaNomDatosVO> catalogo_oficial_actual;

    private boolean fechaConfirmacionError;
    private boolean numeroOrdenError;

    private String fechaConfirmacionErrorTxt;
    private String numeroOrdenErrorTxt;

    private boolean error;
    private boolean remplazar;

    private boolean errorListado;

    public CargaNomVO() {
        this.datos = new ArrayList<>(0);
        this.correctos = new ArrayList<>(0);
        this.actualizados = new ArrayList<>(0);
        this.erroneos = new ArrayList<>(0);
        this.catalogo_oficial_actual = new ArrayList<>(0);
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getNumeroExcel() {
        return numeroExcel;
    }

    public void setNumeroExcel(String numeroExcel) {
        this.numeroExcel = numeroExcel;
    }

    public List<CargaNomDatosVO> getDatos() {
        return datos;
    }

    public void setDatos(List<CargaNomDatosVO> datos) {
        this.datos = datos;
    }

    public List<CargaNomDatosVO> getCatalogo_oficial_actual(){
        return catalogo_oficial_actual;
    }

    public void setCatalogo_oficial_actual(List<CargaNomDatosVO> catalogo_oficial_actual) {
        this.catalogo_oficial_actual = catalogo_oficial_actual;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isRemplazar() {
        return remplazar;
    }

    public void setRemplazar(boolean remplazar) {
        this.remplazar = remplazar;
    }

    public boolean isFechaConfirmacionError() {
        return fechaConfirmacionError;
    }

    public void setFechaConfirmacionError(boolean fechaConfirmacionError) {
        this.fechaConfirmacionError = fechaConfirmacionError;
    }

    public boolean isNumeroOrdenError() {
        return numeroOrdenError;
    }

    public void setNumeroOrdenError(boolean numeroOrdenError) {
        this.numeroOrdenError = numeroOrdenError;
    }

    public String getFechaConfirmacionErrorTxt() {
        return fechaConfirmacionErrorTxt;
    }

    public void setFechaConfirmacionErrorTxt(String fechaConfirmacionErrorTxt) {
        this.fechaConfirmacionErrorTxt = fechaConfirmacionErrorTxt;
    }

    public String getNumeroOrdenErrorTxt() {
        return numeroOrdenErrorTxt;
    }

    public void setNumeroOrdenErrorTxt(String numeroOrdenErrorTxt) {
        this.numeroOrdenErrorTxt = numeroOrdenErrorTxt;
    }

    public boolean isErrorListado() {
        return errorListado;
    }

    public void setErrorListado(boolean errorListado) {
        this.errorListado = errorListado;
    }

    public List<CargaNomDatosVO> getCorrectos() {
        return correctos;
    }

    public void setCorrectos(List<CargaNomDatosVO> correctos) {
        this.correctos = correctos;
    }

    public List<CargaNomDatosVO> getActualizados() {
        return actualizados;
    }

    public void setActualizados(List<CargaNomDatosVO> actualizados) {
        this.actualizados = actualizados;
    }

    public List<CargaNomDatosVO> getErroneos() {
        return erroneos;
    }

    public void setErroneos(List<CargaNomDatosVO> erroneos) {
        this.erroneos = erroneos;
    }

}