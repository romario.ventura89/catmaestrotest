$(document).ready(function () {
    inicio();
    function inicio() {
        $("#username").focus();
        eventos();

        function eventos() {
            $("#enviar").click(function () {
                validaLogin();
            });

            $("#username").keypress(function (event) {
                var keycode = event.keyCode || event.which;
                if (keycode == '13') {
                    validaLogin();
                }
            });
            $("#contra").keypress(function (event) {
                var keycode = event.keyCode || event.which;
                if (keycode == '13') {
                    validaLogin();
                }
            });

        }

        function validaLogin() {
            if (validaFormulario()) {
                var path = "service/login/entraLogin";
                var datos = {
                    usuario: $("#username").val(),
                    password: $("#contra").val()
                };

                conectaPost(path, datos, function (data) {
                    loadPage("#cuerpo", "service/navegacion/pagina?id=1");
                    loadPage("#menu", "service/navegacion/menu");
                });


            }
        }

        function validaFormulario() {
            if ($("#username").val().trim() == "") {
                mensajeError("Recuerda capturar tu usuario");
                $("#username").focus();
                return false;
            }
            if ($("#contra").val().trim() == "") {
                mensajeError("Recuerda capturar tu contrase&ntilde;a");
                $("#contra").focus();
                return false;
            }

            return true;
        }
    }
});

