function mensajeExito(mensaje)
{
    mensajeAlerta('success', mensaje);
}

function mensajeError(mensaje)
{
    mensajeAlerta('error', mensaje);
}

function mensajePrevencion(mensaje)
{
    mensajeAlerta('warning', mensaje);
}

function mensajeInformacion(mensaje)
{
    mensajeAlerta('info', mensaje);
}

function mensajeDefault(mensaje)
{
    mensajeAlerta('alert', mensaje);
}

function mensajeAlerta(id, mensaje, clase) {
    var componente = '<div class="alert ' + clase + '" role="alert">' + mensaje + '</div>';
    $(id).html(componente);
}

function mensajeAlerta(tipo, mensaje)
{
    new Noty({
        layout: 'topRight',
        theme: 'metroui',
        type: tipo,
        text: mensaje,
        timeout: 5000,
        closeWith: ['click', 'button'],
        animation: {
            open: 'animated fadeInRight',
            close: 'animated fadeOutRight'
        }
    }).show();



}
Noty.setMaxVisible(5);

function limpiaMensaje(id)
{
    $(id).html('');
}

function muestraMensaje(id) {
    $(id).show();
}

function escondeMensaje(id)
{
    $(id).hide();
}

function mensajeAlertaTiempo(id, tiempo) {
    setTimeout(function () {
        escondeMensaje(id);
    }, tiempo);

}

function crearDatatable(id, listaFiltros)
{
    var i = 0;
    var tabla = $(id).DataTable({
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var mostrar = false;
                for (var j = 0; j < listaFiltros.length; j++)
                {
                    if (i == listaFiltros[j])
                    {
                        mostrar = true;
                        break;
                    }
                }
                i++;
                if (mostrar)
                {
                    var select = $('<br /><select><option value=""></option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }

            });
        }

    });
    return tabla;
}

function crearDatatable100(id, listaFiltros)
{
    var i = 0;
    var tabla = $(id).DataTable({
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var mostrar = false;
                for (var j = 0; j < listaFiltros.length; j++)
                {
                    if (i == listaFiltros[j])
                    {
                        mostrar = true;
                        break;
                    }
                }
                i++;
                if (mostrar)
                {
                    var select = $('<br /><select><option value=""></option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }

            });
        },
        "pageLength": 100

    });
    return tabla;
}

function crearDatatableFacturas(id, listaFiltros)
{
    var i = 0;
    var tabla = $(id).DataTable({
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var mostrar = false;
                for (var j = 0; j < listaFiltros.length; j++)
                {
                    if (i == listaFiltros[j])
                    {
                        mostrar = true;
                        break;
                    }
                }
                i++;
                if (mostrar)
                {
                    var select = $('<br /><select><option value=""></option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }

            });
        },
        columnDefs: [
            {"width": "100px", "targets": 0},
            {"width": "100px", "targets": 1},
            {"width": "100px", "targets": 2},
            {"width": "100px", "targets": 3},
            {"width": "100px", "targets": 4},
            {"width": "100px", "targets": 5},
            {"width": "100px", "targets": 6},
            {"width": "100px", "targets": 7},
            {"width": "150px", "targets": 8},
            {"width": "100px", "targets": 9},
            {"width": "100px", "targets": 10}
        ],
        scrollCollapse: true,
        "scrollX": true,
        fixedColumns: {
            leftColumns: 1
        }

    });
    return tabla;
}

function crearDatatableConfirmaciones(id, listaFiltros)
{
    var i = 0;
    var tabla = $(id).DataTable({
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var mostrar = false;
                for (var j = 0; j < listaFiltros.length; j++)
                {
                    if (i == listaFiltros[j])
                    {
                        mostrar = true;
                        break;
                    }
                }
                i++;
                if (mostrar)
                {
                    var select = $('<br /><select><option value=""></option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }

            });
        },
        columnDefs: [
            {"width": "100px", "targets": 0},
            {"width": "100px", "targets": 1},
            {"width": "100px", "targets": 2},
            {"width": "100px", "targets": 3},
            {"width": "150px", "targets": 4},
            {"width": "150px", "targets": 5},
            {"width": "150px", "targets": 6},
            {"width": "100px", "targets": 7},
            {"width": "100px", "targets": 8}
        ],
        scrollCollapse: true,
        "scrollX": true,
        fixedColumns: {
            leftColumns: 2
        }

    });
    return tabla;
}

function crearDatatableComodato(id, listaFiltros, columnas)
{
    var i = 0;
    var tabla = $(id).DataTable({
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var mostrar = false;
                for (var j = 0; j < listaFiltros.length; j++)
                {
                    if (i == listaFiltros[j])
                    {
                        mostrar = true;
                        break;
                    }
                }
                i++;
                if (mostrar)
                {
                    var select = $('<br /><select><option value=""></option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }

            });
        },
        "pageLength": 100,
        columnDefs: columnas,
        scrollCollapse: true,
        "scrollX": true,
        fixedColumns: {
            leftColumns: 4,
            rightColumns: 2
        }

    });
    return tabla;
}


function crearDatatablePedidos(id, listaFiltros, columnas)
{
    var i = 0;
    var tabla = $(id).DataTable({
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var mostrar = false;
                for (var j = 0; j < listaFiltros.length; j++)
                {
                    if (i == listaFiltros[j])
                    {
                        mostrar = true;
                        break;
                    }
                }
                i++;
                if (mostrar)
                {
                    var select = $('<br /><select><option value=""></option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }

            });
        },
        "pageLength": 100,
        columnDefs: columnas,
        scrollCollapse: true,
        "scrollX": true,
        fixedColumns: {
            leftColumns: 2,
            rightColumns: 4
        }

    });
    return tabla;
}

