var reporte = "";
var tablaReporte = "";
$(document).ready(function () {

    var cuentaFacturas = 0;

    $("#fechaInicio").datepicker();
    $("#fechaFin").datepicker();
    eventos();

    function eventos() {
        $("#limpiar").click(function () {
            limpiar();
            $("#botonDescarga").hide();
        });

        $("#buscar").click(function () {
            obtenerReporte();
        });

        $("#descargarReporte").click(function () {
            descargarReporte();
        });
    }


    function obtenerReporte() {
        var path = "service/comodato/buscar";
        var datos = {
            fechaInicio: $("#fechaInicio").val(),
            fechaFin: $("#fechaFin").val()
        };
        conectaPost(path, datos, function (data) {
            reporte = data;
            console.log("Datos recuperados", data);
            llenaReporte();
            $("#botonDescarga").show();
        });
    }

    function limpiar() {
        $("#fechaInicio").val("");
        $("#fechaFin").val("");
        $("#resultado").html("");
    }

    function llenaReporte() {
        var tablaHTML = "";
        tablaHTML += llenaTitulos();
        $("#resultado").html(tablaHTML);
        var cuerpoHTML = "";
        cuerpoHTML += llenaValores();

        $("#cuerpoTabla").html(cuerpoHTML);
        var tamanioColumnas = new Array();
        tamanioColumnas.push({"width": "50px", "targets": 0});
        tamanioColumnas.push({"width": "150px", "targets": 1});
        tamanioColumnas.push({"width": "150px", "targets": 2});
        tamanioColumnas.push({"width": "50px", "targets": 3});
        for (var i = 1; i <= cuentaFacturas; i++)
        {
            if (cuentaFacturas <= 5) {
                if (cuentaFacturas == 1)
                {
                    tamanioColumnas.push({"width": "500px", "targets": i + 3});
                }

                if (cuentaFacturas == 2)
                {
                    tamanioColumnas.push({"width": "250px", "targets": i + 3});
                }

                if (cuentaFacturas == 3)
                {
                    tamanioColumnas.push({"width": "175px", "targets": i + 3});
                }

                if (cuentaFacturas == 4)
                {
                    tamanioColumnas.push({"width": "120px", "targets": i + 3});
                }

                if (cuentaFacturas == 5)
                {
                    tamanioColumnas.push({"width": "100px", "targets": i + 3});
                }

            } else {
                tamanioColumnas.push({"width": "80px", "targets": i + 3});
            }
        }

        tamanioColumnas.push({"width": "100px", "targets": cuentaFacturas + 1});
        tamanioColumnas.push({"width": "80px", "targets": cuentaFacturas + 2});

        var filtros = new Array();


        tablaReporte = crearDatatableComodato("#reporte", filtros, tamanioColumnas);



    }

    function llenaTitulos() {
        cuentaFacturas = 0;
        var resHTML = "";
        resHTML += "<table id='reporte' class='stripe row-border order-column'><thead>";
        resHTML += "<tr><th style='background:white'>SKU</th>";
        resHTML += "<th style='background:white'>PRODUCT NAME</th>";
        resHTML += "<th style='background:white'>ORDER NO</th>";
        resHTML += "<th style='background:white'>PIECES CONFIRMED</th>";
        for (var i = 0; i < reporte.contenedores.length; i++)
        {
            var contenedor = reporte.contenedores[i];
            resHTML += "<th class='text-center'>" + contenedor.nombreContenedor + "</th>";
            cuentaFacturas++;
        }
        resHTML += "<th style='background:white'>TOTAL INVOICE</th>";
        resHTML += "<th style='background:white'>DIFFERENCE</th>";
        resHTML += "</tr></thead>";
        resHTML += "<tbody id='cuerpoTabla'></tbody></table>";
        return resHTML;
    }

    function llenaValores() {
        var resHTML = "";
        for (var i = 0; i < reporte.comodatos.length; i++)
        {
            var registro = reporte.comodatos[i];
            resHTML += "<tr>";
            resHTML += "<td>" + registro.sku + "</td>";
            resHTML += "<td>" + registro.productName + "</td>";
            resHTML += "<td>" + registro.numeroOrden + "</td>";
            resHTML += "<td class='text-right'>" + registro.piezasConfirmadas + "</td>";
            for (var j = 0; j < registro.contenedores.length; j++)
            {
                var contenedor = registro.contenedores[j];
                resHTML += "<td class='text-center'>" + contenedor.piezas + "</td>";
            }

            resHTML += "<td class='text-right'>" + registro.totalFacturas + "</td>";

            var registrosNoEnviados = registro.noEnviados.replace(",", "");
            registrosNoEnviados = registrosNoEnviados * 1;

            if (registrosNoEnviados < 0) {
                resHTML += "<td class='bg-danger text-right'>";
            } else if (registrosNoEnviados == 0) {
                resHTML += "<td class='bg-success text-right'>";
            } else if (registrosNoEnviados > 0) {
                resHTML += "<td class='bg-info text-right'>";
            } else {
                resHTML += "<td class='text-right'>";
            }


            resHTML += registro.noEnviados + "</td>";
            resHTML += "</tr>";
        }
        return resHTML;
    }

    function descargarReporte()
    {
        leerArchivo("service/comodato/excel", "REPORTE_COMODATO.xlsx");
    }

});

