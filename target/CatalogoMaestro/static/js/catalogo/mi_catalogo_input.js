var datosCatalogo = "";
var detalleProductos = "";
var confirmacionSel = "";
var tabla = "";
var detalleCatalogo="";
var detalle="";
var flagFecha="";

$(document).ready(function () {
    $("#fecha").datepicker({language: 'es'});
    $("#fechaInicio").datepicker({language: 'es'});
    $("#fechafin").datepicker({language: 'es'});
    eventos();

    function eventos() {

        $("#limpiar").click(function () {
            limpiar();
        });

        $("#buscar").click(function () {
            if(validaFechas()){
                buscar();    
            }
        });
        
        $("#aceptarEliminar").click(function () {
            eliminaSKU(catalogoSel);
        });
        
//        $("#actualizar").click(function () {
//            actualizar();
//        });
        
        $("#limpiarFec").click(function (){
           limpFecha();
        });
        
        $("#limpiarRan").click(function (){
            $("#fechaInicio").val("");
            $("#fechafin").val("");
        })
        
        document.addEventListener("keyup", function(event){
        event.preventDefault();
        if (event.keyCode === 13 || event.which == 13) {
        document.getElementById("buscar").click();
        }
        }); 
    }
    
    function validaFechas(){
      valuesStart=$("#fechaInicio").val().split("/");
        valuesEnd=$("#fechafin").val().split("/");         
        var fecIni=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]);
        var fecFin=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]);
        if($("#fecha").val()!="" && $("#fechaInicio").val()!="" && $("#fechafin").val()!=""){
            mensajeError("Todas las fechas han sido seleccionadas, favor de validar.");
            return false;
        }
        if ($("#fechafin").val()!=""){         
          if( fecIni > fecFin){
           mensajeError("La fecha inicio no puede se mayor a fecha fin, favor de validar.");
           return false;
          } 
        }
        if($("#fechaInicio").val()!="" && $("#fechafin").val()==""){
            mensajeError("Debe ingresar el valor de la fecha fin, favor de validar.");
            return false;
        }
        if($("#fechaInicio").val()=="" && $("#fechafin").val()!=""){
            mensajeError("Debe ingresar el valor de la fecha inicio, favor de validar.");
            return false;
        }
        
        return true;  
    }
    
    function actualizar(){
        var path = "service/catalogoInput/actualizar";
        var datos = {
            idGdInput:detalle.idGdInput,
            codeBarras:detalle.codeBarras,
            munit: $("#munit").val(),
            qpc:$("#qpc").val(),
            qpcStr:$("#qpcStr").val(),
            rtlPrc:$("#rtlPrc").val(),
            uuid:detalle.uuid,
            fechaRegistro:detalle.fechaRegistro
        };

        conectaPost(path, datos, function (data) {
            mensajeExito("Se realizo la actualizacion correctamente.");
            $("#cuerpoTabla").html("");
            datosCatalogo = data;
            buscar();
        });
    }

    function buscar() {
        var path = "service/catalogoInput/buscar";
        var datos = {
            uuid: $("#uuid").val(),
            cBarras: $("#cBarras").val(),
            fecha: $("#fecha").val(),
            fechaInicio:$("#fechaInicio").val(),
            fechaFin:$("#fechafin").val(),
            estatus:$("#status").val(),
            sku:$("#sku").val()
        };

        conectaPost(path, datos, function (data) {
            $("#cuerpoTabla").html("");
            datosCatalogo = data;
            $("#botonExcel").show();
            $("#panelConfirmacion").show();
            llenarConfirmaciones();
        });
    }

    function llenarConfirmaciones() {
        var resHTML = "";
        if(datosCatalogo.length <=0){
            resHTMLExcel="";
        }else{
            resHTMLExcel="<div class='row'>";
            resHTMLExcel += "<td class='text-center'>";
            resHTMLExcel += '<button type="button" id="excel"  class="excel btn btn-danger btn-xs">Exportar</button>';
            resHTMLExcel += "</td>";
            resHTMLExcel += "</div>";
        }
        for (var i = 0; i < datosCatalogo.length; i++){
            var cat = datosCatalogo[i];
            resHTML += "<tr>";
            resHTML += "<td style='text-align: justify;'>";
            resHTML += cat.estatus;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += cat.sku;
            resHTML += "</td>";
            
            resHTML += "<td>";
            resHTML += cat.codeBarras;
            resHTML += "</td>";
            
            resHTML += "<td>";
            resHTML += cat.procesoDeOperacion;
            resHTML += "</td>";
            
            resHTML += "<td class='text-center'>";
            resHTML += cat.munit;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += cat.qpc;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += cat.qpcStr;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += cat.uuid;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += cat.fechaRegistro;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="detalle-' + i + '"  class="detalle btn btn-info btn-xs confirmaciones" data-toggle="tooltip" data-placement="left">Detalle</button>';
            resHTML += "</td>";
            
            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="editar-' + i + '"  class="editar btn btn-success btn-xs confirmaciones" data-toggle="tooltip" data-placement="left">Editar</button>';
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="eli-' + i + '"  class="delete btn btn-danger btn-xs">Eliminar</button>';
            resHTML += "</td>";
        }
        if (tabla != ""){
            tabla.destroy();
        }

        $("#cuerpoTabla").html(resHTML);
        $("#contenidoExcel").html(resHTMLExcel);
        

        $(".detalle").click(function (e) {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("detalle-", "");
            catalogoSel = datosCatalogo[miId * 1];
            obtenDetalle(catalogoSel,1);
        });
        
        $(".editar").click(function (e) {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("editar-", "");
            catalogoSel = datosCatalogo[miId * 1];
            obtenDetalle(catalogoSel,2);
        });
        
        $(".delete").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("eli-", "");
            catalogoSel = datosCatalogo[miId * 1];
            $("#modalEliminar").modal();
            $("#eliminarOrdenTexto").html(catalogoSel.code);
            $("#eliminarTipoTexto").html(catalogoSel.uuid);

        });

        $(".xml").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("xml-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            obtenXML(confirmacionSel);
        });
        
        $("#excel").click(function () {
            obtenExcel();
        });
        
        var filtros = [];

        $("#titulosConfirmaciones").html(creaEncabezados());
        tabla = crearDatatableConfirmaciones("#tablaReporteConfirmacion", filtros);

    }

    function creaEncabezados() {
        var resHTML = "<tr>";
        resHTML += "<th style='background:white'>Estatus</th>";
        resHTML += "<th style='background:white'>Sku</th>";
        resHTML += "<th style='background:white'>Codigo de Barras</th>";
        resHTML += "<th>Proceso</th>";
        resHTML += "<th>munit</th>";
        resHTML += "<th>qpc</th>";
        resHTML += "<th>qpcStr</th>";
        resHTML += "<th>uuid</th>";
        resHTML += "<th>fechaRegistro</th>";
        resHTML += "<th class='text-center'>Detalle</th>";
        resHTML += "<th class='text-center'>Editar</th>";
        resHTML += "<th class='text-center'>Eliminar</th>";
        resHTML += "</tr>";
        return resHTML;
    }

    function obtenXML(confirmacion)
    {
        var path = "service/confirmacion/xml?idConfirmacion=" + confirmacion.idConfirmacion + "&idTipoOrden=" + confirmacion.idTipoOrden;
        window.open(path, '_blank');
    }

    function obtenExcel() {
        var path = "service/catalogoInput/excel?uuid=" + $("#uuid").val() +"&cbarras="+$("#cBarras").val()+ "&sku=" + $("#sku").val() + "&fecha=" + $("#fecha").val()+"&fechaInicio="+ $("#fechaInicio").val() + "&fechaFin=" + $("#fechafin").val() + "&status="+$("#status").val();
        window.open(path, '_blank');
    }

    function eliminaSKU(confirmacion) {
        var path = "service/catalogoInput/eliminar";
        conectaPost(path, confirmacion, function (data){
            mensajeExito("Se realizó la eliminación correctamente.");
            $("#modalEliminar").modal('toggle');
            buscar();
        });
    }

    function obtenDetalle(confirmacion,num){
        var path = "service/catalogoInput/detalle";
        conectaPost(path, confirmacion, function (data){
            detalleCatalogo = data;
            creaDetalle(num);
            $("#modalDetalle").modal();
        });
    }

    function creaDetalle(num) {
        $("#detalleNumOrden").html(catalogoSel.code);
        var resHTML = "";
        var resHTMLAct="<div class='row'>";
        resHTMLAct += "<td class='text-center'>";
        resHTMLAct+='<button type="button0" class="btn btn-danger" data-dismiss="modal">Cerrar</button>';

        var resHTMLGeneral = titulosTablaTabs("tablaGeneral");
        var general = false;
        if(num==1){
            for (var i = 0; i < detalleCatalogo.length; i++) {
                detalle = detalleCatalogo[i];
                resHTMLGeneral += llenaCuarpoTab(detalle);
                general = true;
            }
        }else{
            for (var i = 0; i < detalleCatalogo.length; i++) {
                detalle = detalleCatalogo[i];
                resHTMLGeneral += llenaCuarpoTabEditar(detalle);
                general = true;
            }
            resHTMLAct += '<button type="button" class="btn btn-primary" data-dismiss="modal" id="actualizar">Actualizar</button>';
        }
        resHTMLAct += "</td>";
        resHTMLAct += "</div>";
        resHTMLGeneral += "</tbody></table>";

        resHTML += '<ul class="nav nav-tabs" role="tablist">';
        if (general)
        {
            resHTML += '<li role="presentation" class="active" ><a href="#tabGeneral" aria-controls="tabGeneral" role="tab" data-toggle="tab">General</a></li>';
        }
        resHTML += '</ul>';

        resHTML += '<div class="tab-content" style="margin-top:10px">';
        if (general)
        {
            resHTML += '<div role="tabpanel" class="table-responsive" id="tabGeneral">1</div>';
        }
        resHTML += '</div>';
        $("#contenidoDetalle").html(resHTML);
        $("#contenidoActualizar").html(resHTMLAct);
        
        if (general){
            $("#tabGeneral").html(resHTMLGeneral);
            $("#tablaGeneral").DataTable();
            
        }
        $("#actualizar").click(function () {
            actualizar();
        });
    }

    function titulosTablaTabs(id) {
        var resHTML = "<table class='table-hover display' id='" + id + "'>";
        resHTML += "<thead><tr>";
        resHTML += "<th>Registro</th>";
        resHTML += "<th>Sku</th>";
        resHTML += "<th>Codigo de barras</th>";
        resHTML += "<th>Munit</th>";
        resHTML += "<th>qpc</th>";
        resHTML += "<th>qpcStr</th>";
        resHTML += "<th>rtlPrc</th>";
        resHTML += "<th>uuid</th>";
        resHTML += "<th>fechaRegistro</th>";
        resHTML += "<th>fechaActualizacion</th>";
        resHTML += "<th>estatus</th>";
        resHTML += "<th>descripcion</th>";
        resHTML += "</tr></thead>";
        resHTML += "<tbody>";
        return resHTML;
    }

    function llenaCuarpoTab(detalle){
        var resHTML = "";
        resHTML += '<tr>';

        resHTML += '<td>';
        resHTML += detalle.idGdInput;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.sku;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.codeBarras;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.munit;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.qpc;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.qpcStr;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.rtlPrc;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.uuid;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.fechaRegistro;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.fechaActualizacion;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.estatus;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.descripcion;
        resHTML += '</td>';

        resHTML += '</tr>';
        return resHTML;
    }
    
    function llenaCuarpoTabEditar(detalle){
        var resHTML = "";
        resHTML += '<tr>';

        resHTML += '<td>';
        resHTML += detalle.idGdInput;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.sku;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.codeBarras;
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="munit" value="'+detalle.munit+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="qpc" value="'+detalle.qpc+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="qpcStr" value="'+detalle.qpcStr+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="rtlPrc" value="'+detalle.rtlPrc+'">';
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.uuid;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.fechaRegistro;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.fechaActualizacion;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.estatus;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.descripcion;
        resHTML += '</td>';
        
        resHTML += '</tr>';
        return resHTML;
    }

    function limpiar() {
        $("#uuid").val("");
        $("#cBarras").val("");
        $("#fecha").val("");
        $("#fechaInicio").val("");
        $("#fechafin").val("");
        $("#status").val("0");
        $("#sku").val("");
    }
    
     function limpFecha(){     
     if (flagFecha == ""){
      flagFecha = 1;    
       $("#fecha").datepicker({
        language: 'es',
	dateFormat: 'dd/mm/yy'
    }).datepicker("setDate", new Date());   
    } else {
        flagFecha = "";
        $("#fecha").val("");
        }  
    };
});



