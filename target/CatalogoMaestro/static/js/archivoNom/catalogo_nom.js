var datosConfirmacion = "";
var detalleProductos = "";
var confirmacionSel = "";
var tabla = "";
var detalle="";
var detalleCatalogo="";

$(document).ready(function () {

    $("#fecha").datepicker({language: 'es'});
    $("#fechaInicio").datepicker({language: 'es'});
    $("#fechafin").datepicker({language: 'es'});
    
    eventos();

    function eventos() {
        $("#limpiar").click(function () {
            limpiar();
        });

        $("#buscar").click(function () {
            buscar();
        });
        
        $("#aceptarEliminar").click(function () {
            eliminaConfirmacion(confirmacionSel);
        });
        
        $("#limpiarFec").click(function (){
            $("#fecha").val("");
        });
        
        $("#limpiarRan").click(function (){
            $("#fechaInicio").val("");
            $("#fechafin").val("");
        });
        
        $("#excel").click(function () {
            obtenExcel();
        });
        
        $("#xml").click(function () {
            obtenXML();
        });
    }

    function buscar() {
        var path = "service/catalogoNom/buscar";
         var datos = {
            uuid: $("#uuid").val(),
            cBarras: $("#cBarras").val(),
            fecha: $("#fecha").val(),
            fechaInicio:$("#fechaInicio").val(),
            fechaFin:$("#fechafin").val(),
            estatus:$("#status").val(),
            sku:$("#sku").val()
        };

        conectaPost(path, datos, function (data) {
            $("#cuerpoTabla").html("");
            datosConfirmacion = data;
            $("#botonExcel").show();
            $("#panelConfirmacion").show();
            llenarConfirmaciones();
        });
    }

    function actualizar(){
        var path = "service/catalogoNom/actualizar";
        var datos = {
        idCatMaestro:detalle.idCatMaestro,
        codProveedor:detalle.codProveedor,
        codBarra:detalle.codBarra,
        clave_de_linea_de_producto:$("#clv").val(),
        familia: $("#familia").val(),
        subFamilia: $("#subFamilia").val(),
        subSubFamilia: $("#subSubFamilia").val(),
        descripcion: $("#descripcion").val(),
        category: $("#category").val(),
        middleCategory: $("#mCategory").val(),
        subcategory: $("#SubCategory").val(),
        smallCategory: $("#smallCategory").val(),
        product: $("#product").val(),
        fraccAr: $("#fraccAr").val(),
        inner_: $("#inner_").val(),
        validacion: $("#validacion").val(),
        fvMxp: $("#fvMxp").val(),
        fvRmb: $("#fvRmb").val(),
        ctRmb: $("#ctRmb").val(),
        textil: $("#textil").val(),
        calzado: $("#calzado").val(),
        fechaRegistro:detalle.fechaRegistro,
        archivoExcel:detalle.archivoExcel,
        };
        conectaPost(path, datos, function (data) {
            mensajeExito("Se realizo la actualizacion correctamente.");
            $("#cuerpoTabla").html("");
            datosCatalogo = data;
            buscar();
        });
    }

    function llenarConfirmaciones() {
        var resHTML = "";
        if(datosConfirmacion.length <=0){
            resHTMLExcel="";
            resHTMLXml="";
        }else{
            resHTMLExcel="<div class='row'>";
            resHTMLExcel += "<td class='text-center'>";
            resHTMLExcel += '<button type="button" id="excel"  class="excel btn btn-danger btn-xs">Exportar</button>';
            resHTMLExcel += "</td>";
            resHTMLExcel += "</div>";
            
            resHTMLXml="<div class='row'>";
            resHTMLXml += "<td class='text-center'>";
            resHTMLXml += '<button type="button" id="xml"  class="xml btn btn-primary btn-xs">Xml</button>';
            resHTMLXml += "</td>";
            resHTMLXml += "</div>";
        }
        for (var i = 0; i < datosConfirmacion.length; i++){
            var excel = datosConfirmacion[i];
            resHTML += "<tr>";
            resHTML += "<td style='text-align: justify;'>";
            resHTML += excel.codProveedor;
            resHTML += "</td>";

            resHTML += "<td>";
            resHTML += excel.codBarra;
            resHTML += "</td>";
            
            resHTML += "<td class='text-center'>";
            resHTML += excel.estatus;
            resHTML += "</td>";
            
            resHTML += "<td class='text-center'>";
            resHTML += excel.clave_de_linea_de_producto;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += excel.familia;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += excel.subFamilia;
            resHTML += "</td>";

            resHTML += "<td class='text-right'>";
            resHTML += excel.descripcion;
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="detalle-' + i + '"  class="detalle btn btn-danger btn-xs confirmaciones" data-toggle="tooltip" data-placement="left">detail</button>';
            resHTML += "</td>";

            resHTML += "<td class='text-center'>";
            resHTML += '<button type="button" id="editar-' + i + '"  class="editar btn btn-success btn-xs confirmaciones" data-toggle="tooltip" data-placement="left">Editar</button>';
            resHTML += "</td>";

        }
        if (tabla != ""){
            tabla.destroy();
        }
        $("#cuerpoTabla").html(resHTML);
        $("#contenidoExcel").html(resHTMLExcel);
        $("#contenidoXml").html(resHTMLXml);

        $(".detalle").click(function (e){
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("detalle-", "");
            catalogoSel = datosConfirmacion[miId * 1];
            obtenDetalle(catalogoSel,1);
        });
        
        $(".editar").click(function (e){
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("editar-", "");
            catalogoSel = datosConfirmacion[miId * 1];
            obtenDetalle(catalogoSel,2);
        });

        $(".delete").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("eli-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            $("#modalEliminar").modal();
            $("#eliminarOrdenTexto").html(confirmacionSel.numeroOrden);
            $("#eliminarTipoTexto").html(confirmacionSel.tipoOrden);

        });

        $(".xml").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("xml-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            obtenXML(confirmacionSel);
        });

        $(".excel").click(function (e) {
             e.preventDefault();
            var miId = this.id;
            miId = miId.replace("exc-", "");
            confirmacionSel = datosConfirmacion[miId * 1];
            obtenExcel(confirmacionSel);
        });
        var filtros = [];

        $("#titulosConfirmaciones").html(creaEncabezados());
        tabla = crearDatatableConfirmaciones("#tablaReporteConfirmacion", filtros);

    }

    function creaEncabezados() {
        var resHTML = "<tr>";
        resHTML += "<th style='background:white'>SKU</th>";
        resHTML += "<th style='background:white'>Codigo de barras</th>";
        resHTML += "<th>Estatus</th>";
        resHTML += "<th>Clave de producto</th>";
        resHTML += "<th>Familia</th>";
        resHTML += "<th>Sub Familiaquantity</th>";
        resHTML += "<th>Descripcion</th>";
        resHTML += "<th class='text-center'>Details</th>";
        resHTML += "<th class='text-center'>Detalle</th>";
        resHTML += "</tr>";
        return resHTML;
    }

    function obtenXML(){
        var path = "service/catalogoNom/xml?uuid=" + $("#uuid").val() +"&cbarras="+$("#cBarras").val()+ "&sku=" + $("#sku").val() + "&fecha=" + $("#fecha").val()+"&fechaInicio="+ $("#fechaInicio").val() + "&fechaFin=" + $("#fechafin").val() + "&status="+$("#status").val();
        window.open(path, '_blank');
    }

    function obtenExcel(){
        var path = "service/catalogoNom/excel?uuid=" + $("#uuid").val() +"&cbarras="+$("#cBarras").val()+ "&sku=" + $("#sku").val() + "&fecha=" + $("#fecha").val()+"&fechaInicio="+ $("#fechaInicio").val() + "&fechaFin=" + $("#fechafin").val() + "&status="+$("#status").val();
        window.open(path, '_blank');
    }

    function eliminaConfirmacion(confirmacion) {
        var path = "service/catalogoNom/eliminar";
        conectaPost(path, confirmacion, function (data){
            mensajeExito("Se realizó la eliminación correctamente.");
            $("#modalEliminar").modal('toggle');
            buscar();
        });
    }

    function obtenDetalle(confirmacion,num){
        var path = "service/catalogoNom/detalle";
        conectaPost(path, confirmacion, function (data){
            detalleCatalogo = data;
            creaDetalle(num);
            $("#modalDetalle").modal();
        });
    }

    function creaDetalle(num) {
        $("#detalleNumOrden").html(catalogoSel.codProveedor);
        $("#detalleCBarras").html(catalogoSel.codBarra);
        var resHTML = "";
        var resHTMLGeneral="";
        var resHTMLAct="<div class='row'>";
        resHTMLAct += "<td class='text-center'>";
        resHTMLAct += '<button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar">Cerrar</button>';
        resHTMLGeneral = titulosTablaTabs("tablaGeneral");
        var general = true;
        if(num==1){
            for (var i = 0; i < detalleCatalogo.length; i++) {
                detalle = detalleCatalogo[i];
                resHTMLGeneral += llenaCuarpoTab(detalle);
                general = true;
            }
        }else{
            for (var i = 0; i < detalleCatalogo.length; i++) {
                detalle = detalleCatalogo[i];
                resHTMLGeneral += llenaCuarpoTabEditar(detalle);
                general = true;
            }
            resHTMLAct += '<button type="button" class="btn btn-primary" data-dismiss="modal" id="actualizar">Actualizar</button>';
        }
        resHTMLAct += "</td>";
        resHTMLAct += "</div>";
        resHTMLGeneral += "</tbody></table>";

        resHTML += '<ul class="nav nav-tabs" role="tablist">';
        if (general){
            resHTML += '<li role="presentation" class="active" ><a href="#tabGeneral" aria-controls="tabGeneral" role="tab" data-toggle="tab">General</a></li>';
        }
        resHTML += '</ul>';

        resHTML += '<div class="tab-content" style="margin-top:10px">';
        if (general){
            resHTML += '<div role="tabpanel" class="table-responsive" id="tabGeneral">2</div>';
        }
        resHTML += '</div>';
        $("#contenidoDetalle").html(resHTML);
        $("#contenidoActualizar").html(resHTMLAct);

        if (general){
            $("#tabGeneral").html(resHTMLGeneral);
            $("#tablaGeneral").DataTable();
        }
        $("#actualizar").click(function () {
            actualizar();
        });
    }
    
    function titulosTablaTabs(id) {
        var resHTML = "<table class='table table-striped' id='" + id + "'>";
        resHTML += "<thead><tr>";
        resHTML += "<th>idCatMaestro</th>";
        resHTML += "<th>codProveedor</th>";
        resHTML += "<th>codBarra</th>";
        resHTML += "<th>clave_de_linea_de_producto</th>";
        resHTML += "<th>familia</th>";
        resHTML += "<th>subFamilia</th>";
        resHTML += "<th>subSubFamilia</th>";
        resHTML += "<th>descripcion</th>";
        resHTML += "<th>category</th>";
        resHTML += "<th>middleCategory</th>";
        resHTML += "<th>subcategory</th>";
        resHTML += "<th>smallCategory</th>";
        resHTML += "<th>product</th>";
        resHTML += "<th>fraccAr</th>";
        resHTML += "<th>inner</th>";
        resHTML += "<th>validacion</th>";
        resHTML += "<th>fvMxp</th>";
        resHTML += "<th>fvRmb</th>";
        resHTML += "<th>ctRmb</th>";
        resHTML += "<th>textil</th>";
        resHTML += "<th>calzado</th>";
        resHTML += "<th>fechaRegistro</th>";
        resHTML += "<th>fechaActualizacion</th>";
        resHTML += "<th>archivoExcel</th>";
        resHTML += "<th>estatus</th>";
        resHTML += "</tr></thead>";
        resHTML += "<tbody>";
        return resHTML;
    }

    function llenaCuarpoTab(detalle){
        var resHTML = "";
        resHTML += '<tr>';

        resHTML += '<td>';
        resHTML += detalle.idCatMaestro;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.codProveedor;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.codBarra;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.clave_de_linea_de_producto;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.familia;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.subFamilia;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.subSubFamilia;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.descripcion;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.category;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.middleCategory;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.subcategory;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.smallCategory;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.product;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.fraccAr;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.inner_;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.validacion;
        resHTML += '</td>';
     
        resHTML += '<td>';
        resHTML +=detalle.fvMxp;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.fvRmb;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.ctRmb;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.textil;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.calzado;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.fechaRegistro;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.fechaActualizacion;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.archivoExcel;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.estatus;
        resHTML += '</td>';

        resHTML += '</tr>';
        return resHTML;
    }
    
    
    function llenaCuarpoTabEditar(detalle){
        var resHTML = "";
        resHTML += '<tr>';

        resHTML += '<td>';
        resHTML += detalle.idCatMaestro;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.codProveedor;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML +=detalle.codBarra;
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="clv" value="'+detalle.clave_de_linea_de_producto+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="familia" value="'+detalle.familia+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="subFamilia" value="'+detalle.subFamilia+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="subSubFamilia" value="'+detalle.subSubFamilia+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="descripcion" value="'+detalle.descripcion+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="category" value="'+detalle.category+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="mCategory" value="'+detalle.middleCategory+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="SubCategory" value="'+detalle.subcategory+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="smallCategory" value="'+detalle.smallCategory+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="product" value="'+detalle.product+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="fraccAr" value="'+detalle.fraccAr+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="inner_" value="'+detalle.inner_+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="validacion" value="'+detalle.validacion+'">';
        resHTML += '</td>';
     
        resHTML += '<td><input type="text" id="fvMxp" value="'+detalle.fvMxp+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="fvRmb" value="'+detalle.fvRmb+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="ctRmb" value="'+detalle.ctRmb+'">';
        resHTML += '</td>';

        resHTML += '<td><input type="text" id="textil" value="'+detalle.textil+'">';
        resHTML += '</td>';
        
        resHTML += '<td><input type="text" id="calzado" value="'+detalle.calzado+'">';
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML +=detalle.fechaRegistro;
        resHTML += '</td>';

        resHTML += '<td>';
        resHTML += detalle.fechaActualizacion;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.archivoExcel;
        resHTML += '</td>';
        
        resHTML += '<td>';
        resHTML += detalle.estatus;
        resHTML += '</td>';

        resHTML += '</tr>';
        return resHTML;
    }

    function limpiar() {
        $("#noOrden").val("");
        $("#sku").val("");
        $("#status").val(0);
        $("#cBarras").val("");
        $("#fecha").val("");
        $("#fechaInicio").val("");
        $("#fechafin").val("");
    }
});



