$(document).ready(function ()
{
    eventos();
    function eventos() {
        $("#miCatalogo").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=3");
        });
        $("#miCatalogoInput").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=6");
        });
        
        $("#cargaJsonGds").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=4");
        });
        $("#cargaJsonInpt").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=5");
        });
        $("#carExcel").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=7");
        });
        $("#catExcel").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=8");
        });
        
        $("#carNom").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=9");
        });
        $("#catNom").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=10");
        });
        
        $("#rErrores").click(function () {
            buscarErrores();
        });
        $("#rCompras").click(function () {
            reporteCompras();
        });
        $("#inicio").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=1");
            loadPage("#menu", "service/navegacion/seleccionaMenu?id=3");
        });

        $("#salir").click(function () {
            loadPage("#cuerpo", "service/navegacion/pagina?id=2");
            loadPage("#menu", "service/navegacion/seleccionaMenu?idMenu=0");
        });

    }
});
    function buscarErrores(){
        var path = "service/reporteErrores/buscarErrores";
        window.open(path, '_blank');
    }

    function reporteCompras(){
        var path = "service/reporteCompras/buscarSkuCompras";
        window.open(path, '_blank');
        if (this.status === 200){
            mensajeExito("Se genero el reporte correctamente");
        }
    }