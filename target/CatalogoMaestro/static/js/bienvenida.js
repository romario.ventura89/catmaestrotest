var sociedades;
$(document).ready(function () {

    function cargaSociedades() {
        var path = "service/bienvenida/obtenerSociedades";
        conectaPost(path, "", function (data) {
            sociedades = data;
            creaBotones();
        });
    }

    function creaBotones() {
        var resHTML = "";
        for (var i = 0; i < sociedades.length; i++)
        {
            var sociedad = sociedades[i];
            resHTML += "<div class='col-md-3'>";
            resHTML += "<button id='soc-" + sociedad.idSociedad + "' class='btn btn-danger btn-lg sociedades col-md-12'> ";
            resHTML += sociedad.descripcion;
            resHTML += "</button> ";
            resHTML += "</div>";
        }
        $("#sociedades").html(resHTML);
        $(".sociedades").click(function (e)
        {
            e.preventDefault();
            var miId = this.id;
            miId = miId.replace("soc-", "");
            loadPage("#menu", "service/navegacion/seleccionaMenu?idMenu=" + miId);

        });
    }
});

