<%-- 
    Document   : error
    Created on : 19/01/2018, 01:40:49 PM
    Author     : KRIODOFT07
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Atención a clientes</title>

        <link rel="icon" type="image/png" href="../static/img/logo.png" />

        <link href="../static/css/bootstrap.min.css" rel="stylesheet" />

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <link href="../static/css/font-awesome.min.css" rel="stylesheet" />
        <link href="../static/css/animate.css" rel="stylesheet" />
        <link href="../rec/css/et-line-font/et-line-font.css" rel="stylesheet" />
        <link href="../rec/css/style.css" rel="stylesheet" />
        <link href="../rec/css/colors/default.css" rel="stylesheet">
        <link href="../static/css/themes/nest.css" rel="stylesheet">
        <link href="../static/css/noty.css" rel="stylesheet">
        <link href="../rec/css/custom.css" rel="stylesheet" />
    </head>
    <body>
        
        <div class="navbar navbar-custom navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="../index.html"><img src="../static/img/logoMini.png" alt="MINISO" /></a>
                </div>
                <div class="font-alt mb-20 titan-title-size-1" style="margin-top:20px;color: #fff;font-size: 2em">PORTAL DE ATENCIÓN A CLIENTES DE MINISO</div>
            </div>
        </div>
        
        <main id="cuerpo" style="min-height: 400px; margin: 70px 0px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 100px">

                        <div class="font-alt mb-10 titan-title-size-2" ><span class="icon-sad" aria-hidden="true" style="color: #d8262f; font-size: 3em"></span> Zona restringida</div>
                    </div>
                </div>
                <div class="row">
                    <hr class="hrRojo"/>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-8 col-sm-offset-2">
                            <h4 class="font-alt mb-0">Error de Acceso</h4>
                            <hr class="divider-w mt-10 mb-20">
                            <p>
                                Esta intentando entrar a una zona restringida, favor de verificar su usuario y contraseña gracias.</p>
                            <p>
                                Para poder regresar favor de dar clic en el botón inicio, gracias.
                            </p>
                          
                            <a class="navbar-brand" href="../index.html"> <button href="../index.html" type="button" id="inicio" class="btn btn-danger"><i class="fa fa-home"></i> Inicio</button></a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </main>
               
        <div class="navbar navbar-fixed-bottom">
            <footer class="footer bg-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="copyright font-alt">&copy; 2018&nbsp; Kriosoft, All Rights Reserved</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        
        <script src="rec/js/lib/jquery-3.2.1.min.js"></script>
        <script src="rec/js/lib/bootstrap.min.js"></script>
        <script src="rec/js/lib/noty.min.js"></script>
        <script src="rec/js/lib/moment.min.js"></script>
        <script src="rec/js/conecta.js"></script>
        <script src="rec/js/utilidades.js"></script>
        <script src="rec/js/index.js"></script> 
    </body>
</html>
