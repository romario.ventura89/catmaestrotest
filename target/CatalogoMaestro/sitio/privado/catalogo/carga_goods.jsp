<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>
<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Carga de archivo JSON Goods</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>

<div class="row">
    <form id="cargaMasiva" name="cargamasiva" id="cargamasiva"
          enctype="multipart/form-data" method="post" role="" class="form-horizontal">

        <div class="form-group">
            <label class="col-md-2 text-right">Fecha de carga:*</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="fechaCarga" name="fechaCarga" class="form-control" readonly="readonly"/>
            </div>
            <label class="col-md-2 text-right">Identificador Json(Goods):*</label>
            <div class="col-md-2">
                <input type="text" style="text-transform: none" id="numeroJson" name="numeroJson" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 text-right"  for="archivo">Archivo de carga masiva:*</label> 
            <div class="col-md-6">
                <input class=" col-md-12"
                       name="archivo" id="archivo" type="file"
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                       style="padding: 10px;" />
            </div>
        </div>

        <div class="col-md-12 text-center">
            <button type="button" class="btn btn-danger" id="cargar">Cargar</button>
        </div>
    </form>
</div>

<div class="row" id="panelResultados" style="display: none;margin-top: 30px" >
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Resultados</label>
            </div>
            <div class="panel-body">
                <h1 id="resultadoCarga"></h1>
                <h3>Fecha de Carga: <span id="fecRes"></span></h3>
                <h5 id="datosRes"></h5>
                <table class="table" class=" table-hover display">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Movimiento</th>
                            <th>SKU</th>
                            <th>Marca</th>
                            <th>Nombre</th>
                            <th>Uuid</th>
                            <th>Fecha de Registro</th>
                        </tr>
                    </thead>
                    <tbody id="tbodyRes">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="static/js/catalogo/carga_json.js"></script>

<% }%>