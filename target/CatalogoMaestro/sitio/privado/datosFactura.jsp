
        <div class="container" id="cuerpo" >

            <div class="row">
                <div class="titulo">
                    Facturaci�n MINISO M�xico
                </div>
            </div>

            <div class="row">
                <hr />
            </div>

            <div class="row">
                    <div class="row bs-wizard text-center" style="border-bottom:0;">
                        <div class="col-xs-4 bs-wizard-step active" id="paso1">
                            <div class="text-center bs-wizard-stepnum">Paso 1</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Mis datos.</div>
                        </div>

                        <div class="col-xs-4 bs-wizard-step disabled" id="paso2"><!-- complete -->
                            <div class="text-center bs-wizard-stepnum">Paso 2</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Datos de la compra.</div>
                        </div>

                        <div class="col-xs-4 bs-wizard-step disabled" id="paso3"><!-- complete -->
                            <div class="text-center bs-wizard-stepnum">Paso 3</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Resumen de la factura.</div>
                        </div>
                    </div><br/>

                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Tipo Persona:</label>
                        <div class="col-md-8">
                            <label class="checkbox-inline"><input type="radio" id="tipoMoral" value="1"><span class="label label-info">Moral</span></label>
                            <label class="checkbox-inline"><input type="radio" id="tipoFisica" value="2"><span class="label label-primary">Fisica</span></label>
                        </div>
                    </div><br/>
        
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">RFC:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="rfc" class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Raz�n Social:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="razonSocial" class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Nombre:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="nombre" class="form-control"/>
                        </div>

                    </div>
                    
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Apellido Paterno:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="aPaterno" class="form-control"/>
                        </div>

                    </div>
                    
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Apellido Materno:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="aMaterno" class="form-control"/>
                        </div>

                    </div>
                    
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Correo Electr�nico:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="email" class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Buscar c�digo postal:</label>
                        <div class="col-md-6">
                              <select id="codigoPostal" class="form-control">
                                    <option value="codigoPostal">Seleccionar</option>
                                </select>
                        </div>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Colonia:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="colonia" class="form-control"/>
                        </div>
                    </div>
                    
                   <div class="form-group col-md-12">
                        <label class="col-md-3 control-label">Otro:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="otro" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-danger" id="enviar">Enviar</button>
                    </div>

                </div>



            </div>
        </div>