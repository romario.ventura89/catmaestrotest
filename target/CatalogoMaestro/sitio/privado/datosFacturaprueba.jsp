<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Administración</title>

        <link rel="icon" type="image/png" href="../static/img/logoMini.png" />

        <link href="../static/css/bootstrap.min.css" rel="stylesheet" />

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <link href="../static/css/font-awesome.min.css" rel="stylesheet" />
        <link href="../static/css/animate.css" rel="stylesheet" />
        <link href="../rec/css/et-line-font/et-line-font.css" rel="stylesheet" />
        <link href="../rec/css/style.css" rel="stylesheet" />
        <link href="../rec/css/colors/default.css" rel="stylesheet">
        <link href="../static/css/themes/nest.css" rel="stylesheet">
        <link href="../static/css/noty.css" rel="stylesheet">
        <link href="../rec/css/highcharts.css" rel="stylesheet" />
        <link href="../rec/css/custom.css" rel="stylesheet" />

    </head>
            <div class="page-loader" style="display: none">
            <div class="loader-text">Cargando...</div>
            <div class="loader"></div>
        </div>
        <main id="cuerpo"></main>
        <div id="menu"></div>
        <div id="portada" >
            <div class="titan-caption">
                <div class="caption-content" style="color: #fff">
                    <div class="font-alt mb-30 titan-title-size-1">prueba </div>
                    <div class="font-alt mb-30 titan-title-size-2">Favor de proporcionarnos la siguiente información</div>
                    <div class="form-horizontal col-md-offset-3 col-md-6">
                        <div class="form-group">
                            <form class="form">
                                <div class="form-group">
                                    <input type="text" style="text-transform: none" id="rfcCliente" class="form-control" placeholder="RFC"/><br />
                                </div>
                                <div class="form-group">
                                    <input type="text" style="text-transform: none" id="serie" class="form-control" placeholder="Número de serie"/><br />
                                </div>
                                <div class="form-group">
                                    <input type="text" style="text-transform: none" id="folio" class="form-control" placeholder="Folio"/><br />
                                </div>                                
                                <a class="section-scroll btn btn-border-w btn-round" href="#" id="enviaInfo" >Enviar</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <main id="cuerpo">
            
        </main>

        <hr class="divider-d">
        <footer class="footer bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <p class="copyright font-alt">&copy; 2018&nbsp; Kriosoft, All Rights Reserved</p>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script src="../static/js/lib/jquery-3.2.1.min.js"></script>
         <script src="../rec/js/lib/highcharts.js"></script>
        <script src="../static/js/lib/bootstrap.min.js"></script>
        <script src="../static/js/lib/noty.min.js"></script>
        <script src="../static/js/lib/moment.min.js"></script>
        <script src="../rec/js/lib/fullcalendar.min.js"></script>
        <script src="../rec/js/lib/fullcalendar.es.js"></script>
        <script src="../rec/js/conecta.js"></script>
        <script src="../rec/js/utilidades.js"></script>
        <script src="../rec/js/administrador.js"></script>
        <script src="../rec/js/facturacion.js"></script>        
    </body>
</html>
