<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Mis pedidos</p>
    </div>

</div>
<div class="row">
    <hr class="rojohr"/>
</div>


<div class="row">

    <div class="col-md-12" style="padding: 10px 0px">
        <fieldset>
            <div class="form-group col-md-12">
                <table id="tablaMisPedidos" class="table table-bordered" >
                    <thead>
                        <tr>
                            <th style="text-align: right;width: 5%">#</th>
                            <th>Fecha de pedido</th>
                            <th>Estatus</th>
                            <th>Descarga</th>
                        </tr>
                    </thead>
                    <tbody id="cuerpoControlVentas">
                    </tbody>
                </table>
            </div>

        </fieldset>
    </div>
</div>
<script src="static/js/mis_pedidos.js"></script>

<% }%>
