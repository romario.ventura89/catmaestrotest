<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>



<div class="row" id="cuerpo">

    <div class="form-group required col-md-12"></div>
    <div class="form-group required col-md-12">
        <label class="col-md-3 control-label">Numero de pedido:</label>
        <div class="col-md-3">
             <input type="text" style="text-transform: none" id="numPedido" class="form-control" readonly="readonly"/>
        </div>
        <label class="col-md-3 control-label">Fecha de pedido:</label>
        <div class="col-md-3">
             <input type="text" style="text-transform: none" id="fechaPedido" class="form-control" readonly="readonly"/>
        </div>
    </div>        
    
    <div class="form-group required col-md-12">
        <label class="col-md-3 control-label">SKU:</label>
        <div class="col-md-3">
             <input type="text" style="text-transform: none" id="sku" class="form-control" required="required" maxlength="30">
        </div>
        <label class="col-md-3 control-label">Cantidad:</label>
        <div class="col-md-3">
            <input type="text" style="text-transform: none" id="piezas" class="form-control" required="required" maxlength="10"/>
        </div>
    </div>

    <div class="form-group required col-md-12">
        <label class="col-md-3 control-label">Fecha de registro:</label>
        <div class="col-md-3">
            <input type="text" style="text-transform: none" id="fechaRegistro" class="form-control" required="required" readonly="readonly"/>
        </div>
    </div>
    
    <div class="col-md-3">
            <p><span style="color:red">*</span> Campos Obligatorios</p>
    </div>
    
    <div class="form-group col-md-12">
        <label class="col-md-6 control-label"></label>
        <div class="col-md-6 control-label">
            <button type="button" class="btn btn-danger col-md-5" id="guardaSKU">Agregar</button>
        </div>
    </div> <br><br/>
    
    <ul class="nav nav-tabs" role="tablist">
     
    </ul>
    
    <div class="container">
     <div class="row">

        <div role="tabpanel" class="tab-pane active" id="catReporte">
            <fieldset>
                <div class="col-md-12">
                    <table id="tablaControlVentas" class="table table-hover display" >
                        <thead>
                            <tr>
                                <th style="text-align: right;width: 5%">#</th>
                                <th>SKU</th>
                                <th>Piezas</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="cuerpoControlVentas">

                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
            <div class="panel-footer text-right" style="display: none" id="botonExcel">  
                 <a id="descargarPedido" class="btn btn-danger" target="_blank">Descargar Excel</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalEliminar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmación</h4>
            </div>
                <div class="modal-body">
                    <h4>¿Seguro que desea eliminar el producto "<strong id="skuEliminar"></strong>"?</h4>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-d" data-dismiss="modal" aria-label="Close">Cancelar</button>
                <button type="button" class="btn btn-danger" id="eliminarProductoPedido">Eliminar</button>
            </div>
        </div>
    </div>
</div>    
<script src="static/js/dataTable.js"></script>
<script src="static/js/RegistroSku.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<% }%>
