<%-- 
    Document   : almacen
    Created on : 19/01/2018, 01:26:20 PM
    Author     : KRIODOFT07
--%>

<%@page import="mx.com.kriosoft.control.dto.Usuario"%>
<%@page import="mx.com.kriosoft.control.vo.LoginVO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Almacén</title>

        <link rel="icon" type="image/png" href="../rec/img/logo.png" />

        <link href="../rec/css/bootstrap.min.css" rel="stylesheet" />

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

        <link href="../rec/css/font-awesome.min.css" rel="stylesheet" />
        <link href="../rec/css/animate.css" rel="stylesheet" />
        <link href="../rec/css/et-line-font/et-line-font.css" rel="stylesheet" />
        <link href="../rec/css/style.css" rel="stylesheet" />
        <link href="../rec/css/colors/default.css" rel="stylesheet">
        <link href="../rec/css/themes/nest.css" rel="stylesheet">
        <link href="../rec/css/noty.css" rel="stylesheet">
        <link href="../rec/css/fullcalendar.min.css" rel="stylesheet">
        <link href="../rec/css/custom.css" rel="stylesheet" />

        <%
            Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
            if (usuario == null) {
        %>
        <script>
            window.location.href = "../dmz/error.jsp";
        </script>
        <% } else {%>

    </head>
    <body>

        <div class="page-loader" style="display: none">
            <div class="loader-text">Cargando...</div>
            <div class="loader"></div>
        </div>

        <div id="menu"></div>
        <main id="cuerpo" style="min-height: 600px; margin: 70px 0px">
          
        </main>

        <hr class="divider-d">
        <footer class="footer bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <p class="copyright font-alt">&copy; 2018&nbsp; Kriosoft, All Rights Reserved</p>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-social-links"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-dribbble"></i></a><a href="#"><i class="fa fa-skype"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script src="../rec/js/lib/jquery-3.2.1.min.js"></script>
        <script src="../rec/js/lib/bootstrap.min.js"></script>
        <script src="../rec/js/lib/noty.min.js"></script>
        <script src="../rec/js/lib/moment.min.js"></script>
        <script src="../rec/js/lib/fullcalendar.min.js"></script>
        <script src="../rec/js/lib/fullcalendar.es.js"></script>
        <script src="../../../static/js/conecta.js"></script>
        <script src="../../../static/js/utilidades.js"></script>

    </body>
    <% }%>
</html>
