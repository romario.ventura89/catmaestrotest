
<%@page import="java.util.Date"%>
<%@page import="mx.com.miniso.maestro.dto.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script>
    $("body").show();
</script>

<%
    Usuario usuario = (Usuario) request.getSession().getAttribute("USUARIO");
    if (usuario == null) {
%>
<script>
    window.location.href = "sitio/privado/error.jsp";
</script>
<% } else {
%>
<div class="row">
    <div class="col-md-12">
        <p class="tituloPage">Catalogo Maestro Actualizado</p>
    </div>
</div>
<div class="row">
    <hr class="rojohr"/>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <label class="text-right">Filtros</label>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group confirmaciones">
                        <label class="col-md-2 text-right">Sku:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="sku" class="form-control">
                        </div>
                        <label class="col-md-2 text-right">Codigo de barras:</label>
                        <div class="col-md-2">
                            <input type="text" style="text-transform: none" id="cBarras" class="form-control">
                        </div>
                        <label class="col-md-2 text-right">Estatus:</label>
                        <div class="col-md-2">
                            <select id="status" class="form-control">
                                <option selected value="0">Selecciona un estatus</option>
                                <option value="1">Nuevo</option>
                                <option value="2">Actualizado</option>
                                <option value="3">Erroneo</option>
                             </select>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">

                </div>
                
                <div class="form-horizontal">
                    <div class="accordion" id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" id="limpiarFec">
                                  Fecha Registro/Actualizacion
                                </button>
                              </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <label class="col-md-2 text-right">Fecha:</label>
                                    <div class="form-group confirmaciones center-block">
                                      <div class="col-md-2">
                                          <input type="text" style="text-transform: none" id="fecha" class="form-control" readonly="readonly"/>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingOne">
                              <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" id="limpiarRan">
                                  Rango de fechas
                                </button>
                              </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="form-group confirmaciones" id="rangoReg">
                                        <label class="col-md-2 text-right">Fecha de inicio:</label>
                                        <div class="col-md-2">
                                            <input type="text" style="text-transform: none" id="fechaInicio" class="form-control" readonly="readonly"/>
                                        </div>
                                        <label class="col-md-2 text-right">Fecha de fin</label>
                                        <div class="col-md-2">
                                            <input type="text" style="text-transform: none" id="fechafin" class="form-control" readonly="readonly"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="panel-footer text-center">
                        <button type="button" class="btn btn-default" id="limpiar">Limpiar</button>
                        <button type="button" class="btn btn-danger" id="buscar">Buscar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12" >
        <table id="tablaReporteConfirmacion" class="table-hover display" class="table">
            <thead id="titulosConfirmaciones">

            </thead>
            <tbody id="cuerpoTabla">
            </tbody>
            <tfoot>
                <div id="contenidoExcel" class="col-md-12">
                </div>
                <div id="contenidoXml" class="col-md-12">
                </div>
            </tfoot>
        </table>
    </div>
</div>

<div class="modal body" tabindex="-1" role="dialog" id="modalDetalle">
    <div class="modal-dialog" role="document" style="width: 75%">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">SKU: <span id="detalleNumOrden"></span></h4>
                <h4 class="modal-title">Codigo de Barras: <span id="detalleCBarras"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row" >
                    <div id="contenidoDetalle" class="col-md-12"></div>
                </div>
            </div>
            <div class="modal-footer">
                
                
                <div id="contenidoActualizar" class="col-md-12">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal body" tabindex="-1" role="dialog" id="modalEditar">
    <div class="modal-dialog" role="document" style="width: 75%">
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">SeditarKU: <span id="detalleNumOrden2"></span></h4>
                <h4 class="modal-title">Codigo de Barras: <span id="detalleCBarras2"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row" >
                    <div id="contenidoDetalle2" class="col-md-12"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar2">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal body" tabindex="-1" role="dialog" id="modalEliminar" >
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header modalPopup">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Eliminación de Registro</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12">
                        <h3>¿Está seguro que desea eliminar el SKU con la número 
                            "<span id="eliminarOrdenTexto"></span>", y UUID "<span id="eliminarTipoTexto"></span>"?</h3>
                        <h4>El registro se eliminará forma permanente.</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" id="aceptarEliminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>   
<script src="static/js/actualizacion/catalogo_actualizado.js"></script>

<% }%>