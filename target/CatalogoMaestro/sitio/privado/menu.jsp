<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MINISO M&Eacute;XICO | CONTROL DE VENTAS</title>

        <link rel="icon" type="image/png" href="static/img/logo.png" />

        <link href="static/css/bootstrap.min.css" rel="stylesheet" />
        <link href="static/css/mdb.min.css" rel="stylesheet" />

        <link href="static/css/font-awesome.min.css" rel="stylesheet" />
        <link href="static/css/animate.css" rel="stylesheet" />
        <link href="static/css/noty.css" rel="stylesheet">
        <link href="static/css/themes/metroui.css" rel="stylesheet">
        <link href="static/css/fuentes.css" rel="stylesheet">
        <link href="static/css/custom.css" rel="stylesheet" />
        <link href="static/css/loader.css" rel="stylesheet" />
    </head>

    <body>


        <div id="loader" style="display: none">  
            <div id="inner_loader">
                <div class="sk-circle">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                    <div class="loader-text">Cargando...</div>
                </div>
                <div id="backLoader"></div>
            </div>
        </div>

        <div id="menu" class="container">
            <div class="row">
                <img src="static/img/logo_pagina.png" alt="Miniso" class="animated bounceIn" />
            </div>
        </div>

        <div class="container" id="cuerpo" >

            <div class="row">
                <div class="titulo">
                    Control de Ventas MINISO Mx
                </div>
            </div>

            <div class="row">
                <hr />
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h5>Favor de proporcionarnos la siguiente información:</h5>
                </div>

                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Usuario:</label>
                        <div class="col-md-6">
                            <input type="text" style="text-transform: none" id="username" class="form-control"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label">Contraseña:</label>
                        <div class="col-md-6">
                            <input type="password" style="text-transform: none" id="contra" class="form-control"/>
                        </div>
                    </div>
                    
                   <div class="form-group">
                        <label class="col-md-3 control-label">Código de verificación:</label>
                        <div class="col-md-2">
                            <img id="captcha" />			
                        </div>
                        <div class="col-md-2">
                            <input type="text" maxlength="6" class="form-control"
                                   id="txtCaptcha" style="text-transform: none"/>
                            <small id="error_txtCaptcha"
                                   style="display: none; color: #D0021B"></small>
                        </div>
                        <div class="col-md-1">
                            <a class="btn btn-indigo" href="#" id="refresh" ><i class="fa fa-refresh"></i></a>
                        </div>
                 </div>
                  <div class="form-group">
                       <label class="col-md-3 control-label"></label>
                        <div class="col-md-6 control-label">
                            <button type="button" class="btn btn-danger" id="enviar">Enviar</button>
                        </div>
                   </div>
                   
                </div>
                
          
            </div>
        </div>

        <div class="row">
            <hr />
        </div>
        <div class="footer">
            <div class="container text-center">
                <p id="footer-Miniso">
                    MINISO MEXICO
                </p>
                <p id="derechosReservados">
                    Todos los derechos reservados 2017
                </p>
                <div id="linea"></div>

                <div id="enlaces">
                    <a href="https://www.miniso.com.mx/terminos-de-uso-del-sitio" target="_self">
                        T&eacute;rminos y Condiciones
                    </a>
                    <a href="https://www.miniso.com.mx/politica-de-datos-personales" target="_self">
                        Aviso de Privacidad
                    </a>
                    <a href="https://www.miniso.com.mx/bolsa-de-trabajo" target="_self">
                        Bolsa de Trabajo
                    </a>
                    <a href="https://www.miniso.com.mx/sucursales" target="_self">
                        Sucursales
                    </a>
                    <a href="https://www.miniso.com.mx/facturacion" target="_self">
                        Facturaci&oacute;n
                    </a>
                    <a href="https://www.miniso.com.mx/contacto" target="_self">
                        Contacto
                    </a>
                </div>

                <div id="redes_sociales">
                    <a href="https://twitter.com/minisomexico?lang=es" target="_blank" data-content="https://twitter.com/minisomexico?lang=es" data-type="external" id="comp-javjoi98link" class="wp2link" style="cursor: pointer; width: 18px; height: 18px;">
                        <img id="comp-javjoi98imgimage" alt="" data-type="image" style="width: 18px; height: 18px; object-fit: cover;" src="https://static.wixstatic.com/media/0b3eb2_4dd368fac8c1490498663cccd1612fc0~mv2.png/v1/fill/w_18,h_18,al_c/0b3eb2_4dd368fac8c1490498663cccd1612fc0~mv2.png">

                    </a>
                    <a href="https://es-la.facebook.com/MinisoMexico/" target="_blank" data-content="https://es-la.facebook.com/MinisoMexico/" data-type="external" id="comp-javjq51llink" class="wp2link" style="cursor: pointer; width: 18px; height: 18px;">
                        <img id="comp-javjq51limgimage" alt="" data-type="image" style="width: 18px; height: 18px; object-fit: cover;" src="https://static.wixstatic.com/media/0b3eb2_6ca353f733ca401ea08cdbb2e01a906d~mv2.png/v1/fill/w_18,h_18,al_c/0b3eb2_6ca353f733ca401ea08cdbb2e01a906d~mv2.png">

                    </a>
                    <a href="https://www.instagram.com/minisomexico/" target="_blank" data-content="https://www.instagram.com/minisomexico/" data-type="external" id="comp-javjqhrnlink" class="wp2link" style="cursor: pointer; width: 18px; height: 18px;">
                        <img id="comp-javjqhrnimgimage" alt="" data-type="image" style="width: 18px; height: 18px; object-fit: cover;" src="https://static.wixstatic.com/media/0b3eb2_4ee41eb68150440b897b537607a3c2df~mv2.png/v1/fill/w_18,h_18,al_c/0b3eb2_4ee41eb68150440b897b537607a3c2df~mv2.png">

                    </a>
                </div>

            </div>
        </div>
    </div>

    <script src="static/js/lib/jquery-3.2.1.min.js"></script>
    <script src="static/js/lib/bootstrap.min.js"></script>
    <script src="static/js/lib/noty.min.js"></script>
    <script src="static/js/lib/moment.min.js"></script>
    <script src="static/js/conecta.js"></script>
    <script src="static/js/utilidades.js"></script>
    <script src="static/js/index.js"></script>

</body>
</html>
